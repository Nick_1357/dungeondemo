Developed in Visual Studio 2015, using the XNA 4.0 framework.

The parts I did NOT create:

- The camera
- The dungeon generator
- Mage, archer, and healer actions/spells
- The HUD was made by another, but I refactored it quite a bit to fix problems.
- All resources (sprites/audio)


It's fun to play, but is not complete because a semester is only so long and the world has deadlines.

Also, this version is a static display for my portfolio because we (the group) intend to finish it privately.

To help with understanding:

-The world is represented by a NxNx2 array of bytes, inside DungeonLevel named 'grid', where n,n,0 are bitmapped tiles and n,n,1 are game entities like the player and monsters.

-The primary game loop is performed by TurnController with its passGameTime(int) function that iterates through all mobs and calls their takeAction() function. Then each mob handles itself, player and AI alike.