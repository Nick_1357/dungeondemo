using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace DungeonDemo
{
    public enum DamageType
    {
        normal = 0,
        fire,earth,ice,absolute
    }

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public enum GameState
        {
            StartMenu,
            Loading,
            Playing,
            Paused
        }
        public static int ScreenHeightRef; // set at same time as screen in game1 constructor
        public static int ScreenWidthRef;  // set at same time as screen in game1 constructor
        public static float CameraZoomDEF;   // set in game1 constructor, DEFINES CAMERA ZOOM
        public static float CameraMoveSpdDEF;// set in game1 constructor, DEFINES CAMERA MOVE-SPEED
        public static GameState gameState;
        public static Random rng = new Random();
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static SpriteFont gameFont;
        public static SpriteFont hudFont;

        StartMenu startMenu;
        Texture2D startMenuTex;

        Texture2D fireHoundSpriteSheet;
        Texture2D iceHoundSpriteSheet;

        public static Player player;
        public static Player companion;

        DungeonLevel currentDungeon;
        public static Camera2D currentCamera;
        public HUD hud;

        Song backgroundMusic;
        Song earthMusic;
        Song fireMusic;
        Song iceMusic;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280; //dev and tested in 1280 //in prog devloping 1024
            graphics.PreferredBackBufferHeight = 640; //dev and tested in 640  //in prog devloping 512

            CameraZoomDEF = 1.75f;   //dev and tested in 1.75f
            CameraMoveSpdDEF = 1.1f; //dev and tested in 1.0f

            ScreenWidthRef = graphics.PreferredBackBufferWidth;
            ScreenHeightRef = graphics.PreferredBackBufferHeight;

            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;

            gameState = GameState.StartMenu;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            startMenuTex = Content.Load<Texture2D>(@"sprites\misc\startMenu");
            Texture2D selectionBoxTex = Content.Load<Texture2D>(@"sprites\misc\selectionBox");
            startMenu = new StartMenu(startMenuTex, selectionBoxTex, this);

            DungeonData.knightSpriteSheet = Content.Load<Texture2D>(@"sprites\playerClasses\knight");//*&^^rename correctly when resources are added
            DungeonData.archerSpriteSheet = Content.Load<Texture2D>(@"sprites\playerClasses\archer");
            DungeonData.mageSpriteSheet = Content.Load<Texture2D>(@"sprites\playerClasses\mage");
            DungeonData.healerSpriteSheet = Content.Load<Texture2D>(@"sprites\playerClasses\priest");

            ///////////////// Assign Monster Sprites /////////////////////////
            DungeonData.fireMon1Sprites = Content.Load<Texture2D>(@"sprites\monsters\GrayHellHound");
            DungeonData.fireMon2Sprites = DungeonData.knightSpriteSheet;

            DungeonData.iceMon1Sprites = Content.Load<Texture2D>(@"sprites\monsters\BlueHellHound");
            DungeonData.iceMon2Sprites = DungeonData.healerSpriteSheet;

            DungeonData.earthMon1Sprites = DungeonData.archerSpriteSheet;
            DungeonData.earthMon2Sprites = Content.Load<Texture2D>(@"sprites\monsters\green-dragon");
            //////////////////////////////////////////////////////////////

            //tile sprites
            DungeonData.BLACKSPACE = Content.Load<Texture2D>(@"sprites\tiles\blackSpace");
            DungeonData.WHITESPACE = Content.Load<Texture2D>(@"sprites\misc\1x1WhitePixel");
            DungeonData.earthTiles = Content.Load<Texture2D>(@"sprites\tiles\earth_dungeon");
            DungeonData.fireTiles = Content.Load<Texture2D>(@"sprites\tiles\fire_dungeon");
            DungeonData.iceTiles = Content.Load<Texture2D>(@"sprites\tiles\ice_dungeon");
            DungeonData.overWorldTiles = Content.Load<Texture2D>(@"sprites\tiles\overworld");

            //spells texture content
            DungeonData.fireballSprite = Content.Load<Texture2D>(@"sprites\spells\fireball");
            DungeonData.particleSprites = Content.Load<Texture2D>(@"sprites\spells\particles");
            DungeonData.archerArrowSprite1 = Content.Load<Texture2D>(@"sprites\spells\Arrow_RegularShot1");
            DungeonData.piercingShotSprite = Content.Load<Texture2D>(@"sprites\spells\Arrow_BlueMagic");

            /// <summary>
            /// HUD content loaded
            /// </summary>
            DungeonData.HUDcontrolsImage = Content.Load<Texture2D>(@"sprites\misc\wasd-mouse-controls");
            gameFont = Content.Load<SpriteFont>(@"sprites\Fonts\DungeonFont");
            hudFont = Content.Load<SpriteFont>(@"sprites\Fonts\HUDfont");

            DungeonData.armorhit = Content.Load<SoundEffect>(@"sound\armor hit");
            DungeonData.arrowland = Content.Load<SoundEffect>(@"sound\arrowland");
            DungeonData.arrowlaunch = Content.Load<SoundEffect>(@"sound\arrowlaunch");
            DungeonData.spellsound = Content.Load<SoundEffect>(@"sound\spell sound");
            DungeonData.fireball = Content.Load<SoundEffect>(@"sound\fireball");

            backgroundMusic = Content.Load<Song>(@"sound\bgm\backgroundmusic");
            fireMusic = Content.Load<Song>(@"sound\bgm\fireearthmusic");
            earthMusic = Content.Load<Song>(@"sound\bgm\fireearthmusic");
            iceMusic = Content.Load<Song>(@"sound\bgm\icemusic");
            MediaPlayer.Play(backgroundMusic);
            MediaPlayer.IsRepeating = true;

            player = new Player(new Stats(), 1, Player.PlayerClass.mage, 27, 1, new Rectangle(3 * 32, 3 * 32, 32, 32), DungeonData.mageSpriteSheet, DungeonData.defaultAnimSpeed,
                            new Point(32, 32), 4); //sets player at grid location 3,3 and assumes 32 tile size and 3 animation frames

            companion = new Player(new Stats(), 2, Player.PlayerClass.knight, 28, 1, new Rectangle(2 * 32, 2 * 32, 32, 32), DungeonData.knightSpriteSheet, DungeonData.defaultAnimSpeed,
                            new Point(32, 32), 4); //sets companion at grid location 2,2 and assumes 32 tile size sprite and 3 animation frames

            currentDungeon = new DungeonLevel(1, player, companion, (int)DungeonData.dungeonType.overworld); //"Friendly Load Message :}", DungeonData.fireTiles, monsterTextures);

            //init camrea after dungeon level data is populated
            currentCamera = new Camera2D();                     ///////////////////////////// new by JORDAN
            currentCamera.Initialize(DungeonLevel.mapSize.X, DungeonLevel.mapSize.Y, this); ///////////////////////////// new by JORDAN // passed in 'this' so it can access the GraphicsDevice static variable

            //init HUD here after player and companion are created
            hud = new HUD(player, companion, hudFont);  /////////////////////////////////// new by ALLISON

            TempPrintOnScreen t = new TempPrintOnScreen();
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            switch (gameState)
            {
                case GameState.StartMenu:
                    startMenu.update();

                    break;
                case GameState.Playing:
                    // Allows the game to exit
                    if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                        this.Exit();

                    for (int i = 0; i < DungeonLevel.mobs.Count; i++)
                    {
                        DungeonLevel.mobs[i].Update(gameTime);
                    }
                    for (int i = 0; i < DungeonLevel.activeSpells.Count; i++)
                    {
                        DungeonLevel.activeSpells[i].update(gameTime);
                    }
                    for (int i = 0; i < DungeonLevel.particles.Count; i++)
                    {
                        DungeonLevel.particles[i].update(gameTime);
                    }

                    DungeonLevel.Update(gameTime);

                    ///////////////////////////////////////////////////////////////////////////////////////
                    // Check to advance level
                    if (DungeonLevel.player.animState == Base.AnimState.waiting)
                    {
                        if (player.stats.health <= 0)
                        {
                            DungeonLevel.transitionTarget = (int)DungeonData.dungeonType.overworld;
                            DungeonLevel.levelTarget = 1;
                            currentDungeon = currentDungeon.getNextLevel();
                            currentCamera.Initialize(DungeonLevel.mapSize.X, DungeonLevel.mapSize.Y, this);
                            player.stats.health = player.stats.healthMax;
                            companion.stats.health = companion.stats.healthMax;
                            return;
                        }

                        if (currentDungeon.specificDungeon.checkTransitions()) // check to change level
                        {
                            int currentType = DungeonLevel.type;
                            currentDungeon = currentDungeon.getNextLevel();
                            currentCamera.Initialize(DungeonLevel.mapSize.X, DungeonLevel.mapSize.Y, this);

                            if (DungeonLevel.type != currentType)
                            {

                                // Pause all song
                                MediaPlayer.Stop();

                                switch (DungeonLevel.type)
                                {
                                    case (int)DungeonData.dungeonType.overworld:
                                        // Play overworld music here
                                        MediaPlayer.Play(backgroundMusic);

                                        break;
                                    case (int)DungeonData.dungeonType.fire_dungeon:
                                        // Play fire dungeon music
                                        MediaPlayer.Play(fireMusic);
                                        break;

                                    case (int)DungeonData.dungeonType.ice_dungeon:
                                        // Play ice dungeon music
                                        MediaPlayer.Play(iceMusic);
                                        break;

                                    case (int)DungeonData.dungeonType.earth_dungeon:
                                        // Play earth dungeon music
                                        MediaPlayer.Play(earthMusic);
                                        break;
                                }
                            }
                        }
                    }
                    ////////////////////////////////////////////////////////////////////////////////////////

                    currentCamera.Focus = new Vector2(DungeonLevel.mobs[0].posRect.Center.X, DungeonLevel.mobs[0].posRect.Center.Y);///////////////////////////// new by JORDAN
                    currentCamera.Update(gameTime);///////////////////////////// new by JORDAN

                    hud.HUDUpdate(); /////////////////////////////////// new by ALLISON

                    TempPrintOnScreen.Update(gameTime);

                    break;

                case GameState.Paused:
                    break;
                case GameState.Loading:
                    break;
                
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);

            switch (gameState)
            {
                case GameState.StartMenu://-------------------------------------------------------------------
                    spriteBatch.Begin();
                    startMenu.draw(spriteBatch);
                    spriteBatch.End();
                    break;

                case GameState.Playing://---------------------------------------------------------------------
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, currentCamera.Transform); ///////////////////////////// new by JORDAN
                    for (int i = 0; i < DungeonLevel.tiles.Count; i++)
                    {
                        DungeonLevel.tiles[i].Draw(spriteBatch, gameTime);
                    }
                    for (int i = 0; i < DungeonLevel.mobs.Count; i++)
                    {
                        DungeonLevel.mobs[i].Draw(spriteBatch, gameTime);
                    }
                    for (int i = 0; i < DungeonLevel.activeSpells.Count; i++)
                    {
                        DungeonLevel.activeSpells[i].Draw(spriteBatch, gameTime);
                    }
                    for (int i = 0; i < DungeonLevel.particles.Count; i++)
                    {
                        DungeonLevel.particles[i].Draw(spriteBatch, gameTime);
                    }
                    TempPrintOnScreen.Draw(spriteBatch);

                    spriteBatch.End();
                    
                    // hud sprite batch to change alpha blend state for colors of the hud to show on top
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied, null, null, null, null, currentCamera.Transform*Matrix.CreateScale(new Vector3(1/CameraZoomDEF, 1/CameraZoomDEF, 1)) ); ///////////////////////////// new by JORDAN
                    hud.Draw(spriteBatch);
                    spriteBatch.End();
                    break;

                case GameState.Paused://----------------------------------------------------------------------
                    break;

                case GameState.Loading://---------------------------------------------------------------------
                    break;
            }
            

            //Matrix mouse = Matrix.Invert(currentCamera.Transform) * new Matrix(   Mouse.GetState().X, 0,0,0,
            //  Mouse.GetState().Y, 0,0,0,
            //  0                 , 0,0,0,
            //  0                 , 0,0,0  );
            //mouseCoord = "(" + ((int)(mouse.M11+16)) + "," + ((int)(mouse.M21+16)) + ")";

            //string mouseCoord = "(" + ( (int)(Mouse.GetState().X/currentCamera.Scale + 16 + currentCamera.Position.X - currentCamera.Origin.X)) + 
            // "," + ( (int)(Mouse.GetState().Y/currentCamera.Scale + 16 + currentCamera.Position.Y - currentCamera.Origin.Y)) + 
            // ")";

            //string cameraPoint = "(" + currentCamera.Position.X + "," + currentCamera.Position.Y + ")";

            //spriteBatch.DrawString(gameFont, mouseCoord, new Vector2(DungeonLevel.player.posRect.X, DungeonLevel.player.posRect.Y), Color.LemonChiffon);//Webster ran out of names for colors.




            base.Draw(gameTime);
        }
    }

}
