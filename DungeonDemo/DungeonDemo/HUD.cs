﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

/// <summary>
/// THIS IS THE ONE WITH ERRRORS IN TITLE USE THIS
/// </summary>
namespace DungeonDemo
{
    public class HUD
    {
        HUDBar playerHealth;
        HUDBar playerMana;
        HUDBar compHealth;
        HUDBar compMana;
        Player player;
        Player comp;
        SpriteFont gameFont;
        Vector2 PlayerTitleOffset;
        Vector2 CompanionTitleOffset;

        Color manaColor;
        Color healthColor;
        Color hudTextColor;

        Texture2D controlsTexture;
        Rectangle controlsPosRect;
        Rectangle controlsSRCrect;
        Vector2 attackInstructPosition;

        //Build a sprite font object here using everything you already know and see what happens

        public HUD(Player p, Player c, SpriteFont s)
        {
            this.player = p;
            this.comp = c;
            this.gameFont = s;

            manaColor = Color.RoyalBlue;
            healthColor = Color.Crimson;
            hudTextColor = Color.Ivory;

            PlayerTitleOffset = new Vector2(-360, 395);// PlayerTitleOffset = new Vector2(-360, 143);
            playerHealth = new HUDBar(p, DungeonData.WHITESPACE, -360, 420, 220, 100, 100, healthColor); //playerHealth = new HUDBar(p, DungeonData.WHITESPACE, -360, 160, 120, 100, 100, healthColor);  
            playerMana = new HUDBar(p, DungeonData.WHITESPACE, -360, 440, 220, 100, 100, manaColor);//playerMana = new HUDBar(p, DungeonData.WHITESPACE,        -360, 170, 120, 100, 100, manaColor);

            CompanionTitleOffset = new Vector2(-100, 415);// CompanionTitleOffset = new Vector2(-215, 150)
            compHealth = new HUDBar(c, DungeonData.WHITESPACE, -100, 438, 200, 100, 100, healthColor);//compHealth = new HUDBar(c, DungeonData.WHITESPACE, -215, 168, 100, 100, 100, healthColor);
            compMana = new HUDBar(c, DungeonData.WHITESPACE, -100, 448, 200, 100, 100, manaColor);//compMana = new HUDBar(c, DungeonData.WHITESPACE, -215, 174, 100, 100, 100, manaColor);

            controlsTexture = DungeonData.HUDcontrolsImage; //122w 185h
            controlsPosRect = new Rectangle(494, 80, 102, 150);//controlsPosRect = new Rectangle(294, 80, 68, 100);
            controlsSRCrect = new Rectangle(0, 0, controlsTexture.Width, controlsTexture.Height);//controlsSRCrect = new Rectangle(0, 0, controlsTexture.Width, controlsTexture.Height);//

            attackInstructPosition = new Vector2(160, 430);//attackInstructPosition = new Vector2(20, 160);
        }

        public void HUDUpdate()
        {
            playerHealth.update(player.stats.health);
            compHealth.update(comp.stats.health);
            playerMana.update(player.stats.mana);
            compMana.update(comp.stats.mana);

            //update controls texture posRect location with Camera update
            controlsPosRect.X = (int)(800 + Game1.currentCamera.Position.X); // controlsPosRect.X = (int)(294 + Game1.currentCamera.Position.X);
            controlsPosRect.Y = (int)(300 + Game1.currentCamera.Position.Y); //  controlsPosRect.Y = (int)(80 + Game1.currentCamera.Position.Y);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            playerHealth.draw(spriteBatch);
            playerMana.draw(spriteBatch);
            compHealth.draw(spriteBatch);
            compMana.draw(spriteBatch);
            spriteBatch.DrawString(gameFont, player.playerClass.ToString()+"", 
                                    PlayerTitleOffset + Game1.currentCamera.Position, hudTextColor);
            spriteBatch.DrawString(gameFont, comp.playerClass.ToString() +" - com", 
                                    CompanionTitleOffset + Game1.currentCamera.Position, hudTextColor);
            // draw controls texture to HUD and camera
            spriteBatch.Draw(controlsTexture, controlsPosRect, controlsSRCrect, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.01f);
            spriteBatch.DrawString(gameFont, "Attacks: use number keys 1,2,3,4", attackInstructPosition + Game1.currentCamera.Position, hudTextColor);
        }
    }
}