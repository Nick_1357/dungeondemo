﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class ShieldBash : aSpell
    {
        public ShieldBash(Mob caller, int aimLocation)//aim location is 0-7 for the 8 tiles around. 0 is right, increasing counter-clockwise
        {
            int callerX = caller.mapLocX;
            int callerY = caller.mapLocY;
            List<Mob> targets = new List<Mob>();
            Mob tmp1 = null;
            Mob tmp2 = null;
            Mob tmp3 = null;
            switch (aimLocation)
            {
                case 0:
                    tmp1 = DungeonLevel.getMobByLocation(callerX + 1, callerY + 1);//7
                    tmp2 = DungeonLevel.getMobByLocation(callerX + 1, callerY);//0
                    tmp3 = DungeonLevel.getMobByLocation(callerX + 1, callerY - 1);//1
                    break;
                case 1:
                    tmp1 = DungeonLevel.getMobByLocation(callerX + 1, callerY);//0
                    tmp2 = DungeonLevel.getMobByLocation(callerX + 1, callerY - 1);//1
                    tmp3 = DungeonLevel.getMobByLocation(callerX, callerY - 1);//2
                    break;
                case 2:
                    tmp1 = DungeonLevel.getMobByLocation(callerX + 1, callerY - 1);//1
                    tmp2 = DungeonLevel.getMobByLocation(callerX, callerY - 1);//2
                    tmp3 = DungeonLevel.getMobByLocation(callerX - 1, callerY - 1);//3
                    break;
                case 3:
                    tmp1 = DungeonLevel.getMobByLocation(callerX, callerY - 1);//2
                    tmp2 = DungeonLevel.getMobByLocation(callerX - 1, callerY - 1);//3
                    tmp3 = DungeonLevel.getMobByLocation(callerX - 1, callerY);//4
                    break;
                case 4:
                    tmp1 = DungeonLevel.getMobByLocation(callerX - 1, callerY - 1);//3
                    tmp2 = DungeonLevel.getMobByLocation(callerX - 1, callerY);//4
                    tmp3 = DungeonLevel.getMobByLocation(callerX - 1, callerY + 1);//5
                    break;
                case 5:
                    tmp1 = DungeonLevel.getMobByLocation(callerX - 1, callerY);//4
                    tmp2 = DungeonLevel.getMobByLocation(callerX - 1, callerY + 1);//5
                    tmp3 = DungeonLevel.getMobByLocation(callerX, callerY + 1);//6
                    break;
                case 6:
                    tmp1 = DungeonLevel.getMobByLocation(callerX - 1, callerY + 1);//5
                    tmp2 = DungeonLevel.getMobByLocation(callerX, callerY + 1);//6
                    tmp3 = DungeonLevel.getMobByLocation(callerX + 1, callerY + 1);//7
                    break;
                case 7:
                    tmp1 = DungeonLevel.getMobByLocation(callerX, callerY + 1);//6
                    tmp2 = DungeonLevel.getMobByLocation(callerX + 1, callerY + 1);//7
                    tmp3 = DungeonLevel.getMobByLocation(callerX + 1, callerY);//0
                    break;
            }

            if (tmp1 != null) targets.Add(tmp1);
            if (tmp2 != null) targets.Add(tmp2);
            if (tmp3 != null) targets.Add(tmp3);

            double damage = caller.stats.strength * caller.stats.strength + caller.stats.level + 5;

            Mob hold = null;
            for (int i = 0; i < targets.Count; i++)
            {
                hold = targets[i];
                TempPrintOnScreen.addMessage(hold.takeDamage(caller, damage, DamageType.normal).ToString(),
                                             new Vector2(hold.posRect.X + 6, hold.posRect.Y), 900, Color.Red);
                for (int j = 0; j < 8; j++)
                {
                    addParticle(hold.posRect.X, hold.posRect.Y, Game1.rng.Next(0, 19) - 8, Game1.rng.Next(0, 19) - 8, new Vector2(caller.posRect.X, caller.posRect.Y));
                }
            }
        }

        protected void addParticle(int posX, int posY, int offsetX, int offsetY, Vector2 target)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(64, 0, 8, 8), // source rect
                                                        new Vector2(posX + offsetX, posY + offsetY), // starting position
                                                        target, // target position
                                                        (float)(Math.Atan2(target.Y - posY, target.X - posX) + Math.PI / 2), // rotation // backwards vector since the particle is drawn falling
                                                        0.4f, // scale (0.5 = 50%)
                                                        1.8f, // speed
                                                        0, // delay ms
                                                        450)); // lifespan ms
        }

    }
}
