﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public abstract class SpellObject
    {
        protected Animator anim;
        protected Rectangle posRect;
        protected Point origin;
        protected Vector2 velocity;

        public virtual void update(GameTime gameTime)
        {
            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(anim.spriteSheet, posRect, anim.currFrame, anim.tint, anim.rotation, anim.cntr, SpriteEffects.None, anim.drawDepth);
        }
    }
}
