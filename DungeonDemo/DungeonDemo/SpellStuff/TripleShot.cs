﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    class TripleShot : aSpell /////////////////////////// TO BE CONTINUED , NOT FINISHED YET, DO NOT USE IN GAME YET ///////////////////////////////
    {
        Point mapDest_arrowCenter;
        Point mapDest_arrowLeft;
        Point mapDest_arrowRight;
        public int manaCost;
        double damage;
        Mob caster;
        int targetsHit;
        int shootableTargets;
        Mob firstMonster;
        int solidWallTilesCutoff; //currently set to 31 in constructor

        public TripleShot(Mob ca, Point destination)
        {
            // these values are meant to be for the Archer calling this spell
            range = 500; // seemingly endless distance to set up long elaborate shots, but distance to kill the spell at that distance
            degreesWide = 60;
            manaCost = 25;
            damage = 500;
            timeToAnimate = 600;
            shootableTargets = 10;
            solidWallTilesCutoff = 31;

            caster = ca;

            var destTruncated = new Point(destination.X / 32, destination.Y / 32);

            posRect = new Rectangle(ca.mapLocX * 32, ca.mapLocY * 32, 40, 8);//don't know what size we want the arrows to be
            origin = new Point(posRect.Width / 2, posRect.Height / 2);
            velocity = new Vector2(destTruncated.X - ca.mapLocX, destTruncated.Y - ca.mapLocY);
            velocity.Normalize();
            velocity *= 7;

            anim = new SpellAnimator(this, DungeonData.archerArrowSprite1, new Point(40, 8), (float)Math.Atan2(velocity.Y, velocity.X), DungeonData.defaultAnimSpeed, 0.1f);

            firstMonster = null;
            targetsHit = 0;

            DungeonLevel.activeSpells.Add(this);
        }

        public override void Cast(Mob ca)
        {
            List<Mob> affectedMonsters = GetAffectedTargets(ca, true, range, degreesWide);

            int mapX = ca.target.X / 32; //truncating on purpose
            int mapY = ca.target.Y / 32;

        }

        /////////////////////////// TO BE CONTINUED , NOT FINISHED YET, DO NOT USE IN GAME YET ///////////////////////////////




    }
}
