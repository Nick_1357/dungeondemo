﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class MightyCharge : aSpell
    {
        Mob owner;
        public MightyCharge(Mob caller, int travX, int travY, int timeToMove)// this class is just to move the caller and targets. DO NOT ADD TO activeSpells<aSpell> 
        {
            owner = caller;
            double damage = caller.stats.healthMax / 10 + caller.stats.strength * caller.stats.health / 50;

            int adjacentX = 0;// used to check the tiles next to the caller (charge might have varrying distances)
            int adjacentY = 0;// double purpose bonus round: also used to tell damageLine() which direction to move in
            if (travX == 0)
            {
                adjacentY = (travY > 0) ? 1 : -1;
            }
            else
            {
                adjacentX = (travX > 0) ? 1 : -1;
            }

            if (DungeonLevel.grid[caller.mapLocX + adjacentX, caller.mapLocY + adjacentY, 0] < 100) return;//they are charging into a no-walk tile

            Mob first = DungeonLevel.getMobByLocation(caller.mapLocX + adjacentX, caller.mapLocY + adjacentY);
            if (first == null) first = DungeonLevel.getMobByLocation(caller.mapLocX + travX, caller.mapLocY + travY);

            if (first != null)
            {
                if (MoveTargetMobBack(caller, travX, travY, timeToMove))//tries shove all the mobs in the way
                {
                    damageLine(caller, first, damage, adjacentX, adjacentY);
                }
                else//if they can't move, double damage - good jerb, strategist.
                {
                    damageLine(caller, first, damage * 2, adjacentX, adjacentY);
                }
            }
            else
            {
                MoveTargetMobBack(caller, travX, travY, timeToMove);//clear sailing; chart a course for expensive speed.
            }
        }

        private void damageLine(Mob caller, Mob hit, double damage, int moveX, int moveY)
        {
            TempPrintOnScreen.addMessage(hit.takeDamage(owner, damage, DamageType.normal).ToString(),
                                         new Vector2(hit.posRect.X + 8, hit.posRect.Y), 900, Color.Red);
            Mob next = DungeonLevel.getMobByLocation(hit.mapLocX + moveX, hit.mapLocY + moveY);
            if (next != null)
            {
                damageLine(hit, next, damage * 0.85, moveX, moveY);
            }
        }
    }
}
