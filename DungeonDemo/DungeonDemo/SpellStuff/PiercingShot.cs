﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace DungeonDemo
{
    class PiercingShot : aSpell
    {
        Point mapDest;
        public int manaCost;
        double damage;
        Mob caster;
        int targetsHit;
        int shootableTargets;
        Mob firstMonster;
        int solidWallTilesCutoff; //currently set to 31 in constructor

        int particleCounter;
        int delayBtwnParticles;
        int particleX, particleY;

        public PiercingShot(Mob c, Point destination)
        {

            range = 65; // tileWidth(32) x 2 +1
            degreesWide = 1;
            manaCost = 25;
            damage = 500;
            timeToAnimate = 600;
            shootableTargets = 10;
            solidWallTilesCutoff = 31;

            caster = c;

            var destTruncated = new Point(destination.X / 32, destination.Y / 32);
            posRect = new Rectangle(c.mapLocX * 32, c.mapLocY * 32, 20, 6);//don't know what size we want the arrows to be
            origin = new Point(posRect.Width / 2, posRect.Height / 2);
            velocity = new Vector2(destTruncated.X - c.mapLocX, destTruncated.Y - c.mapLocY);
            velocity.Normalize();
            velocity *= 5;

            anim = new SpellAnimator(this, DungeonData.piercingShotSprite, new Point(16, 4), (float)Math.Atan2(velocity.Y, velocity.X), DungeonData.defaultAnimSpeed, 0.1f);

            mapDest = destTruncated;
            firstMonster = null;
            targetsHit = 0;

            particleCounter = 3;
            delayBtwnParticles = 0;

            DungeonLevel.activeSpells.Add(this);
        }

        /*
         * order of operations for spells
         * 
         * player chooses a numeric number 2 above letter Q
         * calls act_piercingshot
         * act_piercingshot Creates a new PiercingShot
         * PiercingShot is initialize in its own constructor
         * act_piercingshot calls piercingshot.cast()
         * Cast calls task functions from aSepll and local unique functions as needed
         * spell the uses update while in the activeSpells list
         * player turn is done
         * 
         */

        public override void Cast(Mob owner)
        {
            int mapX = owner.target.X / 32; //truncating on purpose
            int mapY = owner.target.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, owner.mapLocX - 15, owner.mapLocX + 15); //clamping so range is restricted to 15 tiles
            mapY = (int)MathHelper.Clamp(mapY, owner.mapLocY - 15, owner.mapLocY + 15);

            // make sure player cannot shoot themselves
            if (owner.mapLocX == mapX && owner.mapLocY == mapY)
            {
                switch (owner.facingDirection)
                {
                    case Base.Direction.right:
                        mapX++;
                        break;
                    case Base.Direction.up:
                        mapY--;
                        break;
                    case Base.Direction.left:
                        mapX--;
                        break;
                    case Base.Direction.down:
                        mapY++;
                        break;
                }
            }

            // how long to be in spell animation state
            owner.animState = Base.AnimState.attacking;
            owner.stats.mana -= manaCost;

            //damage calculation
            int lvl = owner.stats.level;
            int str = owner.stats.strength;
            damage = (str * str + lvl) / lvl;
        }


        protected void addParticle(int posX, int posY, int offsetX, int offsetY)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(96, 0, 8, 8), // source rect
                                                        new Vector2(posX + offsetX, posY + offsetY), // starting position
                                                        new Vector2(posX, posY), // target position
                                                        0.0f, // rotation
                                                        0.4f, // scale (0.5 = 50%)
                                                        0.0f, // speed
                                                        0, // delay ms
                                                        275)); // lifespan ms
        }


        #region update logic
        public override void update(GameTime gameTime)
        {
            SoundEffectInstance soundEffectInstance = DungeonData.arrowland.CreateInstance();

            if (particleCounter <= 0)
            {
                particleX = posRect.X;
                particleY = posRect.Y;
                addParticle(particleX, particleY, 1, 1);
                particleCounter = delayBtwnParticles;
            }
            else
            {
                particleCounter -= (int)(gameTime.ElapsedGameTime.Milliseconds);
            }

            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;

            int mapX = (int)(posRect.Center.X / 32);
            int mapY = (int)(posRect.Center.Y / 32);

            for (int i = 0; i < DungeonLevel.monsters.Count; i++)
            {
                Mob target = DungeonLevel.monsters[i];

                if (target != null && target != firstMonster && mapX == target.mapLocX && mapY == target.mapLocY)
                {
                    TempPrintOnScreen.addMessage(target.takeDamage(caster, damage, DamageType.normal).ToString(),
                                         new Vector2(target.posRect.X - 8, target.posRect.Y), 900, Color.Red);
                    firstMonster = target;
                    targetsHit += 1;
                    if(targetsHit == shootableTargets)
                    {
                        DungeonLevel.activeSpells.Remove(this);
                        break;
                    }
                }
                /////////////////////////////////////////////////////////////////// solid wall tile cut-off number ////////////////////
                if(DungeonLevel.grid[mapX,mapY,0] < solidWallTilesCutoff)
                {
                    soundEffectInstance.Play();
                    DungeonLevel.activeSpells.Remove(this);
                    break;
                }
            }

            base.update(gameTime);
        }
        #endregion

    }

}