﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class aSpell_TESTING : aSpell
    {

        public aSpell_TESTING() : base()
        {
            //Initialization chores here
        }

        public void Initialize(Mob owner, List<Monster> currMonsters, Monster clickedMonster)
        {

        }

        public void LoadContent()
        {

        }

        public void CastTest(Mob owner, List<Monster> currMonsters, Monster clickedMonster)
        {
            var mostersToSmash = GetAffectedTargets(owner, true, 65, 1);

            Console.WriteLine("\n......Testing Affected Monsters List builder for MIGHTY CHARGE......");
            Console.WriteLine("......clicked monster(tileX-" + clickedMonster.mapLocX + ", tileY-" + clickedMonster.mapLocY + ") should be included below");
            Console.WriteLine("-------------------------------");
            foreach (Mob monster in mostersToSmash)
            {
                // print the monsters posRect center point to console
                // for debugging purposes
                Console.WriteLine("Monster-index-" + (mostersToSmash.IndexOf(monster) + 1) + " map lock (tileX-" + monster.mapLocX + ", tileY-" + monster.mapLocY + ")");
                Console.WriteLine("------------------------\n");
            }
        }

        public void Update()
        {

        }

        public void Draw()
        {

        }
    }
}
