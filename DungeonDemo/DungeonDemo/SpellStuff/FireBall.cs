﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace DungeonDemo
{
    public class FireBall : aSpell
    {
        Point mapDest;
        Mob sender;
        List<Mob> enemies;

        public FireBall(Mob s, int locX, int locY, int destX, int destY, double dmg)
        {
            sender = s;

            posRect = new Rectangle(locX * 32, locY * 32, 32, 12);//don't know what size we want the arrows to be
            position = new Vector2(posRect.X, posRect.Y);

            origin = new Point(posRect.Width / 2, posRect.Height / 2);
            velocity = new Vector2(destX - locX, destY - locY);
            velocity.Normalize();
            velocity *= 7; // was 10

            anim = new SpellAnimator(this, DungeonData.fireballSprite, new Point(32, 16), (float)Math.Atan2(velocity.Y, velocity.X), DungeonData.defaultAnimSpeed, 0.1f);

            mapDest = new Point(destX, destY);
            damage = dmg;

            enemies = (sender.entityType < 3) ? DungeonLevel.monsters.ToList<Mob>() : DungeonLevel.players.ToList<Mob>();

            DungeonLevel.activeSpells.Add(this);
        }


        public override void update(GameTime gameTime)
        {
            SoundEffectInstance soundEffectInstance = DungeonData.fireball.CreateInstance();
            position += velocity;

            posRect.X = (int)position.X;
            posRect.Y = (int)position.Y;

            int mapX = (int)(posRect.Center.X / 32);
            int mapY = (int)(posRect.Center.Y / 32);

            for (int i = 0; i < enemies.Count; i++)
            {
                if (mapX == enemies[i].mapLocX && mapY == enemies[i].mapLocY)
                {
                    Mob target = enemies[i];
                    TempPrintOnScreen.addMessage(target.takeDamage(sender, damage, DamageType.normal).ToString(),
                                         new Vector2(target.posRect.X - 8, target.posRect.Y), 900, Color.Red);
                    //trying to get fire sound to play.
                    soundEffectInstance.Play();
                    DungeonLevel.activeSpells.Remove(this);
                }
                else if (mapX == mapDest.X && mapY == mapDest.Y)// || DungeonLevel.grid[mapX, mapY, 0] < 50)
                {
                    DungeonLevel.activeSpells.Remove(this);
                }
            }

        }

    }
}
