﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DungeonDemo
{
    /// <summary>
    /// Spell division of responsiblities
    /// - aSpell type classes -> decides damage
    /// </summary>
    public abstract class aSpell
    {
        protected Mob caster;
        protected Animator anim;
        protected Rectangle posRect;
        protected Point origin;
        protected Vector2 velocity;
        protected Vector2 position;
        protected int range;
        protected int degreesWide; //angle to spread this spell, 1 or less is a single line, 359 or more is full circle
        protected double damage;
        protected int timeToAnimate;

        //constructor, each spell uses a unique constructor to set attribute listed above
        protected aSpell() { }

        public virtual void update(GameTime gameTime)
        {
            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(anim.spriteSheet, posRect, anim.currFrame, anim.tint, anim.rotation, anim.cntr, SpriteEffects.None, anim.drawDepth);
        }


        #region Functions for EVERY SPELL
        //all spells will use this same cast function
        public virtual void Cast(Mob owner) { }
        #endregion


        #region Single Task Functions for any spell to use
        // Single Task Functions for each child class spell to use in their casting function
        // NOTE!! these functions should work on both player and monster Mob types
        // NOTE!! these functions should be able to be called from inside a for loop 
        // to effect a group of targets

        /// <summary>
        /// Affected Targets List = Mob enemy types that are in path/range of spell's effects
        /// </summary>
        /// <param name="spellCaster">Mob who is casting the spell</param>
        /// <param name="MonTruePlayFalse">Mob entity type the caster intends to affect with the spell</param>
        /// <param name="spellArcRadius">the pixel length of the spells reach from caster's center</param>
        /// <param name="spellArcAngleDeg">todo: describe spellArcLength parameter on GetAffectedTargets</param>
        public List<Mob> GetAffectedTargets(Mob spellCaster, bool MonTruePlayFalse, int spellArcRadius, int spellArcAngleDeg)
        {
            //this is used if give angle > 1 degree
            int tileCollisionRadius = 16; //if this==Zero, spellArcRadius must overlap tile's center point to add targets to list
            int playerTypesEnd = 2;     // entity types in map are organized into groups, players are types 1 and 2, monsters are types 3 and above

            //needed for directional vector and angle calculations
            Vector2 casterToTargetVec = new Vector2((spellCaster.target.X - spellCaster.posRect.Center.X),
                                                     (spellCaster.target.Y - spellCaster.posRect.Center.Y));

            List<Mob> affectedTargets = new List<Mob>(); // mobs affected list returned when finished

            if (spellArcAngleDeg <= 1) //spell is casting in straight line
            {
                Console.WriteLine("> > > Straight Line Targets < < <");
                #region old logic - limits straight attack by click distance not by radius/reach
                /*
                if ( distBtwn2Points(spellCaster.posRect.Center, spellCaster.target) <= spellArcRadius )
                {
                    affectedTargets = lineOfSightList(spellCaster.posRect.Center.X, spellCaster.posRect.Center.Y,
                                                      spellCaster.target.X, spellCaster.target.Y, MonTruePlayFalse, playerTypesEnd);
                }
                else if ( distBtwn2Points(spellCaster.posRect.Center, spellCaster.target) > spellArcRadius )
                {
                    casterToTargetVec.Normalize(); //directional vec for setting destination x,y
                    //calculate spell final destination from radius length and direction vector
                    int destinX = (int)(spellCaster.posRect.Center.X + (spellArcRadius * casterToTargetVec.X));
                    int destinY = (int)(spellCaster.posRect.Center.Y + (spellArcRadius * casterToTargetVec.Y));

                    affectedTargets = lineOfSightList(spellCaster.posRect.Center.X, spellCaster.posRect.Center.Y,
                                                      destinX, destinY, MonTruePlayFalse, playerTypesEnd);
                }
                */
                #endregion

                //////////////////////////////// NEW --- lock every straight line spell on to the exact radius/reach using the casterToTargetVec /////////////////////////////////////

                casterToTargetVec.Normalize(); //directional vec for setting destination x,y
                                               //calculate spell final destination from radius length and direction vector
                int destinX = (int)(spellCaster.posRect.Center.X + (spellArcRadius * casterToTargetVec.X));
                int destinY = (int)(spellCaster.posRect.Center.Y + (spellArcRadius * casterToTargetVec.Y));

                affectedTargets = lineOfSightList(spellCaster.posRect.Center.X, spellCaster.posRect.Center.Y, destinX, destinY, MonTruePlayFalse, playerTypesEnd);

            }
            else if (spellArcAngleDeg > 1 && spellArcAngleDeg < 359) //spell is casting in a limited arc angle
            {                                                       //ASSUMES:: target clicked is center of anlge provided
                //casterToTargetVec.Normalize(); //needed for each mob in list
                Console.WriteLine("> > > Limtied ARC Targets < < <");
                foreach (Mob mobTarget in DungeonLevel.mobs)
                {
                    //if mob is right type and not dead
                    if ((MonTruePlayFalse && mobTarget.entityType > playerTypesEnd) ||
                            (!MonTruePlayFalse && mobTarget.entityType <= playerTypesEnd))
                    {
                        //if mob is within radius
                        if (distBtwn2Points(spellCaster.target, mobTarget.posRect.Center) < tileCollisionRadius + spellArcRadius)
                        {
                            //calculate the angle between the center vector and the new mobtarget vector
                            Vector2 casterToMobVec = new Vector2((mobTarget.posRect.Center.X - spellCaster.posRect.Center.X),
                                                                 (mobTarget.posRect.Center.Y - spellCaster.posRect.Center.Y));
                            //casterToMobVec.Normalize();

                            //the angle between two vectors above, in degrees
                            double angleBtwn2Vec = (Math.Acos(Vector2.Dot(casterToTargetVec, casterToMobVec) /
                                                              (casterToTargetVec.Length() * casterToMobVec.Length()))) * (180 / Math.PI);
                            //Math.Round(angleBtwn2Vec,0);

                            //XXXXXXXXXXXXXX//Testing Angle Between Two Vectors//XXXXXXXXXXXXXXXXXX//
                            Console.WriteLine("> > > Angle Between Arc Vectors == " + angleBtwn2Vec);
                            //XXXXXXXXXXXXXX//Testing Angle Between Two Vectors//XXXXXXXXXXXXXXXXXX//

                            //if angle is less than HALF of given angle in degrees, then enemy is affected
                            if (angleBtwn2Vec <= (spellArcAngleDeg / 2.0))
                            {
                                affectedTargets.Add(mobTarget);
                            }
                        }
                    }
                }//exit foreach loop

            }
            else if (spellArcAngleDeg >= 359) //spell is casting in full circle
            {
                Console.WriteLine("> > > FULL 360 ARC Targets < < <");
                foreach (Mob mobTarget in DungeonLevel.mobs)
                {
                    if ((MonTruePlayFalse && mobTarget.entityType > playerTypesEnd) ||
                            (!MonTruePlayFalse && mobTarget.entityType <= playerTypesEnd))
                    {
                        // if the distance between the center of the caster and the center the tile 
                        //  is less than half the tile size(16) + the spellArcRadius... spell collision with tile is TRUE
                        if (distBtwn2Points(spellCaster.target, mobTarget.posRect.Center) < tileCollisionRadius + spellArcRadius)
                        {
                            affectedTargets.Add(mobTarget);
                        }
                    }
                }
            }


            return affectedTargets;
        }

        //returns the mobs in this path. a null fills the last spot if it hits a wall
        public List<Mob> lineOfSightList(int sourceX, int sourceY, int destX, int destY, bool monTruePlayFalse, int playerTypesEnd)
        {
            double h = Math.Sqrt((sourceX - destX) * (sourceX - destX) + (sourceY - destY) * (sourceY - destY));
            double opposite = destY - sourceY;
            double adjacent = destX - sourceX;

            double sine = opposite / h;
            double cosine = adjacent / h;

            double locX = sourceX + 0.5;
            double locY = sourceY + 0.5;

            Point loc = new Point(sourceX, sourceY);
            List<Mob> mobsInPath = new List<Mob>();

            while (loc.X != destX || loc.Y != destY)
            {
                locY += sine;
                locX += cosine;

                loc.X = (int)(locX);// + ((cosine>0)? 0.5: -0.5));
                loc.Y = (int)(locY);// + ((sine>0)? 0.5: -0.5));

                if (DungeonLevel.grid[loc.X / 32, loc.Y / 32, 0] == 0)//hit a wall
                {
                    mobsInPath.Add(null);
                    return mobsInPath;
                }

                if (DungeonLevel.grid[loc.X / 32, loc.Y / 32, 1] != 0)
                {
                    Mob tempMob = DungeonLevel.getMobByLocation(loc.X / 32, loc.Y / 32);

                    //entity type boolean gate 'monTruePlayFalse' so spells can be cast from enemies on players types
                    if ((monTruePlayFalse && tempMob.entityType > playerTypesEnd) ||
                         (!monTruePlayFalse && tempMob.entityType <= playerTypesEnd))
                    {
                        if (!mobsInPath.Contains(tempMob)) { mobsInPath.Add(tempMob); }
                    }
                }
            }

            return mobsInPath;//made it
        }

        public int distBtwn2Points(Point start, Point end) //pythagorean distance formula
        {
            // 2D distance calculation
            return (int)Math.Sqrt(Math.Pow(end.X - start.X, 2.0) + Math.Pow(end.X - start.X, 2.0));
        }



        // Spells will all use this to change mana total
        public void ChangeTargetMana(Mob target, int manaChange)
        {
            if (target.stats.mana + manaChange < target.stats.manaMax)
                target.stats.mana += manaChange;
            else
                target.stats.mana = target.stats.manaMax;
        }

        // All Damage to Health of the Mob goes thru function below
        public void ChangeTargetHealth(Mob target, int healthChange)
        {
            if (target.stats.health + healthChange < target.stats.healthMax)
                target.stats.health += healthChange;
            else
                target.stats.health = target.stats.healthMax;
        }

        //helper function for life drain controls
        public float GetPercentHealth(Mob target)
        {
            return target.stats.health / target.stats.healthMax;
        }

        //be able to move a mob from one tile to another
        public bool MoveTargetMobBack(Mob target, int gridXchange, int gridYchange, int mSecToMove)
        {
            if (target == null) return true;//end of the line - getMobByLocation returned null

            Point start = new Point(target.mapLocX, target.mapLocY);
            Point goal = new Point(start.X + gridXchange, start.Y + gridYchange);
            Point dest = State.firstLineOfSightMightyCharge(start.X, start.Y, goal.X, goal.Y);//get the landing location (might be early if something is in the way)

            double theta = Math.Atan2(dest.Y - start.Y, dest.X - start.X);//get heading in radians

            int deltaY = (theta > 0) ? 1 : -1;
            int deltaX = (Math.Abs(theta) < Math.PI / 2) ? 1 : -1;
            if (Math.Abs(theta) < 0.01 || Math.Abs(theta) > Math.PI - 0.01)//edge cases where the mob is only moving in the x or y directions.
            { 
                deltaY = 0;
            }
            if (Math.Abs(Math.Abs(theta) - Math.PI / 2) < 0.01)
            {
                deltaX = 0;
            }

            if (DungeonLevel.grid[dest.X, dest.Y, 0] > 99)// if the landing tile is a walk tile
            {
                DungeonLevel.grid[target.mapLocX, target.mapLocY, 1] = 0;//clearing grid info so everything can move

                bool canMove = MoveTargetMobBack(DungeonLevel.getMobByLocation(dest.X, dest.Y), deltaX, deltaY, mSecToMove);
                if (canMove)
                {
                    target.mapLocX = dest.X;
                    target.mapLocY = dest.Y;
                }
                else
                {
                    target.mapLocX = dest.X - deltaX;//setting one tile away from where un-movable location is
                    target.mapLocY = dest.Y - deltaY;
                }
                target.animState = Base.AnimState.walking;
                target.setTimeInState(300);

                DungeonLevel.grid[target.mapLocX, target.mapLocY, 1] = target.entityType;//writing back grid info after everything has moved
                return canMove;
            }
            else//there is a wall and it cannot move there
            {
                DungeonLevel.grid[target.mapLocX, target.mapLocY, 1] = 0;
                target.mapLocX = dest.X - deltaX;
                target.mapLocY = dest.Y - deltaY;
                target.animState = Base.AnimState.walking;//and on your left, super glue coupling. Everybody wave. up next on the right. Clooney's 5th summer home.
                target.setTimeInState(300);
                DungeonLevel.grid[target.mapLocX, target.mapLocY, 1] = target.entityType;
                return false;
            }
        }

        //change the direction the sprite is facing based on where mob.target is located
        public void ChangeOwnerDirection(Mob owner)
        {
            if (owner.target.Y < owner.posRect.Top) //target is Up/North
            {
                owner.facingDirection = Base.Direction.up;
            }
            else if (owner.posRect.Bottom < owner.target.Y) //target is Down/South
            {
                owner.facingDirection = Base.Direction.down;
            }

            if (owner.target.X < owner.posRect.Left) //target is to the Left/West
            {
                owner.facingDirection = Base.Direction.left;
            }
            else if (owner.posRect.Right < owner.target.X) //target is to the Right/East
            {
                owner.facingDirection = Base.Direction.right;
            }
        }

        // dont let target choose attacks for a number of turns
        public void SkipTargetAttackActions(int numberOfTurns) { }

        // dont let target choose move actions for a num of turns
        public void SkipTargetMoveActions(int numberOfTurns) { }

        // make the target mob skip an int number of turns
        public void SkipTargetTurns(Mob target, int numOfSkippedTurns) { }

        //the following function should time-out after time-elapsed or turns-taken
        public void TempChangeTargetStatus() { }

        /*
        * temporary stun, blind, snare functions go below 
        * NO DAMAGE/HEALTH IS TAKEN BY THESE FUNCTIONS
        * 
        * these functions effect target's status || actions || turns
        * each function is only ONE of these tasks
        */

        // make given mob frozen in place and cannot attack for a number of turns
        public void StunTarget(Mob target) { }

        // make given mob frozen in place but allowed to attack for a number of turns
        public void SnareTarget(Mob target) { }

        // make mob have a high chance of missing their attacks for a number of turns
        public void BlindTarget(Mob target) { }


        /*
         * Create objects of different types needed for the spell
         * i.e. fireball object, arrow object, etc.
         */
        public object NewSpellObject() { return new object(); }

        #endregion


    }
}
