﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    class Hall
    {
        public Point location;
        public Point size;
        public Rectangle boundary;

        public Hall(Point location, Point size)
        {
            this.location = location;
            this.size = size;
            boundary = new Rectangle(location.X - 1, location.Y - 1, size.X + 1, size.Y + 1);
        }
    }
}
