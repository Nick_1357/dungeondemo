﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    class Room
    {
        public Point location;
        public Point size;
        public int depth;
        public Boolean hasNorthHall;
        public Boolean hasEastHall;
        public Boolean hasSouthHall;
        public Boolean hasWestHall;
        public Rectangle boundary;

        public Room(Point location, Point size, int depth)
        {
            this.location = location;
            this.size = size;
            this.depth = depth;
            hasNorthHall = false;
            hasEastHall = false;
            hasSouthHall = false;
            hasWestHall = false;
            boundary = new Rectangle(location.X - 1, location.Y - 1, size.X + 1, size.Y + 1);
        }
    }
}
