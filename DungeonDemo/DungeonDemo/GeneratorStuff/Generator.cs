﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    class Generator
    {
        public Point levelSize;
        public Point minRoomSize;
        public Point maxRoomSize;
        public int minHallLength;
        public int maxHallLength;
        public int minNumRooms;
        public int maxNumRooms;
        public int targetNumRooms;
        public int maxDepth;
        public int initialMobs;

        // every tile has a 1 : altTileFrequency/(number of random tile types) chance of being alternate tile
        // maybe find a better way to handle this later, to allow for different chance values for each tile
        public int altTileFrequency = 70; // frequency of alternate tiles
        public int mobTypes = 2; // how many types of mobs there are

        public int playerRoom = 0;


        Random rand;
        public byte[,,] grid;
        List<Room> rooms;
        List<Hall> halls;

        public enum direction
        {
            up, right, down, left
        }

        public enum tileType
        {
            //wall, floor, wall_connecting, alt_floor_1, alt_floor_2
            wall = DungeonData.tileType.ice_wall,
            wall_connecting = DungeonData.tileType.ice_wall_connecting,
            floor = DungeonData.tileType.ice_floor,
            alt_floor_1 = DungeonData.tileType.ice_snow,
            alt_floor_2 = DungeonData.tileType.ice_snow,
            stairs = DungeonData.tileType.ice_stairs
        }

        public Generator(Point levelSize, Point minRoomSize, Point maxRoomSize, int minHallLength, int maxHallLength,
                            int minNumRooms, int maxNumRooms, int maxDepth, int initialMobs)
        {
            this.levelSize = levelSize;
            this.minRoomSize = minRoomSize;
            this.maxRoomSize = maxRoomSize;
            this.minHallLength = minHallLength;
            this.maxHallLength = maxHallLength;
            this.minNumRooms = minNumRooms;
            this.maxNumRooms = maxNumRooms;
            this.maxDepth = maxDepth;
            this.initialMobs = initialMobs;

            // Values less than these will break things
            if (minRoomSize.X < 3)
                minRoomSize.X = 3;
            if (minRoomSize.Y < 3)
                minRoomSize.Y = 3;
            if (minHallLength < 1)
                minHallLength = 1;

            rand = new Random();
            rooms = new List<Room>();
            halls = new List<Hall>();
        }

        // All generation happens in here
        public byte[,,] generate()
        {
            // Clear any previously generated structures
            rooms.Clear();
            halls.Clear();


            // Choose target number of rooms
            targetNumRooms = rand.Next(minNumRooms, maxNumRooms + 1);

            // Make first room
            Point size = randPoint(minRoomSize, maxRoomSize); // random size room
            Point location = new Point((int)(levelSize.X / 2 - size.X / 2), (int)(levelSize.Y / 2 - size.Y / 2)); // center of level
            rooms.Add(new Room(location, size, 0));

            // Keep expanding until target number of rooms is met OR expanding has failed too many times
            int failLimit = 100; // Maximum consecutive failures before giving up
            int failCount = 0;
            while (rooms.Count < targetNumRooms && failCount < failLimit)
            {
                // Try to expand
                if (expand())
                    failCount = 0;
                else
                    failCount++;
            }
            // Done generating rooms and hallways

            // Make the grid
            grid = new byte[levelSize.X, levelSize.Y, 2];

            // fill grid with wall
            fillArea(new Point(0, 0), levelSize, (byte)tileType.wall);

            // draw rooms
            foreach (Room _room in rooms)
            {
                fillArea(_room.location, _room.size, (byte)tileType.floor);
            }

            // draw halls
            foreach (Hall _hall in halls)
            {
                fillArea(_hall.location, _hall.size, (byte)tileType.floor);
            }

            // TRIM

            // Add connecting walls (MUST come before alternate tiles)
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1) - 2; y++)
                {
                    if (grid[x, y, 0] == (byte)tileType.wall && grid[x, y + 1, 0] == (byte)tileType.floor)
                    {
                        grid[x, y, 0] = (byte)tileType.wall_connecting;
                    }
                }
            }

            // Add alternate tiles
            int randResult;
            for (int x = 1; x < grid.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < grid.GetLength(1) - 1; y++)
                {
                    randResult = rand.Next(0, altTileFrequency);
                    if (randResult < 2)
                    {
                        if (grid[x, y, 0] == (byte)tileType.floor &&
                           grid[x - 1, y - 1, 0] != (byte)tileType.alt_floor_1 && grid[x, y - 1, 0] != (byte)tileType.alt_floor_1 && grid[x + 1, y - 1, 0] != (byte)tileType.alt_floor_1 &&
                           grid[x - 1, y - 1, 0] != (byte)tileType.alt_floor_2 && grid[x, y - 1, 0] != (byte)tileType.alt_floor_2 && grid[x + 1, y - 1, 0] != (byte)tileType.alt_floor_2 &&
                           grid[x - 1, y, 0] != (byte)tileType.alt_floor_1 && grid[x - 1, y, 0] != (byte)tileType.alt_floor_2)
                        {
                            if (randResult == 0)
                                grid[x, y, 0] = (byte)tileType.alt_floor_1;
                            else if (randResult == 1)
                                grid[x, y, 0] = (byte)tileType.alt_floor_2;
                        }
                    }
                }
            }

            // add players
            spawnPlayers();
            //grid[(int)(grid.GetLength(0)/2), (int)(grid.GetLength(1)/2), 1] = 1;//player1
            //grid[(int)(grid.GetLength(0)/2) + 1, (int)(grid.GetLength(1)/2), 1] = 2;//companion

            // spawn initial monsters
            spawnInitialMobs(initialMobs);
            //grid[(int)(grid.GetLength(0) / 2), (int)(grid.GetLength(1) / 2)+ 1, 1] = 3; // monster

            // add stairs
            placeStairs((byte)tileType.stairs);
            //grid[(int)(grid.GetLength(0) / 2) - 1, (int)(grid.GetLength(1) / 2) - 1, 0] = (byte)tileType.stairs;


            return grid; // The completed grid
        }

        // Attempts to expand rooms. Returns false upon failing, returns true if room created
        public Boolean expand()
        {
            // Declare variables to be used
            Point roomExit;
            Point roomSize;
            Point roomLocation;
            int hallLength;
            int roomEntry;

            // Choose a random room
            Room roomToExpand = rooms[rand.Next(0, rooms.Count)];

            // Choose a random direction
            int directionToExpand = rand.Next(0, 4);

            switch (directionToExpand)
            {
                case (int)direction.up:
                    // Check if room expands this direction already
                    if (roomToExpand.hasNorthHall)
                        return false; // Failed
                    // Choose exit point of current room
                    roomExit = new Point(rand.Next(roomToExpand.location.X + 1, roomToExpand.location.X + roomToExpand.size.X - 1),
                                         roomToExpand.location.Y);
                    // Choose hall length
                    hallLength = rand.Next(minHallLength, maxHallLength + 1);
                    // Choose new room size
                    roomSize = new Point(rand.Next(minRoomSize.X, maxRoomSize.X + 1),
                                         rand.Next(minRoomSize.Y, maxRoomSize.Y + 1));
                    // Choose entry point of new room
                    roomEntry = rand.Next(1, roomSize.X - 1);
                    // Set location of new room from these values
                    roomLocation = new Point(roomExit.X - roomEntry, roomExit.Y - (hallLength + roomSize.Y));
                    // Check if valid location                   
                    if (checkStructureLocation(roomLocation, roomSize))
                        return false; // Failed
                    // If reached here, everything is good! Add hall and room
                    halls.Add(new Hall(new Point(roomExit.X, roomExit.Y - hallLength), new Point(1, hallLength)));
                    rooms.Add(new Room(roomLocation, roomSize, roomToExpand.depth + 1));
                    roomToExpand.hasNorthHall = true;
                    return true; // Success
                case (int)direction.right:
                    // Check if room expands this direction already
                    if (roomToExpand.hasEastHall)
                        return false; // Failed
                    // Choose exit point of current room
                    roomExit = new Point(roomToExpand.location.X + roomToExpand.size.X,
                                         rand.Next(roomToExpand.location.Y + 1, roomToExpand.location.Y + roomToExpand.size.Y - 1));
                    // Choose hall length
                    hallLength = rand.Next(minHallLength, maxHallLength + 1);
                    // Choose new room size
                    roomSize = new Point(rand.Next(minRoomSize.X, maxRoomSize.X + 1),
                                         rand.Next(minRoomSize.Y, maxRoomSize.Y + 1));
                    // Choose entry point of new room
                    roomEntry = rand.Next(1, roomSize.Y - 1);
                    // Set location of new room from these values
                    roomLocation = new Point(roomExit.X + hallLength, roomExit.Y - roomEntry);
                    // Check if valid location                   
                    if (checkStructureLocation(roomLocation, roomSize))
                        return false; // Failed
                    // If reached here, everything is good! Add hall and room
                    halls.Add(new Hall(new Point(roomExit.X, roomExit.Y), new Point(hallLength, 1)));
                    rooms.Add(new Room(roomLocation, roomSize, roomToExpand.depth + 1));
                    roomToExpand.hasEastHall = true;
                    return true; // Success
                case (int)direction.down:
                    // Check if room expands this direction already
                    if (roomToExpand.hasSouthHall)
                        return false; // Failed
                    // Choose exit point of current room
                    roomExit = new Point(rand.Next(roomToExpand.location.X + 1, roomToExpand.location.X + roomToExpand.size.X - 1),
                                         roomToExpand.location.Y + roomToExpand.size.Y);
                    // Choose hall length
                    hallLength = rand.Next(minHallLength, maxHallLength + 1);
                    // Choose new room size
                    roomSize = new Point(rand.Next(minRoomSize.X, maxRoomSize.X + 1),
                                         rand.Next(minRoomSize.Y, maxRoomSize.Y + 1));
                    // Choose entry point of new room
                    roomEntry = rand.Next(1, roomSize.X - 1);
                    // Set location of new room from these values
                    roomLocation = new Point(roomExit.X - roomEntry, roomExit.Y + hallLength);
                    // Check if valid location                   
                    if (checkStructureLocation(roomLocation, roomSize))
                        return false; // Failed
                    // If reached here, everything is good! Add hall and room
                    halls.Add(new Hall(new Point(roomExit.X, roomExit.Y), new Point(1, hallLength)));
                    rooms.Add(new Room(roomLocation, roomSize, roomToExpand.depth + 1));
                    roomToExpand.hasSouthHall = true;
                    return true; // Success
                case (int)direction.left:
                    // Check if room expands this direction already
                    if (roomToExpand.hasWestHall)
                        return false; // Failed
                    // Choose exit point of current room
                    roomExit = new Point(roomToExpand.location.X,
                                         rand.Next(roomToExpand.location.Y + 1, roomToExpand.location.Y + roomToExpand.size.Y - 1));
                    // Choose hall length
                    hallLength = rand.Next(minHallLength, maxHallLength + 1);
                    // Choose new room size
                    roomSize = new Point(rand.Next(minRoomSize.X, maxRoomSize.X + 1),
                                         rand.Next(minRoomSize.Y, maxRoomSize.Y + 1));
                    // Choose entry point of new room
                    roomEntry = rand.Next(1, roomSize.Y - 1);
                    // Set location of new room from these values
                    roomLocation = new Point(roomExit.X - (hallLength + roomSize.X), roomExit.Y - roomEntry);
                    // Check if valid location                   
                    if (checkStructureLocation(roomLocation, roomSize))
                        return false; // Failed
                    // If reached here, everything is good! Add hall and room
                    halls.Add(new Hall(new Point(roomExit.X - hallLength, roomExit.Y), new Point(hallLength, 1)));
                    rooms.Add(new Room(roomLocation, roomSize, roomToExpand.depth + 1));
                    roomToExpand.hasWestHall = true;
                    return true; // Success
                default: // This should never happen
                    break;
            }

            return false; // This should never be reached
        }

        // Fill specified area with specified tile
        public void fillArea(Point location, Point size, byte tile)
        {
            for (int y = location.Y; y < location.Y + size.Y; y++)
            {
                for (int x = location.X; x < location.X + size.X; x++)
                {
                    grid[x, y, 0] = tile;
                }
            }
        }

        // Make random point given min and max
        public Point randPoint(Point min, Point max)
        {
            return new Point(rand.Next(min.X, max.X + 1), rand.Next(min.Y, max.Y + 1));
        }

        // Check if structure overlaps existing structure or out of bounds. Return true if error, else return false
        public Boolean checkStructureLocation(Point location, Point size)
        {
            //return false; //testing

            if (location.X < 1 || location.Y < 2 || location.X + size.X > levelSize.X - 1 || location.Y + size.Y > levelSize.Y - 1)
                return true;

            Rectangle structure = new Rectangle(location.X, location.Y, size.X, size.Y);
            // Check rooms
            foreach (Room _room in rooms)
            {
                if (structure.Intersects(_room.boundary))
                {
                    return true;
                }
            }
            // Check halls
            foreach (Hall _hall in halls)
            {
                if (structure.Intersects(_hall.boundary))
                {
                    return true;
                }
            }
            return false;
        }

        public void spawnPlayers()
        {
            playerRoom = rand.Next(0, rooms.Count); // choose random room

            // find center of room
            Point spawnPoint = new Point(rooms[playerRoom].location.X + (int)(rooms[playerRoom].size.X / 2),
                                         rooms[playerRoom].location.Y + (int)(rooms[playerRoom].size.Y / 2));
            grid[spawnPoint.X, spawnPoint.Y, 1] = 1; // player
            grid[spawnPoint.X + 1, spawnPoint.Y, 1] = 2; // partner
        }

        public void spawnInitialMobs(int numMobsToSpawn)
        {
            int mobID_offset = 3; // temporary solution to different mobs per dungeon type
            if (DungeonLevel.type == (byte)DungeonData.dungeonType.ice_dungeon)
                mobID_offset = 5;
            else if (DungeonLevel.type == (byte)DungeonData.dungeonType.earth_dungeon)
                mobID_offset = 7;
            int spawned = 0;
            int spawnRoom;
            while (spawned < numMobsToSpawn)
            {
                spawnRoom = rand.Next(0, rooms.Count); // choose random room
                while (spawnRoom == playerRoom && rooms.Count > 1) // ensure not same room as players
                    spawnRoom = rand.Next(0, rooms.Count);

                // Choose random location in room
                Point spawnPoint = new Point(rooms[spawnRoom].location.X + rand.Next(0, rooms[spawnRoom].size.X),
                                             rooms[spawnRoom].location.Y + rand.Next(0, rooms[spawnRoom].size.Y));

                if (grid[spawnPoint.X, spawnPoint.Y, 1] == 0) // if location is unoccupied
                {
                    grid[spawnPoint.X, spawnPoint.Y, 1] = (byte)(mobID_offset + rand.Next(0, mobTypes)); // place monster
                    spawned++;
                }
            }
        }

        public void spawnMobs(int numMobs)
        {

        }

        public void placeStairs(byte tile)
        {
            int stairsRoom = rand.Next(0, rooms.Count); // choose random room
            while (stairsRoom == playerRoom && rooms.Count > 1) // ensure not same room as players
                stairsRoom = rand.Next(0, rooms.Count);

            // Choose random location in room
            Point stairsPoint = new Point(rooms[stairsRoom].location.X + rand.Next(0, rooms[stairsRoom].size.X),
                                         rooms[stairsRoom].location.Y + rand.Next(0, rooms[stairsRoom].size.Y));

            grid[stairsPoint.X, stairsPoint.Y, 0] = tile; // place stairs
        }

        // Remove unused border space
        private void trim(int minNorthBorder, int minEastBorder, int minSouthBorder, int minWestBorder)
        {
            //Point min = levelSize;
            //Point max = new Point(0, 0);

        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // TEMPORARY, WILL BE MOVED TO SPECIFICDUNGEON IMPLEMENTATIONS LATER
        public byte[,,] overworld_generate()
        {
            // Clear any previously generated structures
            rooms.Clear();
            halls.Clear();

            // Make the grid
            grid = new byte[levelSize.X, levelSize.Y, 2];

            // fill grid with water
            fillArea(new Point(0, 0), levelSize, (byte)DungeonData.tileType.overworld_water);

            // grass
            fillArea(new Point(5, 5), new Point(levelSize.X - 9, levelSize.Y - 10), (byte)DungeonData.tileType.overworld_grass);

            // water edges
            Point topLeft = new Point(4, 5);
            Point bottomRight = new Point(levelSize.X - 4, levelSize.Y - 5);
            grid[topLeft.X, topLeft.Y, 0] = (byte)DungeonData.tileType.overworld_grasswater_topleft;
            grid[topLeft.X, bottomRight.Y, 0] = (byte)DungeonData.tileType.overworld_grasswater_bottomleft;
            grid[bottomRight.X, topLeft.Y, 0] = (byte)DungeonData.tileType.overworld_grasswater_topright;
            grid[bottomRight.X, bottomRight.Y, 0] = (byte)DungeonData.tileType.overworld_grasswater_bottomright;

            // Top edge
            for (int i = topLeft.X + 1; i < bottomRight.X; i++)
                grid[i, topLeft.Y, 0] = (byte)DungeonData.tileType.overworld_grasswater_top;

            // Bottom edge
            for (int i = topLeft.X + 1; i < bottomRight.X; i++)
                grid[i, bottomRight.Y, 0] = (byte)DungeonData.tileType.overworld_grasswater_bottom;

            // Left edge
            for (int i = topLeft.Y + 1; i < bottomRight.Y; i++)
                grid[topLeft.X, i, 0] = (byte)DungeonData.tileType.overworld_grasswater_left;

            // right edge
            for (int i = topLeft.Y + 1; i < bottomRight.Y; i++)
                grid[bottomRight.X, i, 0] = (byte)DungeonData.tileType.overworld_grasswater_right;




            // stairs
            Point fireAnchor = new Point(10, 16);
            Point iceAnchor = new Point(15, 13);
            Point earthAnchor = new Point(20, 16);

            // fire
            grid[fireAnchor.X, fireAnchor.Y, 0] = (byte)DungeonData.tileType.fire_stairs;
            grid[fireAnchor.X - 1, fireAnchor.Y - 1, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X, fireAnchor.Y - 1, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X + 1, fireAnchor.Y - 1, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X - 1, fireAnchor.Y, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X + 1, fireAnchor.Y, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X - 1, fireAnchor.Y + 1, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X, fireAnchor.Y + 1, 0] = (byte)DungeonData.tileType.fire_floor;
            grid[fireAnchor.X + 1, fireAnchor.Y + 1, 0] = (byte)DungeonData.tileType.fire_floor;

            // ice
            grid[iceAnchor.X, iceAnchor.Y, 0] = (byte)DungeonData.tileType.ice_stairs;
            grid[iceAnchor.X - 1, iceAnchor.Y - 1, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X, iceAnchor.Y - 1, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X + 1, iceAnchor.Y - 1, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X - 1, iceAnchor.Y, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X + 1, iceAnchor.Y, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X - 1, iceAnchor.Y + 1, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X, iceAnchor.Y + 1, 0] = (byte)DungeonData.tileType.ice_floor;
            grid[iceAnchor.X + 1, iceAnchor.Y + 1, 0] = (byte)DungeonData.tileType.ice_floor;

            // earth
            grid[earthAnchor.X, earthAnchor.Y, 0] = (byte)DungeonData.tileType.earth_stairs;
            grid[earthAnchor.X - 1, earthAnchor.Y - 1, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X, earthAnchor.Y - 1, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X + 1, earthAnchor.Y - 1, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X - 1, earthAnchor.Y, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X + 1, earthAnchor.Y, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X - 1, earthAnchor.Y + 1, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X, earthAnchor.Y + 1, 0] = (byte)DungeonData.tileType.earth_floor;
            grid[earthAnchor.X + 1, earthAnchor.Y + 1, 0] = (byte)DungeonData.tileType.earth_floor;

            // Players
            //grid[(int)(grid.GetLength(0)/2), (int)(grid.GetLength(1)/2), 1] = 1;//player1
            //grid[(int)(grid.GetLength(0)/2) + 1, (int)(grid.GetLength(1)/2), 1] = 2;//companion
            grid[15, 19, 1] = 1;
            grid[16, 19, 1] = 2;

            return grid;
        }

        // TEMPORARY, WILL BE MOVED TO SPECIFICDUNGEON IMPLEMENTATIONS LATER
        public byte[,,] fire_dungeon_generate()
        {
            // Clear any previously generated structures
            rooms.Clear();
            halls.Clear();


            // Choose target number of rooms
            targetNumRooms = rand.Next(minNumRooms, maxNumRooms + 1);

            // Make first room
            Point size = randPoint(minRoomSize, maxRoomSize); // random size room
            Point location = new Point((int)(levelSize.X / 2 - size.X / 2), (int)(levelSize.Y / 2 - size.Y / 2)); // center of level
            rooms.Add(new Room(location, size, 0));

            // Keep expanding until target number of rooms is met OR expanding has failed too many times
            int failLimit = 100; // Maximum consecutive failures before giving up
            int failCount = 0;
            while (rooms.Count < targetNumRooms && failCount < failLimit)
            {
                // Try to expand
                if (expand())
                    failCount = 0;
                else
                    failCount++;
            }
            // Done generating rooms and hallways

            // Make the grid
            grid = new byte[levelSize.X, levelSize.Y, 2];

            // fill grid with wall
            fillArea(new Point(0, 0), levelSize, (byte)DungeonData.tileType.fire_wall);

            // draw rooms
            foreach (Room _room in rooms)
            {
                fillArea(_room.location, _room.size, (byte)DungeonData.tileType.fire_floor);
            }

            // draw halls
            foreach (Hall _hall in halls)
            {
                fillArea(_hall.location, _hall.size, (byte)DungeonData.tileType.fire_floor);
            }

            // TRIM

            // Add connecting walls (MUST come before alternate tiles)
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1) - 2; y++)
                {
                    if (grid[x, y, 0] == (byte)DungeonData.tileType.fire_wall && grid[x, y + 1, 0] == (byte)DungeonData.tileType.fire_floor)
                    {
                        grid[x, y, 0] = (byte)DungeonData.tileType.fire_wall_connecting;
                    }
                }
            }

            // Add alternate tiles
            int randResult;
            for (int x = 1; x < grid.GetLength(0) - 1; x++)
            {
                for (int y = 1; y < grid.GetLength(1) - 1; y++)
                {
                    randResult = rand.Next(0, altTileFrequency);
                    if (randResult < 2)
                    {
                        if (grid[x, y, 0] == (byte)DungeonData.tileType.fire_floor &&
                           grid[x - 1, y - 1, 0] != (byte)DungeonData.tileType.fire_alt_floor_1 && grid[x, y - 1, 0] != (byte)DungeonData.tileType.fire_alt_floor_1 && grid[x + 1, y - 1, 0] != (byte)DungeonData.tileType.fire_alt_floor_1 &&
                           grid[x - 1, y - 1, 0] != (byte)DungeonData.tileType.fire_alt_floor_2 && grid[x, y - 1, 0] != (byte)DungeonData.tileType.fire_alt_floor_2 && grid[x + 1, y - 1, 0] != (byte)DungeonData.tileType.fire_alt_floor_2 &&
                           grid[x - 1, y, 0] != (byte)DungeonData.tileType.fire_alt_floor_1 && grid[x - 1, y, 0] != (byte)DungeonData.tileType.fire_alt_floor_2)
                        {
                            if (randResult == 0)
                                grid[x, y, 0] = (byte)DungeonData.tileType.fire_alt_floor_1;
                            else if (randResult == 1)
                                grid[x, y, 0] = (byte)DungeonData.tileType.fire_alt_floor_2;
                        }
                    }
                }
            }

            // add players
            spawnPlayers();

            // spawn initial monsters
            spawnInitialMobs(initialMobs);

            // add stairs
            placeStairs((byte)DungeonData.tileType.fire_stairs);

            return grid; // The completed grid
        }

        // TEMPORARY, WILL BE MOVED TO SPECIFICDUNGEON IMPLEMENTATIONS LATER
        public byte[,,] ice_dungeon_generate()
        {
            // Clear any previously generated structures
            rooms.Clear();
            halls.Clear();


            // Choose target number of rooms
            targetNumRooms = rand.Next(minNumRooms, maxNumRooms + 1);

            // Make first room
            Point size = randPoint(minRoomSize, maxRoomSize); // random size room
            Point location = new Point((int)(levelSize.X / 2 - size.X / 2), (int)(levelSize.Y / 2 - size.Y / 2)); // center of level
            rooms.Add(new Room(location, size, 0));

            // Keep expanding until target number of rooms is met OR expanding has failed too many times
            int failLimit = 100; // Maximum consecutive failures before giving up
            int failCount = 0;
            while (rooms.Count < targetNumRooms && failCount < failLimit)
            {
                // Try to expand
                if (expand())
                    failCount = 0;
                else
                    failCount++;
            }
            // Done generating rooms and hallways

            // Make the grid
            grid = new byte[levelSize.X, levelSize.Y, 2];

            // fill grid with wall
            fillArea(new Point(0, 0), levelSize, (byte)DungeonData.tileType.ice_wall);

            // draw rooms
            foreach (Room _room in rooms)
            {
                fillArea(_room.location, _room.size, (byte)DungeonData.tileType.ice_floor);
            }

            // draw halls
            foreach (Hall _hall in halls)
            {
                fillArea(_hall.location, _hall.size, (byte)DungeonData.tileType.ice_floor);
            }

            // TRIM

            // Add connecting walls (MUST come before alternate tiles)
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1) - 2; y++)
                {
                    if (grid[x, y, 0] == (byte)DungeonData.tileType.ice_wall && grid[x, y + 1, 0] == (byte)DungeonData.tileType.ice_floor)
                    {
                        grid[x, y, 0] = (byte)DungeonData.tileType.ice_wall_connecting;
                    }
                }
            }

            

            // add players
            spawnPlayers();

            // spawn initial monsters
            spawnInitialMobs(initialMobs);

            // add stairs
            placeStairs((byte)DungeonData.tileType.ice_stairs);

            // add snow
            foreach (Room _room in rooms)
            {
                // get random point in room
                Point snowpoint = new Point(_room.location.X - 1 + rand.Next(0, _room.size.X + 1),
                                            _room.location.Y - 1 + rand.Next(0, _room.size.Y + 1));
                // check that stairs is not here
                if (grid[snowpoint.X, snowpoint.Y, 0] != (byte)DungeonData.tileType.ice_stairs &&
                    grid[snowpoint.X + 1, snowpoint.Y, 0] != (byte)DungeonData.tileType.ice_stairs &&
                    grid[snowpoint.X, snowpoint.Y + 1, 0] != (byte)DungeonData.tileType.ice_stairs &&
                    grid[snowpoint.X + 1, snowpoint.Y + 1, 0] != (byte)DungeonData.tileType.ice_stairs)
                {
                    if (grid[snowpoint.X, snowpoint.Y, 0] == (byte)DungeonData.tileType.ice_floor)
                        grid[snowpoint.X, snowpoint.Y, 0] = (byte)DungeonData.tileType.ice_snow_topleft;

                    if (grid[snowpoint.X + 1, snowpoint.Y, 0] == (byte)DungeonData.tileType.ice_floor)
                        grid[snowpoint.X + 1, snowpoint.Y, 0] = (byte)DungeonData.tileType.ice_snow_topright;

                    if (grid[snowpoint.X, snowpoint.Y + 1, 0] == (byte)DungeonData.tileType.ice_floor)
                        grid[snowpoint.X, snowpoint.Y + 1, 0] = (byte)DungeonData.tileType.ice_snow_bottomleft;

                    if (grid[snowpoint.X + 1, snowpoint.Y + 1, 0] == (byte)DungeonData.tileType.ice_floor)
                        grid[snowpoint.X + 1, snowpoint.Y + 1, 0] = (byte)DungeonData.tileType.ice_snow_bottomright;
                }
            }

            return grid; // The completed grid
        }

        // TEMPORARY, WILL BE MOVED TO SPECIFICDUNGEON IMPLEMENTATIONS LATER
        public byte[,,] earth_dungeon_generate()
        {
            // Clear any previously generated structures
            rooms.Clear();
            halls.Clear();


            // Choose target number of rooms
            targetNumRooms = rand.Next(minNumRooms, maxNumRooms + 1);

            // Make first room
            Point size = randPoint(minRoomSize, maxRoomSize); // random size room
            Point location = new Point((int)(levelSize.X / 2 - size.X / 2), (int)(levelSize.Y / 2 - size.Y / 2)); // center of level
            rooms.Add(new Room(location, size, 0));

            // Keep expanding until target number of rooms is met OR expanding has failed too many times
            int failLimit = 100; // Maximum consecutive failures before giving up
            int failCount = 0;
            while (rooms.Count < targetNumRooms && failCount < failLimit)
            {
                // Try to expand
                if (expand())
                    failCount = 0;
                else
                    failCount++;
            }
            // Done generating rooms and hallways

            // Make the grid
            grid = new byte[levelSize.X, levelSize.Y, 2];

            // fill grid with wall
            fillArea(new Point(0, 0), levelSize, (byte)DungeonData.tileType.earth_wall);

            // draw rooms
            foreach (Room _room in rooms)
            {
                fillArea(_room.location, _room.size, (byte)DungeonData.tileType.earth_floor);
            }

            // draw halls
            foreach (Hall _hall in halls)
            {
                fillArea(_hall.location, _hall.size, (byte)DungeonData.tileType.earth_floor);
            }

            // TRIM

            // Add connecting walls (MUST come before alternate tiles)
            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1) - 2; y++)
                {
                    if (grid[x, y, 0] == (byte)DungeonData.tileType.earth_wall && grid[x, y + 1, 0] == (byte)DungeonData.tileType.earth_floor)
                    {
                        grid[x, y, 0] = (byte)DungeonData.tileType.earth_wall_connecting;
                    }
                }
            }


            // add players
            spawnPlayers();

            // spawn initial monsters
            spawnInitialMobs(initialMobs);

            // add stairs
            placeStairs((byte)DungeonData.tileType.earth_stairs);

            return grid; // The completed grid
        }

    }
}
