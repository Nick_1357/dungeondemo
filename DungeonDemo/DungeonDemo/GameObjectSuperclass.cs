using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace MyFirstGame
{

    /// <summary>
    /// Enum for list of possible States of the Game Object
    /// Using the states:
    /// [0] Alseep is inactive, should be non-animated.
    /// [1] Alive is active and animated.
    /// [2] Dying is Zero health waiting to exit game.
    /// [3] Dead is Zero health AND removed from game.
    /// </summary>
    public enum GameObjectState
    {
        Asleep,
        Alive,
        Dying,
        Dead,
    }
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameObjectSuperclass
    {
        #region State Data
        //
        /// <summary>
        /// GameObjects should set their state internally, so not set is needed.
        /// An external object or function should not set the state of an object.
        /// for example: a player should set itself to dead when its health reaches zero.
        /// </summary>
        GameObjectState status;
        public GameObjectState Status
        {
            get { return status; }
        }
        //
        #endregion

        #region Graphics Data
        //
        Texture2D objTexture;
        public Texture2D Texture
        {
            get { return objTexture; }
            set { objTexture = value; }
        }

        Rectangle boxCollider;
        public Rectangle BoxCollider
        {
            get { return boxCollider; }
        }

        /// <summary>
        /// Origin is used to find the graphical center of the game object
        /// </summary>
        public Vector2 Origin
        {
            get
            {
                return new Vector2(Texture.Width / 2.0f, Texture.Height / 2.0f);
            }
        }

        /// <summary>
        /// opacity value and Alpha method, useful for fading out dying/destoyed objects
        /// </summary>
        float opacity = 1.0f;
        float Alpha
        {
            get { return opacity; }
        }

        /// <summary>
        /// default color property to work with opacity above
        /// </summary>
        Color color = Color.White;
        protected Color Color
        {
            get
            {
                return color * opacity;
            }
            set { color = value; }
        }

        /// <summary>
        /// Name field to serve as a object tag if needed
        /// </summary>
        String name = "obj";
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// Scale is to scale the texture of the object in obj.Draw()
        /// </summary>
        float scale = 1.0f;
        public float ScaleForDraw
        {
            get { return scale; }
            set { scale = value; }
        }

        //
        #endregion

        #region Physics Data
        //
        /// <summary>
        /// GameObject's Position in the Game World coordinate system
        /// </summary>
        Vector2 position = Vector2.Zero;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// GameObject's Speed updates the position during the game
        /// </summary>
        Vector2 positionSpeed;
        public Vector2 PositionSpeed
        {
            get { return positionSpeed; }
            set { positionSpeed = value; }
        }

        /// <summary>
        /// Rotation is where GameObject is looking
        /// </summary>
        float rotation = 0.0f;
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        /// <summary>
        /// RotationSpeed is how fast the GameObject rotates
        /// </summary>
        float rotationSpeed = 0.0f;
        public float RotationSpeed
        {
            get { return rotationSpeed; }
            set { rotationSpeed = value; }
        }
        //
        #endregion

        #region Death Data
        //
        /// <summary>
        /// The time interval to keep object in 'dying' state before death;
        /// Default is zero, for instant death;
        /// ..sounds sad, I know, but you can code it to be invincible if you want :)
        /// </summary>
        TimeSpan dyingTimeSpan = TimeSpan.Zero;
        public TimeSpan DyingTimeSpan
        {
            get { return dyingTimeSpan; }
            set { dyingTimeSpan = value; }
        }
        float diePercent = 0.0f; //Fully Allive = 0.0; Totally Dead = 1.0;
        //
        #endregion

        /// <summary>
        /// Constructor to set your Object's position and speed
        /// </summary>
        /// <param name="objPosition"></param>
        /// <param name="objPosSpeed"></param>
        public GameObjectSuperclass(Vector2 objPosition, Vector2 objPosSpeed, GameObjectState objectState)
        {
            // TODO: Construct any child components here
            this.position = objPosition;
            this.positionSpeed = objPosSpeed;
            status = objectState;
        }

        /// <summary>
        /// Constructor to set your Object's position
        /// </summary>
        /// <param name="objPosition"></param>
        public GameObjectSuperclass(Vector2 objPosition, GameObjectState objectState)
        {
            this.position = objPosition;
            status = objectState;
            positionSpeed = Vector2.Zero; // constructor to start off stationary
        }

        #region Initialization Functions
        //
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public virtual void Initialize()
        {
            // TODO: Add your initialization code here
            //this.position = new vector2(1.0f, 1.0f);
            //this.positionspeed = new vector2(100.0f, 100.0f);
            //this.objtexture = game.content.load<texture2d>(@"images/tennisball");

            if (!(status == GameObjectState.Asleep))
                status = GameObjectState.Asleep;
        }
        //
        #endregion

        #region Update and Draw Methods
        //
        /// <summary>
        /// Allows the game object to update itself by running this method in game.update()
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public virtual void Update(GameTime gameTime)
        {
            // TODO: Add your update code here
            if (this.status == GameObjectState.Alive)
            {
                this.position += Vector2.Multiply(this.positionSpeed, (float)gameTime.ElapsedGameTime.TotalSeconds);
                this.CalculateBoundingBox();
            }
            else if (this.status == GameObjectState.Dying)
            {
                this.Dying(gameTime);
            }
            else if (this.status == GameObjectState.Dead)
            {
                this.DeathEnd(gameTime);
            }
        }

        /// <summary>
        /// Calculate the box collider of the texture.
        /// updated to the object's position attribute.
        /// ONLY works if object has a texture loaded.
        /// </summary>
        private void CalculateBoundingBox()
        {
            if (objTexture != null)
            {
                boxCollider = new Rectangle((int)this.position.X, (int)this.position.Y,
                                            (int)(objTexture.Width * scale),
                                            (int)(objTexture.Height * scale));
            }
        }

        /// <summary>
        /// Method called when you want the object to enter Dying state;
        /// Death logic flow:
        /// obj.Deathstart() -> obj.Dying() -> obj.DeathEnd()
        /// </summary>
        public void DeathStart()
        {
            if (status == GameObjectState.Alive || status == GameObjectState.Asleep)
            {
                if (dyingTimeSpan != TimeSpan.Zero)
                    status = GameObjectState.Dying;
                else
                    status = GameObjectState.Dead;
            }
        }

        /// <summary>
        /// Logic to perform when the object is dying, but not dead yet;
        /// Death logic flow:
        /// obj.Deathstart() -> obj.Dying() -> obj.DeathEnd()
        /// </summary>
        /// <param name="gameTime">The GameTime object from the active screen</param>
        public virtual void Dying(GameTime gameTime)
        {
            if (diePercent >= 1)
                status = GameObjectState.Dead;
            else
            {
                float dieChange = (float)(gameTime.ElapsedGameTime.TotalMilliseconds /
                    dyingTimeSpan.TotalMilliseconds);
                diePercent += dieChange;
            }
        }

        /// <summary>
        /// Logic to perform when the object is completely dead.
        /// Death logic flow:
        /// obj.Deathstart() -> obj.Dying() -> obj.DeathEnd()
        /// </summary>
        /// <param name="gameTime">The GameTime object from the active screen</param>
        public virtual void DeathEnd(GameTime gameTime) { }

        /// <summary>
        /// Collision using Rectangle.Intersect logic with another GameplayObject
        /// </summary>
        /// <param name="target"></param>
        public virtual Boolean CollisionWithObject(GameObjectSuperclass target)
        {
            if (this.boxCollider.Intersects(target.boxCollider))
            {
                this.positionSpeed *= -1;
                target.positionSpeed *= -1;

                /*
                 * add health fucntion call here
                 */
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// !!! MUST REMOVE: only for pumpkin game, replace logic for superclass !!!
        /// Collision with world edges using integer parameters passed from game.cs
        /// </summary>
        /// <param name="worldMaxX">todo: describe worldMaxX parameter on CollisionWithWorld</param>
        /// <param name="worldMaxY">todo: describe worldMaxY parameter on CollisionWithWorld</param>
        /// <param name="worldMinX">todo: describe worldMinX parameter on CollisionWithWorld</param>
        /// <param name="worldMinY">todo: describe worldMinY parameter on CollisionWithWorld</param>
        public virtual void CollisionWithWorld(int worldMinX, int worldMinY,
                                               int worldMaxX, int worldMaxY)
        {
            // calculate half the scaled measurement of the object
            var halfScaledWidth = (objTexture.Width * scale) / 2;
            var halfScaledHeight = (objTexture.Height * scale) / 2;

            // Check for world edges with parameters from game
            if (this.boxCollider.X > (worldMaxX - halfScaledWidth))
            {
                this.positionSpeed.X *= -1;
                this.position.X = worldMaxX - halfScaledWidth;
            }
            else if (this.boxCollider.X < (worldMinX + halfScaledWidth))
            {
                this.positionSpeed.X *= -1;
                this.position.X = worldMinX + halfScaledWidth;
            }

            if (this.boxCollider.Y > (worldMaxY - halfScaledHeight))
            {
                this.positionSpeed.Y *= -1;
                this.position.Y = worldMaxY - halfScaledHeight;
            }
            else if (this.boxCollider.Y < (worldMinY + halfScaledHeight))
            {
                this.positionSpeed.Y *= -1;
                this.position.Y = worldMinY + halfScaledHeight;
            }
        }

        /// <summary>
        /// Draws the game object in the game world.
        /// MUST CALL spriteBatch.Begin() before calling this in the game.Draw() method
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="spriteBatch"></param>
        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (spriteBatch != null)
            {
                if (objTexture != null)
                {
                    spriteBatch.Draw(objTexture, position, null, Color, rotation, Origin,
                                     scale, SpriteEffects.None, 0.0f);
                }
            }
        }
        //
        #endregion
    }
}
