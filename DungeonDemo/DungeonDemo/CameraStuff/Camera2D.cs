﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public class Camera2D : ICamera2D
    {
        protected int TILE_SIZE = 32; //32x32 sprite tile resolution
        private Vector2 _position;
        protected float _viewportHeight;
        protected float _viewportWidth;
        protected float _levelWidth;
        protected float _levelHeight;

        public Camera2D()
        { }

        #region Properties

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }
        public float Rotation { get; set; }
        public Vector2 Origin { get; set; }
        public float Scale { get; set; }
        public Vector2 ScreenCenter { get; protected set; }
        public Matrix Transform { get; set; }
        public Vector2 Focus { get; set; }
        public float MoveSpeed { get; set; }

        #endregion

        /// <summary>
        /// Called when the GameComponent needs to be initialized. 
        /// </summary>
        /// 
        public void Initialize(int numOfTilesWide, int numOfTilesHigh, Game game)/////////////////////////////////////////NEW added Game game so it can be referenced below
        {
            Scale = Game1.CameraZoomDEF;        //set in game1 constructor, alt. could be passed in as initialize() parameters 
            MoveSpeed = Game1.CameraMoveSpdDEF; //set in game1 constructor, alt. could be passed in as initialize() parameters 

            //init camera's custom viewport to fill the Viewport
            _viewportWidth = game.GraphicsDevice.Viewport.Width;
            _viewportHeight = game.GraphicsDevice.Viewport.Height;

            //level width and height coverted to pixels for camera boundaries
            _levelWidth = (numOfTilesWide * TILE_SIZE);
            _levelHeight = (numOfTilesHigh * TILE_SIZE);

            ScreenCenter = new Vector2(_viewportWidth / 2, _viewportHeight / 2);

            //base.Initialize();
        }

        public void Update(GameTime gameTime)///////////////////////////////////////////// NEW this function is just called in the Game1.Update() for the overriding stuff that caused problems with inheriting from Game.base
        {
            Origin = ScreenCenter / Scale;

            // Create the Transform used by any
            // spritebatch process
            Transform = Matrix.Identity *
                        Matrix.CreateTranslation(-Position.X, -Position.Y, 0) *
                        Matrix.CreateRotationZ(Rotation) *
                        Matrix.CreateTranslation(Origin.X, Origin.Y, 0) *
                        Matrix.CreateScale(new Vector3(Scale, Scale, 1));

            

            // Move the Camera to the position that it needs to go
            var delta = (float)gameTime.ElapsedGameTime.TotalSeconds;

            /*/
            _position.X += (Focus.X - Position.X) * MoveSpeed * delta;
            _position.Y += (Focus.Y - Position.Y) * MoveSpeed * delta;
            //*/
            
            _position = UpdatePosition(_position, delta);
            //*/
            //base.Update(gameTime);
        }

        //JORDANS NEW CODE
        ///<summary>
        /// Calculates the cameras position relative to the edges of the game world
        /// (accounts for scale)
        /// </summary>
        /// <param name="curPosition">todo: describe curPosition parameter on UpdatePosition</param>
        /// <param name="deltaMovement">todo: describe deltaMovement parameter on UpdatePosition</param>
        private Vector2 UpdatePosition(Vector2 curPosition, float deltaMovement)
        {
            Vector2 newPosVec2;
            var deltaX = (Focus.X - Position.X) * MoveSpeed * deltaMovement;
            var deltaY = (Focus.Y - Position.Y) * MoveSpeed * deltaMovement;
            var scaledViewWidth = _viewportWidth / Scale;
            var scaledViewHeight = _viewportHeight / Scale;

            //check X boundaries of level in pixels
            if ( (curPosition.X + deltaX) >= (_levelWidth - scaledViewWidth / 2.0f) )
            {
                newPosVec2.X = _levelWidth - scaledViewWidth / 2.0f;
            }
            else if( (curPosition.X + deltaX) <= (scaledViewWidth / 2.0f) )
            {
                newPosVec2.X = scaledViewWidth / 2.0f;
            }
            //apply deltaX movement if not on boundary
            else{
                newPosVec2.X = curPosition.X + deltaX;
            }

            //check Y boundaries of level in pixels
            if ( (curPosition.Y + deltaY) >= (_levelHeight - scaledViewHeight / 2.0f))
            {
                newPosVec2.Y = _levelHeight - scaledViewHeight / 2.0f;
            }
            else if ( (curPosition.Y + deltaY) <= (scaledViewHeight / 2.0f) )
            {
                newPosVec2.Y = scaledViewHeight / 2.0f;
            }
            //apply deltaY movement if not on boundary
            else {
                newPosVec2.Y = curPosition.Y + deltaY;
            }

            return newPosVec2;
        }
        //JORDANS NEW CODE

        /// <summary>
        /// Determines whether the target is in view given the specified position.
        /// This can be used to increase performance by not drawing objects
        /// directly in the viewport
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="texture">The texture.</param>
        /// <returns>
        ///     <c>true</c> if [is in view] [the specified position]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsInView(Vector2 position, Texture2D texture)
        {
            // If the object is not within the horizontal bounds of the screen
            if ((position.X + texture.Width) < (Position.X - Origin.X) || (position.X) > (Position.X + Origin.X))
                return false;

            // If the object is not within the vertical bounds of the screen
            if ((position.Y + texture.Height) < (Position.Y - Origin.Y) || (position.Y) > (Position.Y + Origin.Y))
                return false;

            // In View
            return true;
        }
    }
}
