﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public abstract class Base
    {
        public enum AnimState
        {
            waiting=0, 
            //walking, waking, sleeping, attacking, casting, dying, dead
            walking, attacking, blocking
        }
        public enum Direction // was right, up, down, left
        {
            up=0,
            right,down,left
        }

        public int mapLocX;
        public int mapLocY;

        public Animator anim;
        public Rectangle posRect;//space the entity takes on the screen
        public Direction facingDirection;//used to draw different frames of the sprite sheet
        public AnimState animState;//used to draw different frames of the sprite sheet


        public virtual void Update(GameTime gameTime)
        {
            posRect.X = mapLocX * anim.frameSize.X;
            posRect.Y = mapLocY * anim.frameSize.Y;
            anim.update(gameTime.ElapsedGameTime.Milliseconds, facingDirection, animState);
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(anim.spriteSheet, posRect, anim.currFrame, anim.tint, anim.rotation , anim.cntr, SpriteEffects.None, anim.drawDepth);
        }
    }
}
