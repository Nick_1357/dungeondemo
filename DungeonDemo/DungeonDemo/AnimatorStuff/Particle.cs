﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class Particle
    {
        protected Texture2D tex;
        protected Rectangle sourceRect;
        protected Vector2 position;
        protected Vector2 target;
        protected float rotation;
        protected Vector2 center;
        protected float scale;
        protected int lifespan;
        protected int delay;
        protected Vector2 velocity;

        public Particle(Texture2D tex, Rectangle sourceRect, Vector2 position, Vector2 target, float rotation, float scale, float speed, int delay, int lifespan)
        {
            this.tex = tex;
            this.sourceRect = sourceRect;
            this.position = position;
            this.target = target;
            this.rotation = rotation;
            this.scale = scale;
            this.delay = delay;
            this.lifespan = lifespan;
            center = new Vector2(sourceRect.Width / 2, sourceRect.Height / 2);

            // Set velocity to move away from source
            Vector2 offset = new Vector2((int)(target.X - position.X), (int)(target.Y - position.Y)); 
            offset.Normalize();
            velocity = -offset * speed;

        }

 
        public void update(GameTime gameTime)
        {
            if (delay > 0)
            {
                delay -= (int)gameTime.ElapsedGameTime.TotalMilliseconds;
            }
            else {
                position.X += (int)velocity.X;
                position.Y += (int)velocity.Y;
                lifespan -= (int)gameTime.ElapsedGameTime.TotalMilliseconds;

                // Check to remove
                if (lifespan <= 0)
                {
                    DungeonLevel.particles.Remove(this);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (delay <= 0)
            {
                spriteBatch.Draw(tex, position, sourceRect, Color.White, rotation, center, scale, SpriteEffects.None, 0.1f);
            }
        }
    }
}
