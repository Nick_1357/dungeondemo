﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public abstract class Animator
    {
        public Texture2D spriteSheet;
        public Point frameSize;//(width,height) of the frame **not used yet. possibly won't be used since other variables are working together to accomlish this variabl's purpose
        public Vector2 cntr;//center of the frame
        public Rectangle currFrame;//(column,row) of the frame

        public Base.AnimState state;
        public Base.Direction direction;
        public float rotation = 0;// mostly used for spells, effects, and tiles. 0 is no rotation, 2PI is effectively no rotation

        public int milSecPerFrame;//time between frame changes
        public int milSecSinceLastDraw;

        public Color tint;
        public float drawDepth;

        public abstract void update(int deltaTime, Base.Direction dir, Base.AnimState aniSt);
    }
}
