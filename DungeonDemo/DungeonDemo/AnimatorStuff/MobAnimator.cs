﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class MobAnimator:Animator
    {
        Mob owner;

        private int aniSequenceLocation;
        private int framesPerSequence;
        private Point[,] spriteSheetKey;// spriteSheetKey[state,direction]
        private int aniSecDir;// either 1 or -1;

        public int timeInState;//used by state changes, so they have enough time to animate and go back to waiting
        public int stateSeqTime;//used by state changes, so they have enough time to animate and go back to waiting
        
        public MobAnimator(Mob o, Texture2D sprSheet, int frameSpeed, Point fSize, int FPSeq, Base.Direction dir, Base.AnimState st, float rot, float dep)
        {
            owner = o;

            spriteSheet = sprSheet;
            spriteSheetKey = new Point[3, 4];
            frameSize = fSize;
            framesPerSequence = FPSeq;
            timeInState = 0;

            cntr = new Vector2(frameSize.X / 2, frameSize.Y / 2);
            currFrame = new Rectangle(0, 0, fSize.X, fSize.Y);

            milSecPerFrame = frameSpeed;
            milSecSinceLastDraw = 0;

            tint = Color.White;
            drawDepth = dep;

            state = st;
            direction = dir;
            rotation = rot;

            aniSecDir = 1;

            //make the sprite sheet key of points
            Array states = Enum.GetValues(typeof(Base.AnimState));
            Array directions = Enum.GetValues(typeof(Base.Direction));
            for (int i = 0; i < 3; i++)//change to states.Length
            {
                for (int j = 0; j < directions.Length; j++) 
                {/////////////******************this is where the code needs to be changed based on the sprite sheet scheme******************/////////////
                    spriteSheetKey[i, j] = new Point(i * frameSize.X * framesPerSequence, j * frameSize.Y);
                }
            }

        }

        public override void update(int deltaTime, Base.Direction dir, Base.AnimState aniSt)
        {
            milSecSinceLastDraw += deltaTime;
            timeInState += deltaTime;

            if (state != aniSt)//reset animations on state change
            {
                aniSecDir = 1;
                aniSequenceLocation = 0;
                timeInState = 0;
                milSecSinceLastDraw = milSecPerFrame;
            }
            direction = dir;
            state = aniSt;


            if(timeInState >= stateSeqTime)//set animation state to waiting after animating other states
            {
                owner.animState = Base.AnimState.waiting;
                stateSeqTime = 1 << 31;//wait indefinitely
            }


            if (milSecSinceLastDraw >= milSecPerFrame)//move frames according to frame rate
            {
                aniSequenceLocation += 1;

                if (aniSequenceLocation >= framesPerSequence)
                {
                    aniSequenceLocation = 0;
                }

                currFrame.Location = spriteSheetKey[(int)state, (int)direction];
                currFrame.X += aniSequenceLocation * frameSize.X;

                milSecSinceLastDraw = 0;
            }
        }//end of update()

    }
}
