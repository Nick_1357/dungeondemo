﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class TileAnimator:Animator
    {
        private int aniSequenceLocation;
        private int framesPerSequence;
        private int aniSecDir;// either 1 or -1 or 0(if not an animated tile);

        private Point pointInSheet;

        public TileAnimator(Texture2D sprSheet, int frameSpeed, Point fSize, int FPSeq, Point ptInSheet, Base.Direction dir, Base.AnimState st)
        {
            spriteSheet = sprSheet;
            frameSize = fSize;
            cntr = new Vector2(frameSize.X / 2, frameSize.Y / 2);
            currFrame = new Rectangle(ptInSheet.X, ptInSheet.Y, fSize.X, fSize.Y);

            framesPerSequence = FPSeq;
            milSecPerFrame = frameSpeed;
            milSecSinceLastDraw = 0;

            tint = Color.White;
            drawDepth = 1f;

            pointInSheet = ptInSheet;

            //not used so much. here for completeness
            state = st;
            direction = dir;
            rotation = 0;

        }//end of constructor

        public override void update(int deltaTime, Base.Direction dir, Base.AnimState aniSt)
        {
            if (framesPerSequence == 1) return;//don't waste time with non-animated tiles

            milSecSinceLastDraw += deltaTime;

            if(milSecSinceLastDraw >= milSecPerFrame)
            {
                aniSequenceLocation += aniSecDir;

                if (aniSequenceLocation >= framesPerSequence)//checking right boundary and turning around
                {
                    aniSequenceLocation = framesPerSequence - 1;
                    aniSecDir = -1;
                }
                else if(aniSequenceLocation <=0)//checking left boundary and turning around
                {
                    aniSequenceLocation = 0;
                    aniSecDir = 1;
                }

                milSecSinceLastDraw = 0;

                currFrame.Location = pointInSheet;
                currFrame.X += aniSequenceLocation * frameSize.X;
            }//end of if(last draw >= sec per frame)
        }//end of update()

    }//end of TileAnimator
}
