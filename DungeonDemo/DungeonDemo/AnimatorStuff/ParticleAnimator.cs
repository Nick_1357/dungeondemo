﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class ParticleAnimator : Animator
    {

        public ParticleAnimator(aSpell so, Texture2D tex, Rectangle source, float rot, int milSecPF, float drawD)
        {

            spriteSheet = tex;
            cntr = new Vector2(frameSize.X / 2, frameSize.Y / 2);
            currFrame = new Rectangle(0, 0, 32, 16);// another temp number replacement for after we have arrows

            rotation = rot;

            milSecPerFrame = milSecPF;
            milSecSinceLastDraw = 0;

            tint = Color.White;
            drawDepth = drawD;

        }

        public override void update(int deltaTime, Base.Direction dir, Base.AnimState aniSt)
        {
            //do nothing for now, until we have arrows to anmate
        }
    }
}
