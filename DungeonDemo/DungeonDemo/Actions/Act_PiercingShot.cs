﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    class Act_PiercingShot : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int timeToAnimate = 500; //milliseconds

            PiercingShot ps = new PiercingShot(caller, caller.target);

            /*
             * TEMPRORARY MANA CHECK, A BETTER DESIGN SHOULD BE USED
            */
            if(ps.manaCost <= caller.stats.mana)
            {
                ps.Cast(caller);
            }
            else
            {
                DungeonLevel.activeSpells.Remove(ps);
                caller.actionsRemaining++; //GAME DESIGN DECISION -- give caller turn back to choose a different spell
                timeToAnimate = 0; //0 ms so game doesnt wait to animate a non-existant spell animation
            }
            /*
             * TEMPRORARY MANA CHECK, A BETTER DESIGN SHOULD BE USED
            */

            caller.setTimeInState(timeToAnimate);

            //goes to turn controller to tell it how long to wait for game to move on from current actioon
            return timeToAnimate;
        }
    }
}
