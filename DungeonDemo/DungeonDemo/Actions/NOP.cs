﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class NOP : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            return 0;
        }
    }
}