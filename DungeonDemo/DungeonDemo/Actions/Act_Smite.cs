﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    class Act_Smite : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, 0, DungeonLevel.mapSize.X - 1);// clamp target to the size of the grid
            mapY = (int)MathHelper.Clamp(mapY, 0, DungeonLevel.mapSize.Y - 1);
            int timeToAnimate = 500; //milliseconds
            int manaCost = 15;

            if (manaCost <= caller.stats.mana) // check if user has enough mana
            {
                Mob target = null;

                for (int i = 0; i < DungeonLevel.mobs.Count; i++)//find out which monster is there
                {
                    if (DungeonLevel.mobs[i].mapLocX == mapX && DungeonLevel.mobs[i].mapLocY == mapY) target = DungeonLevel.mobs[i];
                }

                caller.animState = Base.AnimState.attacking;//change to Base.AnimState.attacking when the sprite sheets are added

                caller.setTimeInState(timeToAnimate);

                if (target == null)
                {
                    return timeToAnimate / 2;//for now it consumes a turn. maybe we should give the turn back for swinging on an empty square.
                }

                int lvl = caller.stats.level;
                int str = caller.stats.strength;
                double damage = str * str / lvl;

                TempPrintOnScreen.addMessage(target.takeDamage(caller, damage, DamageType.normal).ToString(),
                                             new Vector2(target.posRect.X - 8, target.posRect.Y), 900, Color.Red);
                for (int i = 0; i < 20; i++)
                {
                    addParticle(mapX * 32, mapY * 32, Game1.rng.Next(0, 17) - 8, Game1.rng.Next(0, 17) - 8);
                }
            }

            return timeToAnimate;
        }

        protected void addParticle(int posX, int posY, int offsetX, int offsetY)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(32, 0, 8, 8), // source rect
                                                        new Vector2(posX + offsetX, posY + offsetY), // starting position
                                                        new Vector2(posX, posY), // target position
                                                        0.0f, // rotation
                                                        0.7f, // scale (0.5 = 50%)
                                                        3.0f, // speed
                                                        0, // delay ms
                                                        300)); // lifespan ms
        }
    }
}
