﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    class Act_BlindingBurst : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int timeToAnimate = 500; //milliseconds
            int manaCost = 10;

            if (manaCost <= caller.stats.mana) // check if user has enough mana
            {
                caller.stats.mana -= manaCost;

                // Get targets in all 8 adjacent tiles
                List<Mob> targets = new List<Mob>();
                Mob temp;

                // (-1, -1)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX - 1, caller.mapLocY - 1);
                if(temp != null)
                    targets.Add(temp);

                // (0, -1)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX, caller.mapLocY - 1);
                if (temp != null)
                    targets.Add(temp);

                // (1, -1)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX + 1, caller.mapLocY - 1);
                if (temp != null)
                    targets.Add(temp);

                // (-1, 0)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX - 1, caller.mapLocY);
                if (temp != null)
                    targets.Add(temp);

                // (1, 0)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX + 1, caller.mapLocY);
                if (temp != null)
                    targets.Add(temp);

                // (-1, 1)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX - 1, caller.mapLocY + 1);
                if (temp != null)
                    targets.Add(temp);

                // (0, 1)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX, caller.mapLocY + 1);
                if (temp != null)
                    targets.Add(temp);

                // (1, 1)
                temp = DungeonLevel.getMobByLocation(caller.mapLocX + 1, caller.mapLocY + 1);
                if (temp != null)
                    targets.Add(temp);
                
                
                caller.animState = Base.AnimState.attacking;

                caller.setTimeInState(timeToAnimate);
                
                int lvl = caller.stats.level;
                int str = caller.stats.strength;
                double damage = str * str / lvl;

                for (int i = 0; i < targets.Count; i++)
                {
                    TempPrintOnScreen.addMessage(targets[i].takeDamage(caller, damage, DamageType.normal).ToString(),
                                                 new Vector2(targets[i].posRect.X - 8, targets[i].posRect.Y), 900, Color.Red);
                }


                // Create circle of particles
                int numParticles = 20;
                int radius = 8; // was 16
                int callerX = caller.mapLocX * 32;
                int callerY = caller.mapLocY * 32;
                for (int i = 0; i < numParticles; i++)
                {
                    int particleType = 0;
                    if (i % 2 == 1)
                        particleType = 128;

                    int degrees = 360 / numParticles * i;
                    int posX = (int)(callerX + radius * Math.Cos(degrees * Math.PI / 180));
                    int posY = (int)(callerY + radius * Math.Sin(degrees * Math.PI / 180));
                    addParticle(posX, posY, callerX, callerY, particleType);

                    degrees = 360 / numParticles * i;
                    posX = (int)(callerX + radius * 2 * Math.Cos(degrees * Math.PI / 180));
                    posY = (int)(callerY + radius * 2 * Math.Sin(degrees * Math.PI / 180));
                    addParticle(posX, posY, callerX, callerY, particleType);
                }
            }
            else
            {
                caller.actionsRemaining++; //GAME DESIGN DECISION -- give caller turn back to choose a different spell
                timeToAnimate = 0; //0 ms so game doesnt wait to animate a non-existant spell animation
            }

            return timeToAnimate;
        }

        protected void addParticle(int posX, int posY, int targetX, int targetY, int particle)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(particle, 0, 8, 8), // source rect
                                                        new Vector2(posX, posY), // starting position
                                                        new Vector2(targetX, targetY), // target position
                                                        0.0f, // rotation
                                                        0.5f, // scale (0.5 = 50%)
                                                        3.0f, // speed
                                                        0, // delay ms
                                                        300)); // lifespan ms
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(particle, 0, 8, 8), // source rect
                                                        new Vector2(posX, posY), // starting position
                                                        new Vector2(targetX, targetY), // target position
                                                        0.0f, // rotation
                                                        0.5f, // scale (0.5 = 50%)
                                                        1.5f, // speed
                                                        100, // delay ms
                                                        300)); // lifespan ms

        }
    }
}
