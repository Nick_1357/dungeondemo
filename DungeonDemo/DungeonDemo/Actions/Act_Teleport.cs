﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    class Act_Teleport : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, 0, DungeonLevel.mapSize.X - 1);// clamp target to the size of the grid
            mapY = (int)MathHelper.Clamp(mapY, 0, DungeonLevel.mapSize.Y - 1);
            int timeToAnimate = 500; //milliseconds
            int manaCost = 30;
            
            // Check if this should fail for any reason (not enough mana, target isn't empty tile, target out of range)
            if (manaCost <= caller.stats.mana && // caller has enough mana?
                DungeonLevel.grid[mapX, mapY, 0] > 99 && // target tile is walkable?
                DungeonLevel.grid[mapX, mapY, 1] == 0) // target tile is unoccupied?
            {
                caller.stats.mana -= manaCost;

                int initialX = caller.mapLocX; // remember initial position
                int initialY = caller.mapLocY;

                DungeonLevel.grid[caller.mapLocX, caller.mapLocY, 1] = 0;//set to unoccupied
                DungeonLevel.grid[mapX, mapY, 1] = caller.entityType; // move to new position
                caller.mapLocX = mapX;
                caller.mapLocY = mapY;
                caller.posRect.X = mapX*32; // immediately appear in new position
                caller.posRect.Y = mapY*32;

                // Create particles
                addParticle(mapX * 32, mapY * 32, 2, 0);
                addParticle(mapX * 32, mapY * 32, -2, 0);
                addParticle(mapX * 32, mapY * 32, 0, 2);
                addParticle(mapX * 32, mapY * 32, 0, -2);
                addParticle(mapX * 32, mapY * 32, 1, 1);
                addParticle(mapX * 32, mapY * 32, -1, 1);
                addParticle(mapX * 32, mapY * 32, 1, -1);
                addParticle(mapX * 32, mapY * 32, -1, -1);


                caller.animState = Base.AnimState.attacking;

            }
            else
            {
                caller.actionsRemaining++; //GAME DESIGN DECISION -- give caller turn back to choose a different spell
                timeToAnimate = 0; //0 ms so game doesnt wait to animate a non-existant spell animation
            }

            caller.setTimeInState(timeToAnimate);

            //goes to turn controller to tell it how long to wait for game to move on from current actioon
            return timeToAnimate;
        }

        protected void addParticle(int posX, int posY, int offsetX, int offsetY)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(128, 0, 8, 8), // source rect
                                                        new Vector2(posX + offsetX, posY + offsetY), // starting position
                                                        new Vector2(posX, posY), // target position
                                                        0.0f, // rotation
                                                        0.7f, // scale (0.5 = 50%)
                                                        3.0f, // speed
                                                        0, // delay ms
                                                        300)); // lifespan ms
        }
    }
}
