﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class MoveDown : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            DungeonLevel.grid[caller.mapLocX, caller.mapLocY, 1] = 0;//set to unoccupied
            DungeonLevel.grid[caller.mapLocX, caller.mapLocY + 1, 1] = caller.entityType;

            ++caller.mapLocY;
            caller.facingDirection = Base.Direction.down;
            caller.animState = Base.AnimState.walking;

            int timeToAnimate = 300;
            caller.setTimeInState(timeToAnimate);
            return timeToAnimate;
        }
    }
}