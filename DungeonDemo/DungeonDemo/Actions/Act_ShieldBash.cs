﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class Act_ShieldBash : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, caller.mapLocX - 1, caller.mapLocX + 1);//clamping so range is only the 8 immediate tiles around caller
            mapY = (int)MathHelper.Clamp(mapY, caller.mapLocY - 1, caller.mapLocY + 1);

            //first 7 lines can be optimized later. there is some double work going on.

            if (caller.mapLocX == mapX && caller.mapLocY == mapY)//so they don't swing on themselves - weird behavior if it is allowed
            {
                switch (caller.facingDirection)//direction is deciding aim
                {
                    case Base.Direction.right:
                        mapX++;
                        break;
                    case Base.Direction.up:
                        mapY--;
                        break;
                    case Base.Direction.left:
                        mapX--;
                        break;
                    case Base.Direction.down:
                        mapY++;
                        break;
                }
            }
            else//aim is deciding direction
            {
                double theta = Math.Atan2(mapY - caller.mapLocY, mapX - caller.mapLocX);
                // right,up,left,down -> 0,1,2,3 (remember screen y-axis is reversed)
                int direction = (Math.Abs(theta) < Math.PI / 4) ? 0 : (Math.Abs(theta) > 3 * Math.PI / 4) ? 2 : (theta > 0) ? 3 : 1;

                switch (direction)
                {
                    case 0:
                        caller.facingDirection = Base.Direction.right;
                        break;
                    case 1:
                        caller.facingDirection = Base.Direction.up;
                        break;
                    case 2:
                        caller.facingDirection = Base.Direction.left;
                        break;
                    case 3:
                        caller.facingDirection = Base.Direction.down;
                        break;
                }
            }

            int aim = 0;//counter-clockwise from the right: 0-7 for the center strike
            if (mapX == caller.mapLocX)
            {
                aim = (mapY < caller.mapLocY) ? 2 : 6;
            }
            else if (mapY == caller.mapLocY)
            {
                aim = (mapX < caller.mapLocX) ? 4 : 0;
            }
            else if (mapX < caller.mapLocX)
            {
                aim = (mapY < caller.mapLocY) ? 3 : 5;
            }
            else
            {
                aim = (mapY < caller.mapLocY) ? 1 : 7;
            }
            int timeToAnimate = 500;
            caller.animState = Base.AnimState.attacking;
            caller.setTimeInState(timeToAnimate);



            ShieldBash sb = new ShieldBash(caller, aim);

            return timeToAnimate;

        }
    }
}