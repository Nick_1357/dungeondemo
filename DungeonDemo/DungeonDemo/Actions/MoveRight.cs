﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class MoveRight : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            DungeonLevel.grid[caller.mapLocX, caller.mapLocY, 1] = 0;//set to unoccupied
            DungeonLevel.grid[caller.mapLocX + 1, caller.mapLocY, 1] = caller.entityType;

            ++caller.mapLocX;
            caller.facingDirection = Base.Direction.right;
            caller.animState = Base.AnimState.walking;

            int timeToAnimate = 300;
            caller.setTimeInState(timeToAnimate);
            return timeToAnimate;
        }
    }
}
