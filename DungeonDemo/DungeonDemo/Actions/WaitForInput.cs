﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class WaitForInput : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            ++caller.actionsRemaining;
            return 0;
        }
    }
}