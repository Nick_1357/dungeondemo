﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class Act_Cleave : Action
    {

        public override int act(Mob caller, Point tgt)
        {
            
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, caller.mapLocX - 1, caller.mapLocX + 1);//clamping so range is only the 8 immediate tiles around caller
            mapY = (int)MathHelper.Clamp(mapY, caller.mapLocY - 1, caller.mapLocY + 1);

            //first 7 lines can be optimized later. there is some double work going on.

            if (caller.mapLocX == mapX && caller.mapLocY == mapY)//so they don't swing on themselves - weird behavior if it is allowed
            {
                switch (caller.facingDirection)//direction is deciding aim
                {
                    case Base.Direction.right:
                        mapX++;
                        break;
                    case Base.Direction.up:
                        mapY--;
                        break;
                    case Base.Direction.left:
                        mapX--;
                        break;
                    case Base.Direction.down:
                        mapY++;
                        break;
                }
            }
            else//aim is deciding direction
            {
                double theta = Math.Atan2(mapY - caller.mapLocY, mapX - caller.mapLocX);
                // right,up,left,down -> 0,1,2,3 (remember screen y-axis is reversed)
                int direction = (Math.Abs(theta) < Math.PI / 4) ? 0 : (Math.Abs(theta) > 3 * Math.PI / 4) ? 2 : (theta > 0) ? 3 : 1;

                switch (direction)
                {
                    case 0:
                        caller.facingDirection = Base.Direction.right;
                        break;
                    case 1:
                        caller.facingDirection = Base.Direction.up;
                        break;
                    case 2:
                        caller.facingDirection = Base.Direction.left;
                        break;
                    case 3:
                        caller.facingDirection = Base.Direction.down;
                        break;
                }
            }

            int aim = 0;//counter-clockwise from the right: 0-7 for the strike
            if (mapX == caller.mapLocX)
            {
                aim = (mapY < caller.mapLocY) ? 2 : 6;
            }
            else if (mapY == caller.mapLocY)
            {
                aim = (mapX < caller.mapLocX) ? 4 : 0;
            }
            else if (mapX < caller.mapLocX)
            {
                aim = (mapY < caller.mapLocY) ? 3 : 5;
            }
            else
            {
                aim = (mapY < caller.mapLocY) ? 1 : 7;
            }



            Mob first = null;
            Mob second = null;

            switch (aim)
            {
                case 0:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX + 1, caller.mapLocY);//0
                    second = DungeonLevel.getMobByLocation(caller.mapLocX + 2, caller.mapLocY);//0
                    break;
                case 1:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX + 1, caller.mapLocY - 1);//1
                    second = DungeonLevel.getMobByLocation(caller.mapLocX + 2, caller.mapLocY - 2);//1
                    break;
                case 2:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX, caller.mapLocY - 1);//2
                    second = DungeonLevel.getMobByLocation(caller.mapLocX, caller.mapLocY - 2);//2
                    break;
                case 3:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX - 1, caller.mapLocY - 1);//3
                    second = DungeonLevel.getMobByLocation(caller.mapLocX - 2, caller.mapLocY - 2);//3
                    break;
                case 4:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX - 1, caller.mapLocY);//4
                    second = DungeonLevel.getMobByLocation(caller.mapLocX - 2, caller.mapLocY);//4
                    break;
                case 5:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX - 1, caller.mapLocY + 1);//5
                    second = DungeonLevel.getMobByLocation(caller.mapLocX - 2, caller.mapLocY + 2);//5
                    break;
                case 6:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX, caller.mapLocY + 1);//6
                    second = DungeonLevel.getMobByLocation(caller.mapLocX, caller.mapLocY + 2);//6
                    break;
                case 7:
                    first = DungeonLevel.getMobByLocation(caller.mapLocX + 1, caller.mapLocY + 1);//7
                    second = DungeonLevel.getMobByLocation(caller.mapLocX + 2, caller.mapLocY + 2);//7
                    break;
            }

            double damage = ((caller.stats.strength + 5)*(caller.stats.strength + caller.stats.level + 2)) / 2;
            int timeToAnimate = 500;
            caller.animState = Base.AnimState.attacking;
            caller.setTimeInState(timeToAnimate);

            if(first != null)
            {
                TempPrintOnScreen.addMessage(first.takeDamage(caller, damage, DamageType.normal).ToString(),
                                             new Vector2(first.posRect.X + 6, first.posRect.Y), 900, Color.Red);
                for (int i = 0; i < 25; i++)
                {
                    addParticle(first.posRect.X, first.posRect.Y, Game1.rng.Next(0, 19) - 8, Game1.rng.Next(0, 19) - 8, new Vector2(caller.posRect.X, caller.posRect.Y));
                }
            }
            if (second != null)
            {
                TempPrintOnScreen.addMessage(second.takeDamage(caller, damage, DamageType.normal).ToString(),
                                             new Vector2(second.posRect.X + 6, second.posRect.Y), 900, Color.Red);
                for (int i = 0; i < 25; i++)
                {
                    addParticle(second.posRect.X, second.posRect.Y, Game1.rng.Next(0, 19) - 8, Game1.rng.Next(0, 19) - 8, new Vector2(caller.posRect.X, caller.posRect.Y));
                }
            }

            return timeToAnimate;
        }

        protected void addParticle(int posX, int posY, int offsetX, int offsetY, Vector2 target)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(64, 0, 8, 8), // source rect
                                                        new Vector2(posX + offsetX, posY + offsetY), // starting position
                                                        target, // target position
                                                        (float)(Math.Atan2(target.Y-posY,target.X-posX) + Math.PI/2), // rotation // backwards vector since the particle is drawn falling
                                                        0.6f, // scale (0.5 = 50%)
                                                        1.7f, // speed
                                                        0, // delay ms
                                                        450)); // lifespan ms
        }
    }
}