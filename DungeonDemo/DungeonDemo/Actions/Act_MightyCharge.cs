﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class Act_MightyCharge : Action
    {

        public override int act(Mob caller, Point tgt)
        {
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, caller.mapLocX - 2, caller.mapLocX + 2);//clamping so range is restricted to 2 tiles
            mapY = (int)MathHelper.Clamp(mapY, caller.mapLocY - 2, caller.mapLocY + 2);

            if (caller.mapLocX == mapX && caller.mapLocY == mapY)
            {
                switch (caller.facingDirection)
                {
                    case Base.Direction.right:
                        mapX++;
                        break;
                    case Base.Direction.up:
                        mapY--;
                        break;
                    case Base.Direction.left:
                        mapX--;
                        break;
                    case Base.Direction.down:
                        mapY++;
                        break;
                }
            }

            double theta = Math.Atan2(mapY - caller.mapLocY, mapX - caller.mapLocX);
            // right,up,left,down -> 0,1,2,3 (remember screen y-axis is reversed)
            int direction = (Math.Abs(theta) < Math.PI / 4) ? 0 : (Math.Abs(theta) > 3 * Math.PI / 4) ? 2 : (theta > 0) ? 3 : 1;

            switch (direction)//!_!_!_!_!_! warning!: re-using mapX/Y from above !_!_!_!_!_!
            {
                case 0:
                    mapX = 2;
                    mapY = 0;
                    caller.facingDirection = Base.Direction.right;
                    break;
                case 1:
                    mapX = 0;
                    mapY = -2;
                    caller.facingDirection = Base.Direction.up;
                    break;
                case 2:
                    mapX = -2;
                    mapY = 0;
                    caller.facingDirection = Base.Direction.left;
                    break;
                case 3:
                    mapX = 0;
                    mapY = 2;
                    caller.facingDirection = Base.Direction.down;
                    break;
            }


            int timeToAnimate = 300; //milliseconds

            //caller.animState = Base.AnimState.walking;      mighty charge does this later
            //caller.setTimeInState(timeToAnimate); 

            MightyCharge mc = new MightyCharge(caller, mapX, mapY, timeToAnimate);

            //goes to turn controller to tell it how long to wait for game to move on from current actioon
            return timeToAnimate;

        }
    }
}
