﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class Act_FireBall : Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, caller.mapLocX - 3, caller.mapLocX + 3);//clamping so range is restricted to 3 tiles
            mapY = (int)MathHelper.Clamp(mapY, caller.mapLocY - 3, caller.mapLocY + 3);

            if (caller.mapLocX == mapX && caller.mapLocY == mapY)
            {
                switch (caller.facingDirection)
                {
                    case Base.Direction.right:
                        mapX++;
                        break;
                    case Base.Direction.up:
                        mapY--;
                        break;
                    case Base.Direction.left:
                        mapX--;
                        break;
                    case Base.Direction.down:
                        mapY++;
                        break;
                }
            }

            // how long to be in spell animation state
            caller.animState = Base.AnimState.attacking;
            int timeToAnimate = 500; //milliseconds

            caller.setTimeInState(timeToAnimate);

            //damage calculation
            int lvl = caller.stats.level;
            int str = caller.stats.strength;
            double damage = 25;

            FireBall fb = new FireBall(caller, caller.mapLocX, caller.mapLocY, mapX, mapY, damage);

            //goes to turn controller to tell it how long to wait for game to move on from current actioon
            return timeToAnimate;
        }
    }
}
