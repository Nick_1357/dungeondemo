﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public abstract class Action
    {
        //static Action[] actions = new Action[4]; // implement later so everthing doesn't have a list of action objects
        public abstract int act(Mob caller, Point tgt);
    }
}
