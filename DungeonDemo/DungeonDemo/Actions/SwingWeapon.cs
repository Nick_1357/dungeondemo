﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    class SwingWeapon:Action
    {
        public override int act(Mob caller, Point tgt)
        {
            int mapX = tgt.X / 32; //truncating on purpose
            int mapY = tgt.Y / 32;
            mapX = (int)MathHelper.Clamp(mapX, caller.mapLocX - 1, caller.mapLocX + 1);//clamping so range is only the 8 immediate tiles around caller
            mapY = (int)MathHelper.Clamp(mapY, caller.mapLocY - 1, caller.mapLocY + 1);

            if(caller.mapLocX == mapX && caller.mapLocY == mapY)
            {
                switch(caller.facingDirection)
                {
                    case Base.Direction.right:
                        mapX++;
                        break;
                    case Base.Direction.up:
                        mapY--;
                        break;
                    case Base.Direction.left:
                        mapX--;
                        break;
                    case Base.Direction.down:
                        mapY++;
                        break;
                }
            }

            Mob target = DungeonLevel.getMobByLocation(mapX, mapY);

            caller.animState = Base.AnimState.attacking;//change to Base.AnimState.attacking when the sprite sheets are added
            int timeToAnimate = 500;

            caller.setTimeInState(timeToAnimate);

            if (target == null)
            {
                return timeToAnimate;//for now it consumes a turn. maybe we should give the turn back for swinging on an empty square.
            }

            int lvl = caller.stats.level;
            int str = caller.stats.strength;
            double damage = str * str / lvl;

            TempPrintOnScreen.addMessage(target.takeDamage(caller, damage, DamageType.normal).ToString(), 
                                         new Vector2(target.posRect.X-8, target.posRect.Y), 900, Color.Red);

            for (int i = 0; i < 10; i++)
            {
                addParticle(mapX * 32, mapY * 32, Game1.rng.Next(0, 17) - 8, Game1.rng.Next(0, 17) - 8, new Vector2(caller.mapLocX*32, caller.mapLocY*32));
            }

            return timeToAnimate;
        }

        protected void addParticle(int posX, int posY, int offsetX, int offsetY, Vector2 target)
        {
            DungeonLevel.particles.Add(new Particle(DungeonData.particleSprites, // texture
                                                        new Rectangle(64, 0, 8, 8), // source rect
                                                        new Vector2(posX + offsetX, posY + offsetY), // starting position
                                                        target, // target position
                                                        0.0f, // rotation
                                                        0.3f, // scale (0.5 = 50%)
                                                        3.0f, // speed
                                                        0, // delay ms
                                                        300)); // lifespan ms
        }
    }
}
