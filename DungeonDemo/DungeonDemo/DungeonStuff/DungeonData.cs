﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public class DungeonData
    {
        public static int defaultAnimSpeed = 100;

        /// <summary>
        /// HUD sprites
        /// </summary>
        public static Texture2D HUDcontrolsImage;

        /// <summary>
        /// all tiles sprite sheets
        /// </summary>
        public static Texture2D overWorldTiles;
        public static Texture2D BLACKSPACE;
        public static Texture2D WHITESPACE;
        public static Texture2D earthTiles;
        public static Texture2D fireTiles;
        public static Texture2D iceTiles;

        /// <summary>
        /// player class sprite sheets
        /// </summary>
        public static Texture2D knightSpriteSheet;
        public static Texture2D mageSpriteSheet;
        public static Texture2D archerSpriteSheet;
        public static Texture2D healerSpriteSheet;

        public static Texture2D fireballSprite;
        public static Texture2D particleSprites;
        public static Texture2D piercingShotSprite;
        public static Texture2D archerArrowSprite1;

        public static Texture2D earthMon1Sprites;
        public static Texture2D earthMon2Sprites;
        public static Texture2D earthMon3Sprites;

        public static Texture2D fireMon1Sprites;
        public static Texture2D fireMon2Sprites;
        public static Texture2D fireMon3Sprites;

        public static Texture2D iceMon1Sprites;
        public static Texture2D iceMon2Sprites;
        public static Texture2D iceMon3Sprites;

        // Sounds
        public static SoundEffect armorhit;
        public static SoundEffect arrowland;
        public static SoundEffect arrowlaunch;
        public static SoundEffect spellsound;
        public static SoundEffect fireball;


        //maybe define these somewhere else and initialize them in the game's init()
        ///////////////////////////////////////////////health, mana, strength, intellect
        public static Stats earthMon1Stats = new Stats(1, 10, 0, 2, 1);
        public static Stats earthMon2Stats;
        public static Stats earthMon3Stats;

        public static Stats fireMon1Stats = new Stats(1, 10, 0, 2, 1);
        public static Stats fireMon2Stats;
        public static Stats fireMon3Stats;

        public static Stats iceMon1Stats = new Stats(1, 10, 0, 2, 1);
        public static Stats iceMon2Stats;
        public static Stats iceMon3Stats;


        ///////////////////////////////////////////////// everything below here required for SpecificDungeon

        // number of floors of each multi-floor dungeon type
        public static int fireDungeonFloors = 2;
        public static int iceDungeonFloors = 3;
        public static int earthDungeonFloors = 4;

        
        public enum dungeonType
        {
            overworld = 0,
            fire_dungeon = 1,
            fire_boss_level = 2,
            ice_dungeon = 3,
            ice_boss_level = 4,
            earth_dungeon = 5,
            earth_boss_level = 6

        }


        // this order can be changed at any time and won't mess anything up, as long as:
        // -Unwalkable tiles range from 0-99
        // -Walkable tiles range from 100-255
        public enum tileType
        {
            // unwalkable tiles:
            empty = 0,
            fire_wall = 10,
            fire_wall_connecting = 11,
            ice_wall = 12,
            ice_wall_connecting = 13,
            earth_wall = 14,
            earth_wall_connecting = 15,
            overworld_water = 50,
            overworld_grasswater_bottom = 51,
            overworld_grasswater_right = 52,
            overworld_grasswater_left = 53,
            overworld_grasswater_top = 54,
            overworld_grasswater_topleft = 55,
            overworld_grasswater_topright = 56,
            overworld_grasswater_bottomleft = 57,
            overworld_grasswater_bottomright = 58,

            // walkable tiles:
            overworld_grass = 100,
            fire_floor = 120,
            fire_alt_floor_1 = 121, // lava cracks
            fire_alt_floor_2 = 122, // dark cracks
            fire_stairs = 123,
            ice_floor = 130,
            ice_snow = 131,
            ice_snow_bottom = 132,
            ice_snow_right = 133,
            ice_snow_left = 134,
            ice_snow_top = 135, 
            //ice_snow_backslash = 136,
            //ice_snow_slash = 137,
            ice_snow_bottomright_corner = 138,
            ice_snow_bottomleft_corner = 139,
            ice_snow_topright_corner = 140,
            ice_snow_topleft_corner = 141,
            ice_snow_topleft = 142,
            ice_snow_topright = 143,
            ice_snow_bottomleft = 144,
            ice_snow_bottomright = 145,
            ice_stairs = 146,
            earth_floor = 150,
            earth_stairs = 151
        }

    }
}
