﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public abstract class SpecificDungeon
    {
        public int floors; // number of floors in dungeon
        public Texture2D[] monsterTextures;

        // Check conditions to advance to different level. Load that level if condition met.
        public abstract bool checkTransitions();

        // Perform generation for this dungeon type
        public abstract void doGeneration();


    }
}
