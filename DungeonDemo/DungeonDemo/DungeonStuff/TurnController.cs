﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class TurnController
    {
        int turnIndex = 0;

        int milliSecToAnimateAction = 0;


        public void passGameTime(int deltaTime)//give each Mob a turn
        {
            milliSecToAnimateAction -= deltaTime;//deltaTime is usually about 16.
            if (milliSecToAnimateAction < 0)
            {
                if (turnIndex < DungeonLevel.mobs.Count)
                {
                    milliSecToAnimateAction = DungeonLevel.mobs[turnIndex].takeAction();//each mob's action should handle the number of actions they have and can take

                    if (DungeonLevel.mobs[turnIndex].actionsRemaining <= 0)//if they took their actions
                    {
                        DungeonLevel.mobs[turnIndex].actionsRemaining = DungeonLevel.mobs[turnIndex].actionsPerTurn;
                        turnIndex++;
                    }
                }
                else//loop back to the player
                {
                    turnIndex = 0;

                    bool allSleeping = true;//checking for regeneration (out of combat status)
                    for (int i = 0; i < DungeonLevel.monsters.Count; i++)
                    {
                        if (!DungeonLevel.monsters[i].isSleeping())
                        {
                            allSleeping = false;
                            break;
                        }
                    }
                    if (allSleeping)
                    {

                        Player p = DungeonLevel.player;
                        Player c = DungeonLevel.companion;

                        //regen health
                        int regen = (int)(0.08 * p.stats.healthMax);
                        p.stats.health += regen;
                        TempPrintOnScreen.addMessage(regen.ToString(), new Vector2(p.posRect.X + 2, p.posRect.Y), 900, Color.LawnGreen);

                        //regen mana
                        regen = (int)(0.02 * p.stats.manaMax);
                        p.stats.mana += regen;
                        TempPrintOnScreen.addMessage(regen.ToString(), new Vector2(p.posRect.X + 20, p.posRect.Y), 900, Color.DodgerBlue);
                        
                        if (p.stats.health > p.stats.healthMax)
                        {
                            p.stats.health = p.stats.healthMax;
                        }
                        if (p.stats.mana > p.stats.manaMax)
                        {
                            p.stats.mana = p.stats.manaMax;
                        }

                        if(c.stats.health >=0)
                        {
                            regen = (int)(0.08 * c.stats.healthMax);
                            c.stats.health += regen;
                            TempPrintOnScreen.addMessage(regen.ToString(), new Vector2(c.posRect.X + 2, c.posRect.Y), 900, Color.LawnGreen);

                            regen = (int)(0.02 * p.stats.manaMax);
                            c.stats.mana += regen;
                            TempPrintOnScreen.addMessage(regen.ToString(), new Vector2(c.posRect.X + 20, c.posRect.Y), 900, Color.DodgerBlue);

                            if (c.stats.health > c.stats.healthMax)
                            {
                                c.stats.health = c.stats.healthMax;
                            }
                            if (c.stats.mana > c.stats.manaMax)
                            {
                                c.stats.mana = c.stats.manaMax;
                            }
                        }
                    }//end of all sleeping check
                }//end of (turnIndex < mobs.Count) else{}
            }//end of if(millsecToAnimateAction)
        }//end of passGameTime()
    }//end of TurnController
}
