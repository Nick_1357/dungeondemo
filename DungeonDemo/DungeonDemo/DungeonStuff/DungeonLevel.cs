﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class DungeonLevel
    {
        public static List<Mob> mobs;
        public static List<Player> players;
        public static List<Monster> monsters;

        public static List<Tile> tiles;
        public static List<Tile> walkTiles;
        public static List<Tile> noWalkTiles;

        public static List<aSpell> activeSpells;
        public static List<Particle> particles;

        public static byte[,,] grid; // 3D array of room state
        public static Point mapSize;

        public static Player player;
        public static Player companion;

        public static TurnController turnController;

        // TESTING, need these to create next levels
        public static int level;
        public static int type;
        public SpecificDungeon specificDungeon;
        //Texture2D tileSheet;
        //Texture2D[] monsterSheets;
        Generator gen;

        public static int transitionTarget;
        public static int levelTarget;

        public DungeonLevel(int lvl, Player p1, Player cmpnion, int dungeonType) // string loadMSG, Texture2D tileSheet, Texture2D[] monsterSheets)
        {
            player = p1;
            companion = cmpnion;
            //((AI)companion.controller).targetMob = null;


            // TESTING, need these to create next levels
            level = lvl;
            //this.tileSheet = tileSheet;
            //this.monsterSheets = monsterSheets;

            mobs = new List<Mob>();
            players = new List<Player>();
            monsters = new List<Monster>();

            tiles = new List<Tile>();
            walkTiles = new List<Tile>();
            noWalkTiles = new List<Tile>();

            activeSpells = new List<aSpell>();
            particles = new List<Particle>();

            turnController = new TurnController();


            /// <summary>
            /// Master Switch Statement for Dungeon Generator
            /// Changes each map dynamically
            /// </summary>
            type = dungeonType;
            switch (type)
            {
                case (byte)DungeonData.dungeonType.overworld:
                    specificDungeon = new Overworld();
                    gen = new Generator(new Point(30, 30), // level size // 100 100
                                          new Point(3, 3), // min room size // 3 3
                                          new Point(10, 10), // max room size // 10 10
                                          3, // min hall length // 3
                                          7, // max hall length // 7
                                          10, // min num rooms // 20
                                          10, // max num rooms // 30
                                          10, // max depth // 10
                                          10); // initial mobs to spawn
                    grid = gen.overworld_generate();
                    break;
                case (byte)DungeonData.dungeonType.fire_dungeon:
                    specificDungeon = new FireDungeon();
                    gen = new Generator(new Point(100, 100), // level size // 100 100
                                          new Point(3, 3), // min room size // 3 3
                                          new Point(10, 10), // max room size // 10 10
                                          3, // min hall length // 3
                                          7, // max hall length // 7
                                          10, // min num rooms // 10
                                          10, // max num rooms // 10
                                          10, // max depth // 10
                                          10); // initial mobs to spawn
                    grid = gen.fire_dungeon_generate();
                    break;
                case (byte)DungeonData.dungeonType.fire_boss_level:
                    // specificDungeon = new FireBossLevel();
                    break;
                case (byte)DungeonData.dungeonType.ice_dungeon:
                    gen = new Generator(new Point(100, 100), // level size // 100 100
                                          new Point(6, 6), // min room size // 3 3
                                          new Point(10, 10), // max room size // 10 10
                                          3, // min hall length // 3
                                          5, // max hall length // 7
                                          5, // min num rooms // 20
                                          7, // max num rooms // 30
                                          3, // max depth // 10
                                          10); // initial mobs to spawn
                    specificDungeon = new IceDungeon();
                    grid = gen.ice_dungeon_generate();
                    break;
                case (byte)DungeonData.dungeonType.ice_boss_level:
                    // specificDungeon = new IceBossLevel();
                    break;
                case (byte)DungeonData.dungeonType.earth_dungeon:
                    gen = new Generator(new Point(100, 100), // level size // 100 100
                                          new Point(3, 3), // min room size // 3 3
                                          new Point(7, 7), // max room size // 10 10
                                          4, // min hall length // 3
                                          10, // max hall length // 7
                                          6, // min num rooms // 20
                                          8, // max num rooms // 30
                                          4, // max depth // 10
                                          10); // initial mobs to spawn
                    specificDungeon = new EarthDungeon();
                    grid = gen.earth_dungeon_generate();
                    break;
                case (byte)DungeonData.dungeonType.earth_boss_level:
                    // specificDungeon = new EarthBossLevel();
                    break;

                default: // This should never happen. Load overworld and notify in console
                    Console.WriteLine("Undefined dungeonType {0}", type);
                    // specificDungeon = new Overworld();
                    break;

            }

            mapSize = new Point(grid.GetLength(0), grid.GetLength(1));

            companion.setPather();//See AI constructor for why it's here

            byte tileType;
            byte mobType;
            int tileSize = 32;
            Rectangle worldSpaceBox;//becomes each item's destination rectangle for drawing on the screen

            for (int i = 0; i < mapSize.X; i++)
            {
                for (int j = 0; j < mapSize.Y; j++)
                {
                    worldSpaceBox = new Rectangle(i * tileSize, j * tileSize, tileSize, tileSize);

                    tileType = grid[i, j, 0];
                    mobType = grid[i, j, 1];


                    switch (tileType)
                    {
                        /*case 0: // wall
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 128)));
                            break;
                        case 1: // connecting wall
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 128)));
                            break;
                        case 100: // floor
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 32)));
                            break;
                        case 101: // alt tile 1 (red)
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 160)));
                            break;
                        case 102: // alt tile 2 (black)
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(96, 160)));
                            break;
                        case 255: // stairs
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 128)));
                            break;*/

                        // Unwalkable:
                        case (byte)DungeonData.tileType.empty:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.BLACKSPACE, 0, 0, new Point(tileSize, tileSize), new Point(0, 0)));
                            break;
                        case (byte)DungeonData.tileType.fire_wall:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 128)));
                            break;
                        case (byte)DungeonData.tileType.fire_wall_connecting:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 128)));
                            break;
                        case (byte)DungeonData.tileType.ice_wall:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 128)));
                            break;
                        case (byte)DungeonData.tileType.ice_wall_connecting:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 128)));
                            break;
                        case (byte)DungeonData.tileType.earth_wall:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.earthTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 128)));
                            break;
                        case (byte)DungeonData.tileType.earth_wall_connecting:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.earthTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 128)));
                            break;
                        case (byte)DungeonData.tileType.overworld_water:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(384, 32)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_bottom:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(416, 0)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_right:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(448, 0)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_left:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(480, 0)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_top:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(416, 32)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_topleft:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(448, 64)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_topright:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(480, 64)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_bottomleft:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(448, 96)));
                            break;
                        case (byte)DungeonData.tileType.overworld_grasswater_bottomright:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(480, 96)));
                            break;

                        // Walkable:
                        case (byte)DungeonData.tileType.overworld_grass:
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.overWorldTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 0)));
                            break;
                        case (byte)DungeonData.tileType.fire_floor:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 32)));
                            break;
                        case (byte)DungeonData.tileType.fire_alt_floor_1:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 160)));
                            break;
                        case (byte)DungeonData.tileType.fire_alt_floor_2:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(96, 160)));
                            break;
                        case (byte)DungeonData.tileType.fire_stairs:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.fireTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 128)));
                            break;
                        case (byte)DungeonData.tileType.ice_floor:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 32)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 0)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_bottom:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 0)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_right:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 0)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_left:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(96, 0)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_top:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 32)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_bottomright_corner:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 64)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_bottomleft_corner:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 64)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_topright_corner:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 96)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_topleft_corner:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(32, 96)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_topleft:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 64)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_topright:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(96, 64)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_bottomleft:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 96)));
                            break;
                        case (byte)DungeonData.tileType.ice_snow_bottomright:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(96, 96)));
                            break;
                            


                        case (byte)DungeonData.tileType.ice_stairs:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.iceTiles, 0, 0, new Point(tileSize, tileSize), new Point(64, 128)));
                            break;
                        case (byte)DungeonData.tileType.earth_floor:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.earthTiles, 0, 0, new Point(tileSize, tileSize), new Point(0, 32)));
                            break;
                        case (byte)DungeonData.tileType.earth_stairs:
                            walkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.earthTiles, 0, 0, new Point(tileSize, tileSize), new Point(64,128)));
                            break;
                            
                        default://it should not get to default. filling with black squares so problems should be obvious. might make a pink debug tile to drive this home
                            Console.WriteLine("Undefined tiletype {0} at ({1}, {2})", tileType, i, j); // debug
                            noWalkTiles.Add(new Tile(tileType, i, j, worldSpaceBox, DungeonData.BLACKSPACE, 0, 0, new Point(tileSize, tileSize), new Point(0, 0)));
                            break;
                    }

                    switch (mobType)
                    {
                        case 0://nothing
                            break;
                        case 1://player
                            player.mapLocX = i;
                            player.mapLocY = j;
                            player.posRect = worldSpaceBox;
                            break;
                        case 2://companion
                            companion.mapLocX = i;
                            companion.mapLocY = j;
                            companion.posRect = worldSpaceBox;
                            break;
                        case 3:// Fire monster 1
                            monsters.Add(new Monster(DungeonData.fireMon1Stats, 3, i, j, worldSpaceBox, DungeonData.fireMon1Sprites, DungeonData.defaultAnimSpeed,
                                                        new Point(tileSize, tileSize), 4));// last parameter is animation frames in the animation sequence
                            break;
                        case 4: // Fire monster 2
                            monsters.Add(new Monster(DungeonData.fireMon1Stats, 3, i, j, worldSpaceBox, DungeonData.fireMon2Sprites, DungeonData.defaultAnimSpeed,
                                                        new Point(tileSize, tileSize), 4));// last parameter is animation frames in the animation sequence
                            break;
                        case 5:// Ice monster 1
                            monsters.Add(new Monster(DungeonData.iceMon1Stats, 3, i, j, worldSpaceBox, DungeonData.iceMon1Sprites, DungeonData.defaultAnimSpeed,
                                                        new Point(tileSize, tileSize), 4));// last parameter is animation frames in the animation sequence
                            break;
                        case 6: // Ice monster 2
                            monsters.Add(new Monster(DungeonData.iceMon1Stats, 3, i, j, worldSpaceBox, DungeonData.iceMon2Sprites, DungeonData.defaultAnimSpeed,
                                                        new Point(tileSize, tileSize), 4));// last parameter is animation frames in the animation sequence
                            break;
                        case 7:// Earth monster 1
                            monsters.Add(new Monster(DungeonData.earthMon1Stats, 8, i, j, worldSpaceBox, DungeonData.earthMon1Sprites, DungeonData.defaultAnimSpeed,
                                                        new Point(tileSize, tileSize), 4));// last parameter is animation frames in the animation sequence
                            break;
                        case 8: // Earth monster 2
                            monsters.Add(new Monster(DungeonData.earthMon1Stats, 9, i, j, worldSpaceBox, DungeonData.earthMon2Sprites, DungeonData.defaultAnimSpeed,
                                                        new Point(tileSize, tileSize), 4));// last parameter is animation frames in the animation sequence
                            break;
                        //case 4://second monster type... and so on
                        //break;
                        default://should not get to default. does nothing for now, though
                            break;
                    }
                }
            }//end of filling game data from map data

            players.Add(player);//forcing player to be at players[0] and mobs[0] by pulling them out here
            players.Add(companion);

            //fill the rest of the masking lists
            tiles.AddRange(walkTiles);
            tiles.AddRange(noWalkTiles);

            mobs.AddRange(players);
            mobs.AddRange(monsters);

            Console.WriteLine("Starting {0} floor {1}", ((DungeonData.dungeonType)type).ToString(), level);
        }//end of constructor


        public static void Update(GameTime gameTime)
        {
            turnController.passGameTime(gameTime.ElapsedGameTime.Milliseconds);
        }

        public static Mob getMobByLocation(int x, int y)
        {
            for (int i = 0; i < mobs.Count; i++)
            {
                if (mobs[i].mapLocX == x & mobs[i].mapLocY == y)
                {
                    return mobs[i];
                }
            }
            return null;
        }

        public static double getMobsDist(Mob a, Mob b)
        {
            int x = a.mapLocX - b.mapLocX;
            int y = a.mapLocY - b.mapLocY;
            return Math.Sqrt(x * x + y * y);
        }

        // TESTING, create next level
        public DungeonLevel getNextLevel()
        {
            return new DungeonLevel(levelTarget, player, companion, transitionTarget);
        }

    }
}
