﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    class EarthDungeon : SpecificDungeon
    {

        public EarthDungeon()
        {
            floors = DungeonData.earthDungeonFloors; // number of floors in dungeon

        }

        // Check conditions to advance to different level. Return true and set transition values if condition met.
        public override bool checkTransitions()
        {
            if (DungeonLevel.grid[DungeonLevel.player.mapLocX, DungeonLevel.player.mapLocY, 0] == (byte)DungeonData.tileType.earth_stairs) // if player on stairs
            {
                if (DungeonLevel.level < floors) // if not at max floors for dungeon type
                {
                    DungeonLevel.transitionTarget = (int)DungeonData.dungeonType.earth_dungeon;
                    DungeonLevel.levelTarget = DungeonLevel.level + 1;
                }
                else
                {
                    DungeonLevel.transitionTarget = (int)DungeonData.dungeonType.overworld;
                    DungeonLevel.levelTarget = 1;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        // Perform generation for this dungeon type
        public override void doGeneration()
        {

        }


    }
}
