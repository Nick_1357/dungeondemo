﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    class Overworld : SpecificDungeon
    {

        public Overworld()
        {
            floors = DungeonData.fireDungeonFloors; // number of floors in dungeon

        }

        // Check conditions to advance to different level. Return true and set transition values if condition met.
        public override bool checkTransitions()
        {
            if (DungeonLevel.grid[DungeonLevel.player.mapLocX, DungeonLevel.player.mapLocY, 0] == (byte)DungeonData.tileType.fire_stairs) // if player on stairs
            {
                DungeonLevel.transitionTarget = (int)DungeonData.dungeonType.fire_dungeon;
                DungeonLevel.levelTarget = 1;
                return true;
            }
            else if (DungeonLevel.grid[DungeonLevel.player.mapLocX, DungeonLevel.player.mapLocY, 0] == (byte)DungeonData.tileType.ice_stairs) // if player on stairs
            {
                DungeonLevel.transitionTarget = (int)DungeonData.dungeonType.ice_dungeon;
                DungeonLevel.levelTarget = 1;
                return true;
            }
            else if (DungeonLevel.grid[DungeonLevel.player.mapLocX, DungeonLevel.player.mapLocY, 0] == (byte)DungeonData.tileType.earth_stairs) // if player on stairs
            {
                DungeonLevel.transitionTarget = (int)DungeonData.dungeonType.earth_dungeon;
                DungeonLevel.levelTarget = 1;
                return true;
            }
            else
            {
                return false;
            }
        }

        // Perform generation for this dungeon type
        public override void doGeneration()
        {

        }


    }
}
