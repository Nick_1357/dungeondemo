﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pathing
{
    public class PathFinder//finds path with a flood search that prioritizes closer fringe nodes for expansion
    {
        private class pathNode////////////////////////////////////
        {                                                       //
            public pathNode right;                              //
            public pathNode up;                                 //
            public pathNode left;                               //
            public pathNode down;                               //
            public pathNode parent;                             //
            public int pnX;                                     //
            public int pnY;                                     //
                                                                //
            public int dist2TgtSqrd;                            //
                                                                //
            public pathNode() { }//empty default constructor    //
                                                                //
            public pathNode(int d, int x, int y, pathNode p)    //
            {                                                   //
                pnX = x;                                        //
                pnY = y;                                        //
                dist2TgtSqrd = d;                               //
                parent = p;                                     //
            }                                                   //
        }/////////////////////////////////////////////////////////
        
        // orders by pathNode distance to target. least is in the front
        private class myList//////////////////////////////////////////////////////
        {                                                                       //
            public class lNode////////////////////////////////////              //
            {                                                   //              //
                public lNode lesser;                            //              //
                public lNode greater;                           //              //
                public pathNode myPN;                           //              //
                public lNode() { }//empty default constructor   //              //
                public lNode(pathNode pn) { myPN = pn; }        //              //
            }/////////////////////////////////////////////////////              //
                                                                                //
            public lNode head;                                                  //
            public lNode trav;                                                  //
            private lNode tmp;                                                  //
            public void add(pathNode pn)                                        //
            {                                                                   //
                if (pn == null) return;                                         //
                tmp = new lNode(pn);                                            //
                if (head == null)                                               //
                {                                                               //
                    head = tmp;                                                 //
                    return;////////////////////////exiting add()                //
                }                                                               //
                if (pn.dist2TgtSqrd <= head.myPN.dist2TgtSqrd)//is a new head   //
                {                                                               //
                    head.lesser = tmp;                                          //
                    tmp.greater = head;                                         //
                    head = tmp;                                                 //
                    return;////////////////////////exiting add()                //
                }                                                               //
                else if(head.greater == null)//only the head in the list        //
                {                                                               //
                    tmp.lesser = head;                                          //
                    head.greater = tmp;                                         //
                    return;///////////////////////exiting add()                 //
                }                                                               //
                //now the non-trivial situations                                //
                trav = head;                                                    //
                while (pn.dist2TgtSqrd > trav.myPN.dist2TgtSqrd)                //
                {                                                               //
                    if (trav.greater != null)                                   //
                    {                                                           //
                        trav = trav.greater;                                    //
                    }                                                           //
                    else                                                        //
                    {                                                           //
                        trav.greater = tmp;                                     //
                        tmp.lesser = trav;                                      //
                        return;/////////////////////exiting add()               //
                    }                                                           //
                }                                                               //
                tmp.lesser = trav.lesser;//attaching                            //
                tmp.greater = trav;                                             //
                trav.lesser.greater = tmp;                                      //
                trav.lesser = tmp;                                              //
            }//end of add()                                                     //
            public void pop()                                                   //
            {                                                                   //
                if (head == null) return;                                       //
                if(head.greater == null)head = null;                            //
                else                                                            //
                {                                                               //
                    head = head.greater;                                        //
                    head.lesser.greater = null;                                 //
                    head.lesser = null;                                         //
                }                                                               //
            }//end of pop()                                                     //
        }/////////////////////////////////////////////////////////////////////////

        /////////////////////// members ///////////////////
        myList fringe;//front is shortest distance
        public List<byte> pathInstructions;// sequence of right/up/left/down (1-4 respectively) instructions
        byte[,,] map;
        int mapW;
        int mapH;
        int tgtX;
        int tgtY;

        bool mobInPath;


        ////////////////////// constructors /////////////////
        public PathFinder() { }//empty default constructor

        public PathFinder(byte[,,] m, int width, int height)
        {
            map = m;
            mapW = width;
            mapH = height;
            pathInstructions = new List<byte>();
        }

        ///////////////////// functions ///////////////
        public int getDist(int x, int y)// gives squared distance to tgt position
        {
            x -= tgtX;
            x *= x;

            y -= tgtY;
            y *= y;
            
            return (x + y);
        }

        public void findPath(int myX, int myY, int targetX, int targetY)
        {
            int HPx = 0;// HP: hypothetical position
            int HPy = 0;
            tgtX = targetX;
            tgtY = targetY;
            int tmpDist = 0;

            bool[,] searched = new bool[mapW, mapH];//default new bool[] should set all values to false
            myList nextToMobsInTheWay = new myList();
            fringe = new myList();
            pathInstructions = new List<byte>();
            fringe.add(new pathNode(getDist(myX, myY), myX, myY, null));//first fringe is self location
            searched[myX, myY] = true;
            
            int tolerance = 14;
            pathNode[] closest = new pathNode[tolerance];//'tolerance' closest fringes are expanded
            while(true)
            {
                if (fringe.head == null && !mobInPath) throw new ApplicationException("There is no path to the target location");
                if ((fringe.head == null && mobInPath) ||fringe.head.myPN.dist2TgtSqrd <=2 )//found the target or there is a Mob in the path
                {
                    pathNode walkBack;
                    if(fringe.head == null && mobInPath)
                    {
                        walkBack =nextToMobsInTheWay.head.myPN;
                    }
                    else
                    {
                        walkBack = fringe.head.myPN;
                    }
                    //walkBack = walkBack.parent;
                    pathNode tmp = walkBack.parent;
                    while (tmp != null)
                    {
                        if (tmp.right == walkBack)
                        {
                            pathInstructions.Add(1);
                        }
                        else if (tmp.up == walkBack)
                        {
                            pathInstructions.Add(2);
                        }
                        else if (tmp.left == walkBack)
                        {
                            pathInstructions.Add(3);
                        }
                        else//must be down
                        {
                            pathInstructions.Add(4);
                        }

                        walkBack = tmp;
                        tmp = tmp.parent;
                    }
                    mobInPath = false;
                    nextToMobsInTheWay = null;
                    pathInstructions.Reverse();
                    return;
                }
                for (int i = 0; fringe.head != null && i < tolerance; i++)//get top 'tolerance'
                {
                    closest[i] = fringe.head.myPN;
                    fringe.pop();
                }
                for(int i=0; i<tolerance && closest[i] != null; i++)//expand them
                {
                    HPx = closest[i].pnX;
                    HPy = closest[i].pnY;
                    if ((!searched[HPx + 1, HPy]) && (map[HPx + 1, HPy, 0] > 99))//looking right
                    {
                        tmpDist = getDist(HPx + 1, HPy);
                        if (map[HPx +1, HPy,1] != 0)//remember this node in case mobs block all paths
                        {
                            mobInPath = true;
                            nextToMobsInTheWay.add(closest[i]);
                        }
                        else
                        {
                            closest[i].right = new pathNode(tmpDist, HPx + 1, HPy, closest[i]);
                            fringe.add(closest[i].right);
                        }
                        searched[HPx + 1, HPy] = true;
                    }
                    if ((!searched[HPx, HPy - 1]) && (map[HPx, HPy-1, 0] > 99))//looking up
                    {
                        tmpDist = getDist(HPx, HPy - 1);
                        if (map[HPx, HPy - 1, 1] != 0)//remember node
                        {
                            mobInPath = true;
                            nextToMobsInTheWay.add(closest[i]);
                        }
                        else
                        {
                            closest[i].up = new pathNode(tmpDist, HPx, HPy - 1, closest[i]);
                            fringe.add(closest[i].up);
                        }
                            searched[HPx, HPy-1] = true;
                    }
                    if ((!searched[HPx - 1, HPy]) && (map[HPx - 1, HPy, 0] > 99))//looking left
                    {
                        tmpDist = getDist(HPx - 1, HPy);
                        if (map[HPx - 1, HPy, 1] != 0)//remember node
                        {
                            mobInPath = true;
                            nextToMobsInTheWay.add(closest[i]);
                        }
                        else
                        {
                            closest[i].left = new pathNode(tmpDist, HPx - 1, HPy, closest[i]);
                            fringe.add(closest[i].left);
                        }
                            searched[HPx - 1, HPy] = true;
                    }
                    if ((!searched[HPx, HPy + 1]) && (map[HPx, HPy + 1, 0] > 99))//looking down
                    {
                        tmpDist = getDist(HPx, HPy + 1);
                        if (map[HPx, HPy + 1, 1] != 0)//remember node
                        {
                            mobInPath = true;
                            nextToMobsInTheWay.add(closest[i]);
                        }
                        else
                        {
                            closest[i].down = new pathNode(tmpDist, HPx, HPy + 1, closest[i]);
                            fringe.add(closest[i].down);
                        }
                        searched[HPx, HPy + 1] = true;
                    }
                    closest[i] = null;
                }//end of expansion loop
            }//end of while(searching)
        }//end of findPath
    }//end of PathFinder class
}
