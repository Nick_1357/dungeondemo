﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class AI:Controller
    {
        public Mob owner;
        public Mob targetMob;
        private State currentState;
        public List<Mob> mobsNearMe;
        public State defaultState;//don't let FollowState be the default state of any Mob
        public Point tgtPosition;
        public int desiredRange;// crow-flight, NOT manhattan (in tiles)
        public byte rangedAttackCmd = 5;//defaults to the basic attack
        public bool sleeping;

        public pathing.PathFinder pather;

        public AI(Mob o)
        {
            owner = o;
            mobsNearMe = new List<Mob>();
            
            switch (owner.entityType)
            {
                case 2://companion
                    switch (((Player)owner).playerClass)
                    {
                        case Player.PlayerClass.knight:
                            defaultState = new MeleeChaseState();
                            desiredRange = 1;
                            break;
                        case Player.PlayerClass.mage:
                            defaultState = new RangeChaseState();
                            desiredRange = 3;
                            break;
                        case Player.PlayerClass.archer:
                            defaultState = new RangeChaseState();
                            desiredRange = 3;
                            break;
                        case Player.PlayerClass.healer:
                            defaultState = new RangeChaseState();
                            desiredRange = 3;
                            break;
                    }
                    break;
                case 3://a melee type mob
                    defaultState = new MeleeChaseState();
                    break;
                case 4://a range type mob
                    defaultState = new RangeChaseState();
                    break;
                case 8://a range type mob
                    defaultState = new RangeChaseState();
                    desiredRange = 3;
                    break;
                case 9://a range fireball shooting type mob
                    defaultState = new RangeChaseState();
                    desiredRange = 3;
                    break;
            }

            //side stepping a null reference when companion is instantiated in Game1 before DungeonLevel.grid exists
            if (owner.entityType != 2)
            {
                pather = new pathing.PathFinder(DungeonLevel.grid, DungeonLevel.mapSize.X, DungeonLevel.mapSize.Y);
                currentState = new SleepingState();
                sleeping = true;
            }
            else
            {
                currentState = defaultState;
                sleeping = false;
            }
            
        }


        public List<byte> getInstructions()
        {
            if (targetMob != null && targetMob.stats.health <= 0)
            {
                mobsNearMe.Remove(targetMob);
                targetMob = null;//lets go of the dead Mob
            }

            //update sensory data
            updateMobsNearMe();
            currentState.exe(this, owner, targetMob);
            
            List<byte> ret = currentState.whatDo(this, owner, targetMob);//plan actions for the turn
            
            return ret;
        }

        public void changeState(State s)
        {
            currentState = s;
            s.exe(this, owner, targetMob);//potential infinite loop generator. be careful with state change criteria
        }

        public void wakeUp()
        {
            currentState = defaultState;
            currentState.exe(this, owner, targetMob);//this might produce infinite loops if two states calle wakeUp() inside their .exe() functions
            sleeping = false;
        }

        public void setpather()//special just for the companion    :|
        {
            pather = new pathing.PathFinder(DungeonLevel.grid, DungeonLevel.mapSize.X, DungeonLevel.mapSize.Y);
        }

        public void updateMobsNearMe()
        {
            mobsNearMe.RemoveRange(0, mobsNearMe.Count);
            if (owner.entityType == 2)//if it's the companion
            {
                int x = 0;
                int y = 0;
                for (int k = 0; k < DungeonLevel.monsters.Count; k++)//finding the monster at those coordinates
                {
                    x = DungeonLevel.monsters[k].mapLocX - owner.mapLocX;
                    y = DungeonLevel.monsters[k].mapLocY - owner.mapLocY;
                    if (x*x+y*y<=36)//6 tiles away or closer
                    {
                        mobsNearMe.Add(DungeonLevel.monsters[k]);
                    }
                }

                if(mobsNearMe.Count > 0)
                {
                    targetMob = mobsNearMe[0];
                    int distSqr = (owner.mapLocX - targetMob.mapLocX) * (owner.mapLocX - targetMob.mapLocX) +
                            (owner.mapLocY - targetMob.mapLocY) * (owner.mapLocY - targetMob.mapLocY);
                    for (int i=0; i< mobsNearMe.Count; i++)
                    {
                        int testDistSqr = (owner.mapLocX - mobsNearMe[i].mapLocX) * (owner.mapLocX - mobsNearMe[i].mapLocX) +
                            (owner.mapLocY - mobsNearMe[i].mapLocY) * (owner.mapLocY - mobsNearMe[i].mapLocY);
                        if(testDistSqr+2 < distSqr)
                        {
                            targetMob = mobsNearMe[i];
                            distSqr = testDistSqr;
                        }
                    }
                    changeState(defaultState);
                }
            }
            else//it's a monster
            {
                Point playerLoc = new Point(DungeonLevel.player.mapLocX, DungeonLevel.player.mapLocY);
                Point companionLoc = new Point(DungeonLevel.companion.mapLocX, DungeonLevel.companion.mapLocY);
                double distp = Math.Sqrt((owner.mapLocX - playerLoc.X) * (owner.mapLocX - playerLoc.X) + (owner.mapLocY - playerLoc.Y) * (owner.mapLocY - playerLoc.Y));
                double distc = Math.Sqrt((owner.mapLocX - companionLoc.X) * (owner.mapLocX - companionLoc.X) + (owner.mapLocY - companionLoc.Y) * (owner.mapLocY - companionLoc.Y));

                targetMob = (distp < distc) ? DungeonLevel.player : DungeonLevel.companion;
                if(targetMob.stats.health<=0)//just seems like an easier way to say 'pick the other one if the closest one is dead'
                {
                    targetMob = (distp >= distc)? DungeonLevel.player : DungeonLevel.companion;
                }
            }
        }//end of updateMobsNearMe()
    }//end of AI
}


