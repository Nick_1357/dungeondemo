﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class RangeChaseState : State
    {
        public override void exe(AI ctrlr, Mob owner, Mob tgtMob)
        {
            if (tgtMob == null)
            {
                ctrlr.updateMobsNearMe();
                if (ctrlr.mobsNearMe.Count == 0)
                {
                    ctrlr.changeState(new FollowState());//no one around
                    return;
                }
                else
                {
                    ctrlr.targetMob = tgtMob = ctrlr.mobsNearMe[0];
                }
            }

            ctrlr.pather.findPath(owner.mapLocX, owner.mapLocY, tgtMob.mapLocX, tgtMob.mapLocY);
            owner.target.X = tgtMob.mapLocX * 32 + 16;//update monster's 'simulated mouse-click position'
            owner.target.Y = tgtMob.mapLocY * 32 + 16;
        }

        public override List<byte> whatDo(AI ctrlr, Mob owner, Mob tgtMob)
        {
            List<byte> ret = new List<byte>();

            int hpX = owner.mapLocX;
            int hpY = owner.mapLocY;
            int difX = hpX - tgtMob.mapLocX;
            int difY = hpY - tgtMob.mapLocY;

            Point tar = new Point(tgtMob.mapLocX, tgtMob.mapLocY);
            Point hit = State.firstLineOfSight(hpX, hpY, tgtMob.mapLocX, tgtMob.mapLocY, owner.entityType);

            int dist = (int)Math.Sqrt(difX * difX + difY * difY);
            int i = 0;

            while ((dist > ctrlr.desiredRange || !hit.Equals(tar)) && i < owner.actionsRemaining)//checking for line of sight and range
            {
                if (ctrlr.pather.pathInstructions.Count > i)
                {
                    switch (ctrlr.pather.pathInstructions[i++])//imaginarilly walking along the path instructions
                    {
                        case 1://right
                            ++hpX;
                            ++difX;
                            ret.Add(1);
                            break;
                        case 2://up
                            --hpY;
                            --difY;
                            ret.Add(2);
                            break;
                        case 3://left
                            --hpX;
                            --difX;
                            ret.Add(3);
                            break;
                        case 4://down
                            ++hpY;
                            ++difY;
                            ret.Add(4);
                            break;
                        default://should never get here
                            break;
                    }
                }
                else
                {
                    break;
                }
                dist = (int)Math.Sqrt(difX * difX + difY * difY);
                hit = State.firstLineOfSight(hpX, hpY, tgtMob.mapLocX, tgtMob.mapLocY, owner.entityType);
                Mob beingHit = DungeonLevel.getMobByLocation(hit.X, hit.Y);
                if (beingHit != null && ((owner.entityType < 3) ? beingHit.entityType > 2 : beingHit.entityType < 3))
                {
                    ctrlr.targetMob = beingHit;
                    hit = new Point(beingHit.mapLocX, beingHit.mapLocY);
                }
            }
            ctrlr.pather.pathInstructions.RemoveRange(0, i);

            if (dist < ctrlr.desiredRange && owner.actionsRemaining - i > 0)//trying to maintain a buffer distance
            {
                double dist0 = dist + 0.1;//favors staying where it is to attack more while cornered
                double dist1 = 0;
                double dist2 = 0;
                double dist3 = 0;
                double dist4 = 0;

                //checking movable locations and calculating new distance
                if (DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 0] > 99 &&//right
                    DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 1] == 0)
                {
                    dist1 = Math.Sqrt((hpX + 1 - tgtMob.mapLocX) * (hpX + 1 - tgtMob.mapLocX) +
                                      (hpY - tgtMob.mapLocY) * (hpY - tgtMob.mapLocY));
                }

                if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 0] > 99 &&//up
                    DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 1] == 0)
                {
                    dist2 = Math.Sqrt((hpX - tgtMob.mapLocX) * (hpX - tgtMob.mapLocX) +
                                      (hpY - 1 - tgtMob.mapLocY) * (hpY - 1 - tgtMob.mapLocY));
                }

                if (DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 0] > 99 &&//left
                    DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 1] == 0)
                {
                    dist3 = Math.Sqrt((hpX - 1 - tgtMob.mapLocX) * (hpX - 1 - tgtMob.mapLocX) +
                                      (hpY - tgtMob.mapLocY) * (hpY - tgtMob.mapLocY));
                }

                if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 0] > 99 &&//down
                    DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 1] == 0)
                {
                    dist4 = Math.Sqrt((hpX - tgtMob.mapLocX) * (hpX - tgtMob.mapLocX) +
                                      (hpY + 1 - tgtMob.mapLocY) * (hpY + 1 - tgtMob.mapLocY));
                }

                double max = Math.Max(dist0, Math.Max(dist1, Math.Max(dist2, Math.Max(dist3, dist4))));



                //moving to best option.
                if (dist1 == max && State.firstLineOfSight(hpX + 1, hpY, tgtMob.mapLocX, tgtMob.mapLocY, owner.entityType).Equals(tar))//right
                {
                    ret.Add(1);
                    ++i;
                }
                else if (dist2 == max && State.firstLineOfSight(hpX, hpY - 1, tgtMob.mapLocX, tgtMob.mapLocY, owner.entityType).Equals(tar))//up
                {
                    ret.Add(2);
                    ++i;
                }
                else if (dist3 == max && State.firstLineOfSight(hpX - 1, hpY, tgtMob.mapLocX, tgtMob.mapLocY, owner.entityType).Equals(tar))//left
                {
                    ret.Add(3);
                    ++i;
                }
                else if (dist4 == max && State.firstLineOfSight(hpX, hpY + 1, tgtMob.mapLocX, tgtMob.mapLocY, owner.entityType).Equals(tar))//down
                {
                    ret.Add(4);
                    ++i;
                }
            }

            if (owner.actionsRemaining - i > 0 && hit.Equals(tar) && dist <= ctrlr.desiredRange)
            {
                ret.Add(ctrlr.rangedAttackCmd);
            }

            if (ret.Count == 0 && ctrlr.pather.pathInstructions.Count == 0)
            {
                ret.Add(0);//pass turn at least so it won't freeze
                //ctrlr.changeState(new FollowState());
            }

            return ret;
        }
    }
}
