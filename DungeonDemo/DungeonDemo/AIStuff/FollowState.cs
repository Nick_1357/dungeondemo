﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public class FollowState:State// only follows ally Mobs
    {
        bool tooClose = false;
        byte avoid = 0;

        public override void exe(AI ctrlr, Mob owner, Mob tgtMob)//this has problems if the player is dead and there are no monsters around
        {
            if (tgtMob == null)//if no target to follow, find one
            {
                if (owner.entityType == 2)//is companion
                {
                    tgtMob = DungeonLevel.player;//only one to follow is the player
                    if(tgtMob.stats.health <=0)
                    {
                        ctrlr.changeState(ctrlr.defaultState);
                    }
                }
                else//must be a monster following another monster; find the closest one to follow
                {
                    int distSqrd = 1000;
                    int closestDist = distSqrd;
                    int x = 0;
                    int y = 0;
                    for (int i = 0; i < DungeonLevel.monsters.Count; i++)
                    {
                        x = DungeonLevel.monsters[i].mapLocX - owner.mapLocX;
                        y = DungeonLevel.monsters[i].mapLocY - owner.mapLocY;

                        distSqrd = x * x + y * y;
                        if (distSqrd < closestDist)
                        {
                            tgtMob = DungeonLevel.monsters[i];
                            closestDist = distSqrd;
                        }
                    }
                    if (tgtMob == null)//this is the last monster on the map right now
                    {
                        ctrlr.changeState(ctrlr.defaultState);
                        return;
                    }
                }
            }

            int manhattan = Math.Abs(owner.mapLocX - tgtMob.mapLocX) + Math.Abs(owner.mapLocY - tgtMob.mapLocY);
            if(manhattan == 1)
            {
                tooClose = true;
                int sideX = owner.mapLocX - tgtMob.mapLocX;
                int sideY = owner.mapLocY - tgtMob.mapLocY;
                if(sideX == 0)
                {
                    if(sideY > 0)//standing below
                    {
                        if(DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 0] < 100)//if it cannot move down
                        {
                            if(DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 0] > 99)//try left
                            {
                                avoid = 3;
                            }
                            else if(DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 0] > 99)//try right
                            {
                                avoid = 1;
                            }
                        }
                        else
                        {
                            avoid = 4;
                        }

                    }
                    else//standing above
                    {
                        if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 0] < 100)//if it cannot move up
                        {
                            if (DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 0] > 99)//try left
                            {
                                avoid = 3;
                            }
                            else if (DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 0] > 99)//try right
                            {
                                avoid = 1;
                            }
                        }
                        else
                        {
                            avoid = 2;
                        }
                    }
                }
                else
                {
                    if (sideX > 0)//standing to the right
                    {
                        if (DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 0] < 100)//if it cannot move right
                        {
                            if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 0] > 99)//try up
                            {
                                avoid = 2;
                            }
                            else if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 0] > 99)//try down
                            {
                                avoid = 4;
                            }
                        }
                        else
                        {
                            avoid = 1;
                        }

                    }
                    else//standing to the left
                    {
                        if (DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 0] < 100)//if it cannot move left
                        {
                            if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 0] > 99)//try up
                            {
                                avoid = 2;
                            }
                            else if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 0] > 99)//try down
                            {
                                avoid = 4;
                            }
                        }
                        else
                        {
                            avoid = 3;
                        }
                    }
                }
            }
            if( manhattan > 2)
            {
                ctrlr.pather.findPath(owner.mapLocX, owner.mapLocY, tgtMob.mapLocX, tgtMob.mapLocY);
            }
        }

        public override List<byte> whatDo(AI ctrlr, Mob owner, Mob tgtMob)
        {
            List<byte> ret = new List<byte>();
            for(int i=0; i< owner.actionsRemaining; i++)
            {
                if(!tooClose)
                {
                    if(ctrlr.pather.pathInstructions.Count > 0)
                    {
                        ret.Add(ctrlr.pather.pathInstructions[0]);
                        ctrlr.pather.pathInstructions.RemoveAt(0);
                    }
                    else
                    {
                        ret.Add(0);
                        return ret;
                    }
                }
                else
                {
                    ret.Add(avoid);
                    avoid = 0;
                    tooClose = false;
                }

            }
            return ret;
        }
    }
}
