﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public class MeleeChaseState:State
    {
        public override void exe(AI ctrlr, Mob owner, Mob tgtMob)
        {
            if (tgtMob == null)
            {
                ctrlr.updateMobsNearMe();
                if(ctrlr.mobsNearMe.Count == 0)
                {
                    ctrlr.changeState(new FollowState());//no one around
                    return;
                }
                else
                {
                    ctrlr.targetMob = tgtMob = ctrlr.mobsNearMe[0];
                }

            }
            ctrlr.tgtPosition.X = tgtMob.mapLocX;
            ctrlr.tgtPosition.Y = tgtMob.mapLocY;
            owner.target.X = tgtMob.mapLocX * 32+16;//update monster's 'simulated mouse-click position'
            owner.target.Y = tgtMob.mapLocY * 32+16;

            ctrlr.pather.findPath(owner.mapLocX, owner.mapLocY, tgtMob.mapLocX, tgtMob.mapLocY);
        }

        public override List<byte> whatDo(AI ctrlr, Mob owner, Mob tgtMob)
        {
            List<byte> ret = new List<byte>();
            int hpX = owner.mapLocX;
            int hpY = owner.mapLocY;

            int xOff = Math.Abs(hpX - tgtMob.mapLocX);
            int yOff = Math.Abs(hpY - tgtMob.mapLocY);
            int manhattan = xOff + yOff;

            for(int i=0; i< owner.actionsRemaining; i++)
            {
                if(manhattan==1 || (manhattan==2 && yOff==xOff))
                {
                    ret.Add(5);//SwingWeapon
                }
                else
                {
                    if(ctrlr.pather.pathInstructions.Count == 0)//it cannot reach the target and has actions left
                    {
                        if(owner.entityType > 2)//it's not the companion
                        {
                            ctrlr.changeState(new WaitingState());//waiting handles monster problems well enough
                        }
                        //maybe consider a state change or a change of Mob target or do nothing
                        //for now it looks for another mob and waits if there isn't another in range
                        if(ctrlr.mobsNearMe.Count ==0)
                        {
                            ctrlr.changeState(new WaitingState());
                            ctrlr.targetMob = null;
                            return new List<byte>();//return an empty list since it can't chase and kill the mob 
                        }
                        else
                        {
                            Mob tmpTest = tgtMob;
                            for(int j=0; j< ctrlr.mobsNearMe.Count; j++)
                            {
                                if(ctrlr.mobsNearMe[j] != tgtMob)
                                {
                                    ctrlr.targetMob =  ctrlr.mobsNearMe[j];
                                    if (ret.Count > 0)
                                    {
                                        return ret;
                                    }
                                    else
                                    {
                                        List<byte> t = new List<byte>();
                                        t.Add(0);
                                        return t;
                                    }
                                    //return new List<byte>();//return an empty list since it can't chase and kill the mob 
                                }
                            }
                            if(tmpTest == tgtMob)//then this is the only mob it knows of, and cannot reach it. 
                            {
                                ret.Add(0);//effectively wait without the stateChange call hoping the blockage moves out of the way
                            }
                        }
                        continue;
                    }
                    ret.Add(ctrlr.pather.pathInstructions[0]);
                    switch (ctrlr.pather.pathInstructions[0])
                    {
                        case 1://right
                            hpX++;
                            break;
                        case 2://up
                            hpY--;
                            break;
                        case 3://left
                            hpX--;
                            break;
                        case 4://down
                            hpY++;
                            break;
                    }
                    ctrlr.pather.pathInstructions.RemoveAt(0);
                    xOff = Math.Abs(hpX - tgtMob.mapLocX);
                    yOff = Math.Abs(hpY - tgtMob.mapLocY);
                    manhattan = xOff + yOff;
                }
            }
            return ret;
        }
    }
}
