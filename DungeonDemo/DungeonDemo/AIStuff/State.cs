﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public abstract class State
    {
        public abstract void exe(AI ctrlr, Mob owner, Mob tgtMob);

        public abstract List<byte> whatDo(AI ctrlr, Mob owner, Mob tgtMob);

        //returns the mobs in this path. a null fills the last spot if it hits a wall
        public static List<Mob> lineOfSight(int sourceX, int sourceY, int destX, int destY)
        {
            double opposite = destY - sourceY;
            double adjacent = destX - sourceX;
            double h = Math.Sqrt(opposite * opposite + adjacent * adjacent);

            double sine = opposite / h;
            double cosine = adjacent / h;

            double locX = sourceX + 0.5;
            double locY = sourceY + 0.5;

            Point loc = new Point(sourceX, sourceY);
            List<Mob> mobsInPath = new List<Mob>();

            while (loc.X != destX || loc.Y != destY)
            {
                locY += sine;
                locX += cosine;

                loc.X = (int)(locX);// + ((cosine>0)? 0.5: -0.5));
                loc.Y = (int)(locY);// + ((sine>0)? 0.5: -0.5));

                if (DungeonLevel.grid[loc.X, loc.Y, 1] > 99)
                {
                    mobsInPath.Add(DungeonLevel.getMobByLocation(loc.X, loc.Y));
                }

                if (DungeonLevel.grid[loc.X, loc.Y, 0] < 100)//hit a wall
                {
                    mobsInPath.Add(null);
                    return mobsInPath;
                }
            }

            return mobsInPath;//made it
        }

        public static Point firstLineOfSightMightyCharge(int sourceX, int sourceY, int destX, int destY)
        {
            double opposite = destY - sourceY;
            double adjacent = destX - sourceX;
            double h = Math.Sqrt(opposite * opposite + adjacent * adjacent);

            double sine = opposite / h;
            double cosine = adjacent / h;

            double locX = sourceX + 0.5;
            double locY = sourceY + 0.5;

            Point loc = new Point(sourceX, sourceY);

            while (loc.X != destX || loc.Y != destY)
            {
                locY += sine;
                locX += cosine;

                loc.X = (int)(locX);// + ((cosine>0)? 0.5: -0.5));
                loc.Y = (int)(locY);// + ((sine>0)? 0.5: -0.5));

                if (DungeonLevel.grid[loc.X, loc.Y, 0] < 100 || DungeonLevel.getMobByLocation(loc.X, loc.Y) != null)//hit a wall or mob
                {
                    return loc;
                }
            }
            return loc;//made it
        }

        //returns either the location of the destination, wall tile in the way or first mob hit.
        public static Point firstLineOfSight(int sourceX, int sourceY, int destX, int destY, int entityType)
        {
            double opposite = destY - sourceY;
            double adjacent = destX - sourceX;
            double h = Math.Sqrt(opposite * opposite + adjacent * adjacent);

            double sine = opposite / h / 2;
            double cosine = adjacent / h / 2;

            double locX = sourceX + 0.5;
            double locY = sourceY + 0.5;

            Point loc = new Point(sourceX, sourceY);

            while (loc.X != destX || loc.Y != destY)
            {
                locY += sine;
                locX += cosine;

                loc.X = (int)(locX);// + ((cosine>0)? 0.5: -0.5));
                loc.Y = (int)(locY);// + ((sine>0)? 0.5: -0.5));

                Mob temp = DungeonLevel.getMobByLocation(loc.X, loc.Y);

                if (DungeonLevel.grid[loc.X, loc.Y, 0] < 100 || temp != null && ((entityType > 2) ? (temp.entityType < 2) : (temp.entityType > 2)))//hit a wall or mob
                {
                    return loc;
                }
            }
            return loc;//made it
        }







    }
}
