﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{ 
    public class SleepingState:State
    {
        public SleepingState()
        { }

        public override void exe(AI ctrlr, Mob owner, Mob tgtMob)
        {
            Point playerLoc = new Point(DungeonLevel.player.mapLocX, DungeonLevel.player.mapLocY);
            Point companionLoc = new Point(DungeonLevel.companion.mapLocX, DungeonLevel.companion.mapLocY);
            int mnhattanDistp = Math.Abs(owner.mapLocX - playerLoc.X) + Math.Abs(owner.mapLocY  - playerLoc.Y);
            int mnhattanDistc = Math.Abs(owner.mapLocX - companionLoc.X) + Math.Abs(owner.mapLocY - companionLoc.Y);
            if(mnhattanDistp < 7 || mnhattanDistc < 7)
            {
                ctrlr.targetMob = (mnhattanDistp < mnhattanDistc) ? DungeonLevel.player : DungeonLevel.companion;
                ctrlr.wakeUp();
            }

        }

        public override List<byte> whatDo(AI ctrlr, Mob owner, Mob tgtMob)
        {
             List<byte> ret = new List<byte>();
            for (int i = 0; i < owner.actionsRemaining; i++)
            {
                ret.Add(0);//do nothing while sleeping
            }
            return ret;
        }
    }
}
