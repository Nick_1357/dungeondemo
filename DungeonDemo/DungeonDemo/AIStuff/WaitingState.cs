﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class WaitingState:State
    {
        public override void exe(AI ctrlr, Mob owner, Mob tgtMob)
        {
            if (tgtMob != null)
            {
                return;
            }
            else
            {
                ctrlr.changeState(ctrlr.defaultState);
            }

            ctrlr.updateMobsNearMe();
        }

        public override List<byte> whatDo(AI ctrlr, Mob owner, Mob tgtMob)
        {
            List<byte> ret = new List<byte>();
            for (int i = 0; i < owner.actionsRemaining; i++)
            {
                ret.Add(0);//do nothing while waiting
            }

            return ret;
        }
    }
}
