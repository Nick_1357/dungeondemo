﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public struct Status
    {
        public int stun;
        public int root;
        public int blind;
        public int silence;
        public int VialOfDeath;
        public int archerSuperSpeed;

        public Status( bool notUsed_doesntMatter)
        {
            stun = 0;
            root = 0;
            blind = 0;
            silence = 0;

            VialOfDeath = 0;
            archerSuperSpeed = 0;
        }
    }
}
