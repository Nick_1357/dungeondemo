﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class SelectionBox
    {
        Texture2D boxTex;
        Rectangle posRect;

        public SelectionBox(Texture2D tex, Point pos, Point innerSize)
        {
            boxTex = tex;
            posRect = new Rectangle(pos.X, pos.Y, innerSize.X+4, innerSize.Y+4);
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(boxTex, posRect, Color.WhiteSmoke);
        }

        public void setPosition(Rectangle overlap)
        {
            posRect.X = overlap.X - 2;
            posRect.Y = overlap.Y - 2;

        }
    }
}
