using System;

namespace DungeonDemo
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            // system message box for resolution setting ??????

            using (Game1 game = new Game1())
            {
                game.Run();
            }
        }
    }
#endif
}

