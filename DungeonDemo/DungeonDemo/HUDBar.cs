﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class HUDBar : Bar
    {
        int locationX;
        int locationY;
        int fullLength;

        public HUDBar(Mob o, Texture2D tex, int locX, int locY, int fullLen, double fullVal, double currVal, Color col) :
                 base(o, tex, 0, 0, fullVal, currVal, col)
        {
            locationX = locX;
            locationY = locY;
            fullLength = fullLen;

            if(o.entityType == 1) //player size hud bar
            {
                posRect = new Rectangle(locationX, locationY, (int)(fullLength * (currentValue / fullValue)), 16);
            }
            else if(o.entityType == 2) // companion size hud bar
            {
                posRect = new Rectangle(locationX, locationY, (int)(fullLength * (currentValue / fullValue)), 8);
            }
            else
            {
                //default size
                posRect = new Rectangle(locationX, locationY, (int)(fullLength * (currentValue / fullValue)), 16);
            }

            this.barColor = col;
            this.barColor.A = 180;
        }

        public new void update(double currVal)
        {
            currentValue = currVal;
            posRect.X = (int)(locationX + Game1.currentCamera.Position.X);
            posRect.Y = (int)(locationY + Game1.currentCamera.Position.Y);
            
            posRect.Width = (int)(fullLength * currentValue / fullValue);
        }

        public new void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(barTex, posRect, null, this.barColor, 0.0f, Vector2.Zero, SpriteEffects.None, 0.01f );
        }
    }
}
