﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class Bar
    {
        public Rectangle posRect;
        public Texture2D barTex;
        public Mob owner;
        public Color barColor;
        public double fullValue;
        public double currentValue;
        public int xOffset;
        public int yOffset;


        public Bar(Mob o, Texture2D tex, int xOff, int yOff, double fullVal, double currVal, Color col)
        {
            owner = o;
            barTex = tex;
            fullValue = fullVal;
            currentValue = currVal;
            if(fullValue == 0)
            {
                fullValue = 0.00001;
            }
            xOffset = xOff;
            yOffset = yOff;

            posRect = new Rectangle(owner.posRect.X + xOffset, owner.posRect.Y + yOffset, (int)(32 * (currentValue / fullValue)), 4);

            barColor = col;
            barColor.A = 150;
        }

        public void update(double currVal)
        {
            currentValue = currVal;
            posRect.X = owner.posRect.X + xOffset-16;
            posRect.Y = owner.posRect.Y + yOffset-16;
            posRect.Width = (int)(32 * currentValue / fullValue);
        }

        public void draw(SpriteBatch spriteBach)
        {
            spriteBach.Draw(barTex, posRect, barColor);
        }
    }
}
