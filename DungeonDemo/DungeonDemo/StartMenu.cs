﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DungeonDemo
{
    public class StartMenu
    {
        Texture2D menuTex;
        Rectangle posRect;
        Rectangle srcSize;

        SelectionBox playerSelect;
        SelectionBox companionSelect;

        Player.PlayerClass playerClass;
        Player.PlayerClass companionClass;

        Rectangle mousePoint;

        Rectangle pArcher;
        Rectangle pKnight;
        Rectangle pMage;
        Rectangle pHealer;

        Rectangle cArcher;
        Rectangle cKnight;
        Rectangle cMage;
        Rectangle cHealer;

        Rectangle goButton;

        public StartMenu(Texture2D myTex, Texture2D selectTex, Game game)
        {
            menuTex = myTex;
            posRect = new Rectangle(190,0,game.GraphicsDevice.Viewport.Width - 380, game.GraphicsDevice.Viewport.Height);
            srcSize = new Rectangle(0, 0, menuTex.Width, menuTex.Height);

            Point selectSize = new Point(132, 132); //dev and tested in 132x132 for 1280x640 screen
            playerSelect = new SelectionBox(selectTex, new Point(256, 73), selectSize);
            companionSelect = new SelectionBox(selectTex, new Point(647, 73), selectSize);

            mousePoint = new Rectangle(0, 0, 1, 1);

            //guessing number game
            pArcher = new Rectangle((int)(Game1.ScreenWidthRef * 0.2015f), (int)(Game1.ScreenHeightRef * 0.1171f), 128, 128); // developed in (258, 75, 128, 128);
            pKnight = new Rectangle((int)(Game1.ScreenWidthRef * 0.2015f), (int)(Game1.ScreenHeightRef * 0.3344f), 128, 128);// developed in (258, 214, 128, 128);
            pMage = new Rectangle((int)(Game1.ScreenWidthRef * 0.2015f), (int)(Game1.ScreenHeightRef * 0.5515f), 128, 128);  // developed in (258, 353, 128, 128);
            pHealer = new Rectangle((int)(Game1.ScreenWidthRef * 0.2015f), (int)(Game1.ScreenHeightRef * 0.7687f), 128, 128);// developed in (258, 492, 128, 128);

            cArcher = new Rectangle((int)(Game1.ScreenWidthRef * 0.5070f), (int)(Game1.ScreenHeightRef * 0.1171f), 128, 128); // developed in (649, 75, 128, 128);
            cKnight = new Rectangle((int)(Game1.ScreenWidthRef * 0.5070f), (int)(Game1.ScreenHeightRef * 0.3344f), 128, 128);// developed in (649, 214, 128, 128);
            cMage = new Rectangle((int)(Game1.ScreenWidthRef * 0.5070f), (int)(Game1.ScreenHeightRef * 0.5515f), 128, 128);  // developed in (649, 353, 128, 128);
            cHealer = new Rectangle((int)(Game1.ScreenWidthRef * 0.5070f), (int)(Game1.ScreenHeightRef * 0.7687f), 128, 128);// developed in (649, 492, 128, 128);

            goButton = new Rectangle((int)(Game1.ScreenWidthRef * 0.7187f), (int)(Game1.ScreenHeightRef * 0.8765f), 154, 65);

            playerClass = Player.PlayerClass.archer;
            companionClass = Player.PlayerClass.archer;
        }

        public void update()
        {
            //detect mouse clicks
            MouseState mouseState = Mouse.GetState();
            mousePoint.X = mouseState.X;
            mousePoint.Y = mouseState.Y;

            if(mouseState.LeftButton == ButtonState.Pressed)
            {
                if(mousePoint.Intersects(pArcher))//player
                {
                    playerSelect.setPosition(pArcher);
                    playerClass = Player.PlayerClass.archer;
                }
                else if (mousePoint.Intersects(pKnight))
                {
                    playerSelect.setPosition(pKnight);
                    playerClass = Player.PlayerClass.knight;
                }
                else if (mousePoint.Intersects(pMage))
                {
                    playerSelect.setPosition(pMage);
                    playerClass = Player.PlayerClass.mage;
                }
                else if (mousePoint.Intersects(pHealer))
                {
                    playerSelect.setPosition(pHealer);
                    playerClass = Player.PlayerClass.healer;
                }
                else if (mousePoint.Intersects(cArcher))//companion
                {
                    companionSelect.setPosition(cArcher);
                    companionClass = Player.PlayerClass.archer;
                }
                else if (mousePoint.Intersects(cKnight))
                {
                    companionSelect.setPosition(cKnight);
                    companionClass = Player.PlayerClass.knight;
                }
                else if (mousePoint.Intersects(cMage))
                {
                    companionSelect.setPosition(cMage);
                    companionClass = Player.PlayerClass.mage;
                }
                else if (mousePoint.Intersects(cHealer))
                {
                    companionSelect.setPosition(cHealer);
                    companionClass = Player.PlayerClass.healer;
                }
                else if (mousePoint.Intersects(goButton))
                {
                    //do it!
                    Game1.player.setClass(playerClass);
                    Game1.companion.setClass(companionClass);

                    Game1.gameState = Game1.GameState.Playing;//probably will change to Loading later
                }
            }
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(menuTex, posRect, srcSize, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0.5f);
            playerSelect.draw(spriteBatch);
            companionSelect.draw(spriteBatch);
        }
    }
}
