﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class TempPrintOnScreen
    {
        private class Msg/////////////////////////////////////////////////////////////////////
        {                                                                                   //
            public string text;                                                             //
            public Color textColor;                                                         //
            public Vector2 worldPosition;//place to draw the text in the world-space        //
            public int lifeSpanMilliSec;                                                    //
            public Msg(string txt, Vector2 pos, int lifeMil, Color txtColor)                //
            {                                                                               //
                text = txt;                                                                 //
                textColor = txtColor;                                                       //
                lifeSpanMilliSec = lifeMil;//how long before the message is destroyed       //
                worldPosition = pos;                                                        //
            }                                                                               //
        }/////////////////////////////////////////////////////////////////////////////////////

        static List<Msg> textOnScreen;

        public TempPrintOnScreen()
        {
            textOnScreen = new List<Msg>();
        }

        static public void addMessage(string text, Vector2 worldPos, int milliSecDisp, Color msgColor)
        {
            textOnScreen.Add(new Msg(text, worldPos, milliSecDisp, msgColor));
        }

        static public void Update(GameTime gameTime)
        {
            int deltaTime = gameTime.ElapsedGameTime.Milliseconds;
            for(int i=0; i<textOnScreen.Count; i++)
            {
                textOnScreen[i].lifeSpanMilliSec -= deltaTime;
                textOnScreen[i].worldPosition.Y -= 2;

                if(textOnScreen[i].lifeSpanMilliSec <= 0)
                {
                    textOnScreen.RemoveAt(i);
                    i--;
                }
            }
        }

        static public void Draw(SpriteBatch spriteBatch)
        {
            for(int i=0; i<textOnScreen.Count; i++)
            {
                spriteBatch.DrawString(Game1.gameFont,textOnScreen[i].text, textOnScreen[i].worldPosition, textOnScreen[i].textColor);
            }
        }
    }
}
