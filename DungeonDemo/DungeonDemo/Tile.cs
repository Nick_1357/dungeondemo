﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class Tile: Base
    {
        byte type;

        public Tile(byte t, int mapX, int mapY, Rectangle rec, Texture2D tex, int fSpeed, int FPSeq, Point fSize, Point ptInSheet)
        {
            type = t;// this should be pulled from map[,,0] and maybe used to set facingDirection and animState

            mapLocX = mapX;
            mapLocY = mapY;

            posRect = rec;

            facingDirection = Direction.right;// =0
            animState = AnimState.waiting;// =0

            anim = new TileAnimator(tex, fSpeed, fSize, FPSeq, ptInSheet, facingDirection, animState);
        }
    }
}
