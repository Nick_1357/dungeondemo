﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyFirstGame
{
    class Class123
    {
        //
        Texture2D objTexture;
        public Texture2D Texture
        {
            get { return objTexture; }
            set { objTexture = value; }
        }

        Rectangle boxCollider;
        public Rectangle BoxCollider
        {
            get { return boxCollider; }
        }

        /// <summary>
        /// Origin is used to find the graphical center of the game object
        /// </summary>
        public Vector2 Origin
        {
            get
            {
                return new Vector2(Texture.Width / 2.0f, Texture.Height / 2.0f);
            }
        }
    }
}
