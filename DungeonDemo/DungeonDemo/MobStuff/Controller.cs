﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public interface Controller//maps AI commands or controller input to action commands
    {
        List<byte> getInstructions();
    }
}
