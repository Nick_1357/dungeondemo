﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class Monster:Mob
    {
        public Monster(Stats myStats, byte eType, int mapX, int mapY, Rectangle pRec,
                      Texture2D sprSheet, int fSpeed, Point fSize, int FPSeq)
        {
            mapLocX = mapX;
            mapLocY = mapY;

            facingDirection = Base.Direction.down;
            animState = Base.AnimState.waiting;
            anim = new MobAnimator(this, sprSheet, fSpeed, fSize, FPSeq, facingDirection, animState, 0f, 0.2f);
            posRect = pRec;

            target = new Point(0, 0);
            instructions = new List<byte>();
            actionsPerTurn = 2;
            actionsRemaining = actionsPerTurn;

            entityType = eType;

            actions = new List<Action>();
            actions.Add(new NOP());//0
            actions.Add(new MoveRight());//1
            actions.Add(new MoveUp());//2
            actions.Add(new MoveLeft());//3
            actions.Add(new MoveDown());//4

            //monster specific actions 
            //basic attack
            //spell1
            //spell2
            //spell3
            //spell4
            switch (eType)
            {
                case 3://first monster type is 3. player is 1 and companion is 2
                    actions.Add(new SwingWeapon());//5
                    //action.Add(some other action);
                    break;
                case 4://don't have this monster yet
                    break;
                case 8:
                    actions.Add(new ShootArrow());
                    break;
                case 9:
                    actions.Add(new Act_FireBall());
                    break;
            }

            controller = new AI(this);
            stats = myStats;

            entityType = eType;

            //this will need to be filled based on the monster type
            stats.level = 1;
            stats.healthMax = 75;
            stats.health = 75;
            stats.mana = 100;
            stats.strength = 3;
            stats.intellect = 1;

            healthBar = new Bar(this, DungeonData.WHITESPACE, 0, -5, stats.healthMax, stats.health, Color.DarkRed);
        }

        public bool isSleeping()
        {
            return ((AI)controller).sleeping;
        }
    }
}
