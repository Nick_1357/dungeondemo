﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace DungeonDemo
{
    public class PlayerInterface : Controller
    {
        private KeyboardState oldKBState;
        private KeyboardState newKBState;
        private MouseState oldMouseState;
        private MouseState newMouseState;

        private GamePadState oldGPState;
        private GamePadState newGPState;

        private Player owner;

        public PlayerInterface(Player o)
        {
            newKBState = Keyboard.GetState();
            oldKBState = newKBState;
            newMouseState = Mouse.GetState();
            oldMouseState = newMouseState;

            newGPState = GamePad.GetState(PlayerIndex.One);//implement later
            oldGPState = newGPState;

            owner = o;
        }


        public List<byte> getInstructions()
        {
            List<byte> ret = new List<byte>();

            // a whole bunch of polling and prompts. this is where the player takes their turn
            byte instruction = 0;// action WaitForInput keeps the TurnController on the player until they do something

            oldKBState = newKBState;
            oldMouseState = newMouseState;
            oldGPState = newGPState;

            newKBState = Keyboard.GetState();
            newMouseState = Mouse.GetState();
            newGPState = GamePad.GetState(PlayerIndex.One);


            if (newKBState.IsKeyDown(Keys.D))
            {
                if (DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 0] > 99 &&
                        DungeonLevel.grid[owner.mapLocX + 1, owner.mapLocY, 1] == 0)
                {
                    instruction = 1;
                }
            }
            else if (newKBState.IsKeyDown(Keys.W))
            {
                if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 0] > 99 &&
                        DungeonLevel.grid[owner.mapLocX, owner.mapLocY - 1, 1] == 0)
                {
                    instruction = 2;
                }
            }
            else if (newKBState.IsKeyDown(Keys.A))
            {
                if (DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 0] > 99 &&
                        DungeonLevel.grid[owner.mapLocX - 1, owner.mapLocY, 1] == 0)
                {
                    instruction = 3;
                }
            }
            else if (newKBState.IsKeyDown(Keys.S))
            {
                if (DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 0] > 99 &&
                        DungeonLevel.grid[owner.mapLocX, owner.mapLocY + 1, 1] == 0)
                {
                    instruction = 4;
                }
            }
            else if (newKBState.IsKeyDown(Keys.D1))//hitting 1 above the Q key to choose basic attack
            {
                instruction = 5;

                //update target with mouse position
                owner.target.X = (int)(Mouse.GetState().X / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.X - Game1.currentCamera.Origin.X);
                owner.target.Y = (int)(Mouse.GetState().Y / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.Y - Game1.currentCamera.Origin.Y);
            }

            //////////////////////////////////////////////// TESTING MIGHTY CHARGE now CLASS //////////////////////////////////////////////////////////////////
            else if (newKBState.IsKeyDown(Keys.D2))//hitting 2 above the Q key to choose first Spell attack
            {
                instruction = 6;

                //update target with mouse position
                owner.target.X = (int)(Mouse.GetState().X / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.X - Game1.currentCamera.Origin.X);
                owner.target.Y = (int)(Mouse.GetState().Y / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.Y - Game1.currentCamera.Origin.Y);
            }
            //////////////////////////////////////////////// TESTING MIGHTY CHARGE now CLASS //////////////////////////////////////////////////////////////////

            else if (newKBState.IsKeyDown(Keys.D3))//hitting 3 above the Q key to choose first Spell attack
            {
                instruction = 7;

                //update target with mouse position
                owner.target.X = (int)(Mouse.GetState().X / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.X - Game1.currentCamera.Origin.X);
                owner.target.Y = (int)(Mouse.GetState().Y / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.Y - Game1.currentCamera.Origin.Y);
            }

            else if (newKBState.IsKeyDown(Keys.D4))//hitting 4 above the Q key to choose first Spell attack
            {
                instruction = 8;

                //update target with mouse position
                owner.target.X = (int)(Mouse.GetState().X / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.X - Game1.currentCamera.Origin.X);
                owner.target.Y = (int)(Mouse.GetState().Y / Game1.currentCamera.Scale + 16 + Game1.currentCamera.Position.Y - Game1.currentCamera.Origin.Y);
            }

            ////////////////////////////// Free Self Heal for testing purposes ///////////////////////////////
            if (!oldKBState.IsKeyDown(Keys.H) && newKBState.IsKeyDown(Keys.H))
            {
                if (owner.stats.health + 50 < owner.stats.healthMax)
                    owner.stats.health += 50;
                else
                    owner.stats.health = owner.stats.healthMax;
            }
            if (!oldKBState.IsKeyDown(Keys.J) && newKBState.IsKeyDown(Keys.J))
            {
                if (owner.stats.mana + 50 < owner.stats.manaMax)
                    owner.stats.mana += 50;
                else
                    owner.stats.mana = owner.stats.manaMax;
            }

            if (!oldKBState.IsKeyDown(Keys.P) && newKBState.IsKeyDown(Keys.P))
            {
                if (DungeonLevel.companion.stats.health + 50 < DungeonLevel.companion.stats.healthMax)
                    DungeonLevel.companion.stats.health += 50;
                else
                    DungeonLevel.companion.stats.health = DungeonLevel.companion.stats.healthMax;
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////

            //after the player makes a choice, the action(s) is/are added to the list and returned
            ret.Add(instruction);
            return ret;
        }
    }
}
