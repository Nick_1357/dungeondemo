﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DungeonDemo
{
    public class Player : Mob
    {
        public enum PlayerClass
        {
            knight = 0,
            mage, archer, healer
        }

        public PlayerClass playerClass;

        public Player(Stats myStats, byte eType, PlayerClass pClass, int mapX, int mapY, Rectangle pRec,
                      Texture2D sprSheet, int fSpeed, Point fSize, int FPSeq)
        {
            mapLocX = mapX;
            mapLocY = mapY;

            facingDirection = Base.Direction.down;
            animState = Base.AnimState.waiting;
            anim = new MobAnimator(this, sprSheet, fSpeed, fSize, FPSeq, facingDirection, animState, 0f, 0.2f);
            posRect = pRec;

            target = new Point(0, 0);
            instructions = new List<byte>();
            actionsPerTurn = 3;
            actionsRemaining = actionsPerTurn;
            entityType = eType;
            playerClass = pClass;

            actions = new List<Action>();
            if (entityType == 1)//it's the player
            {
                controller = new PlayerInterface(this);
                actions.Add(new WaitForInput());// unique player action so the game doesn't leave their turn until they act and use all their actionsPerTurn
            }
            else//it's the companion
            {
                controller = new AI(this);
                actions.Add(new NOP());//companion gets the do nothing command, like the monsters do
            }

            actions.Add(new MoveRight());//everything can walk around or sit still
            actions.Add(new MoveUp());
            actions.Add(new MoveLeft());
            actions.Add(new MoveDown());

            stats = myStats;

            stats.level = 1;
            stats.healthMax = 100;
            stats.health = 100;
            stats.manaMax = 100;
            stats.mana = 100;
            stats.strength = 3;
            stats.intellect = 2;

            healthBar = new Bar(this, DungeonData.WHITESPACE, 0, -5, stats.healthMax, stats.health, Color.DarkRed);

        }//end of constructor

        public void nullTargets()
        {
            ((AI)controller).targetMob = null;
        }

        public void setPather()//for the companion. see AI constructor for why it's here
        {
            ((AI)controller).setpather();
        }

        public void setClass(PlayerClass newClass)
        {
            playerClass = newClass;
            if (entityType == 2)
            {
                controller = new AI(this);//giving the companion the correct AI for the new class
                setPather();
            }

            switch (newClass)// add the spells now
            {
                case PlayerClass.knight:
                    anim.spriteSheet = DungeonData.knightSpriteSheet;
                    actions.Add(new SwingWeapon());
                    actions.Add(new Act_MightyCharge());
                    actions.Add(new Act_ShieldBash());
                    actions.Add(new Act_Cleave());
                    //ations.Add( the ultimate ability ) 
                    //add other abilities
                    break;
                case PlayerClass.mage:
                    anim.spriteSheet = DungeonData.mageSpriteSheet;
                    actions.Add(new Act_FireBall());
                    actions.Add(new Act_BlindingBurst());
                    actions.Add(new Act_Teleport());
                    actions.Add(new NOP());
                    //add abilities
                    break;
                case PlayerClass.archer:
                    anim.spriteSheet = DungeonData.archerSpriteSheet;
                    actions.Add(new ShootArrow());
                    actions.Add(new Act_PiercingShot());
                    actions.Add(new SwingWeapon()); // replace with triple shot
                    actions.Add(new NOP());
                    //add abilities
                    break;
                case PlayerClass.healer:
                    anim.spriteSheet = DungeonData.healerSpriteSheet;
                    actions.Add(new Act_Smite());
                    actions.Add(new Act_PiercingShot());
                    actions.Add(new Act_MightyCharge());
                    actions.Add(new NOP());
                    //add abilities
                    break;
            }

        }
    }
}
