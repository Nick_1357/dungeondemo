﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DungeonDemo
{
    public struct Stats
    {
        public int level;

        public int healthMax;
        public int health;
        public int manaMax;
        public int mana;

        public int strength;
        public int intellect;

        public Stats(int lvl, int h, int m, int str, int intell)
        {
            level = lvl;

            healthMax = h;
            health = healthMax;
            manaMax = m;
            mana = manaMax;

            strength = str;
            intellect = intell;
        }

    }
}
