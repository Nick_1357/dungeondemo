﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace DungeonDemo
{
    public abstract class Mob : Base
    {
        protected List<byte> instructions;
        public Point target;// in pixels: this can be converted === (target(x,y)/tileSize)|--> map(x,y)|--> Mob|::  if/as needed inside the spell function that uses it.

        protected List<Action> actions;//array of actions mob can make. usually the 4 move directions and a basic attack at least
        public Controller controller;
        public Stats stats;
        public byte actionsPerTurn;
        public byte actionsRemaining;
        public Bar healthBar;

        bool moving = false;
        int startX = 0;
        int startY = 0;
        int distX = 0;
        int distY = 0;

        public byte entityType;//used in grid[x,y,1] to determine locaions and types of mobs for some functions

        //Base override
        public virtual new void Update(GameTime gameTime)//hides Base.Update() on purpose
        {
            if (stats.health <= 0)
            {
                if (entityType == 1) { /* do nothing; the player is dead and Game1::Update() will handle this case.*/ }
                if (entityType == 2)//companion has died
                {
                    DungeonLevel.players.Remove((Player)this);
                }
                else
                {
                    DungeonLevel.monsters.Remove((Monster)this);
                }
                DungeonLevel.mobs.Remove(this);
                DungeonLevel.grid[mapLocX, mapLocY, 1] = 0;
            }
            anim.update(gameTime.ElapsedGameTime.Milliseconds, facingDirection, animState);
            healthBar.update(stats.health);

            int destX = mapLocX * anim.frameSize.X;
            int destY = mapLocY * anim.frameSize.Y;
            if (posRect.X != destX || posRect.Y != destY)
            {
                if (!moving)
                {
                    moving = true;
                    startX = posRect.X;
                    startY = posRect.Y;
                    distX = (destX - startX);
                    distY = (destY - startY);
                }
                posRect.X = (int)(startX + distX * ((MobAnimator)anim).timeInState / ((MobAnimator)anim).stateSeqTime);
                posRect.Y = (int)(startY + distY * ((MobAnimator)anim).timeInState / ((MobAnimator)anim).stateSeqTime);

                if ((Math.Abs(posRect.X - destX) < 4) && (Math.Abs(posRect.Y - destY) < 4))//4 pixels
                {
                    moving = false;
                    posRect.X = destX;
                    posRect.Y = destY;
                }
            }
        }//end of Update


        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.Draw(spriteBatch, gameTime);
            healthBar.draw(spriteBatch);
        }


        //Mob functions
        public int takeAction()
        {
            if (actionsRemaining <= 0) return 0;//no cheating


            if (instructions.Count <= 0)
            {
                instructions = controller.getInstructions();
            }
            if (instructions.Count == 0) return 0;//takes no time. usually a state transition needs this so it 
                                                  //can change states to get instructions without having a while loop above


            switch (instructions[0])//all of this will change later. for now it just keeps monsters from standing on the same tile and walking through walls
            {
                case 1://right
                    if (DungeonLevel.grid[mapLocX + 1, mapLocY, 1] != 0 || DungeonLevel.grid[mapLocX + 1, mapLocY, 0] < 100) instructions[0] = 0;//if there is a Mob to the right NOP
                    break;
                case 2://up
                    if (DungeonLevel.grid[mapLocX, mapLocY - 1, 1] != 0 || DungeonLevel.grid[mapLocX, mapLocY - 1, 0] < 100) instructions[0] = 0;//if there is a Mob to the up NOP
                    break;
                case 3://left
                    if (DungeonLevel.grid[mapLocX - 1, mapLocY, 1] != 0 || DungeonLevel.grid[mapLocX - 1, mapLocY, 0] < 100) instructions[0] = 0;//if there is a Mob to the left NOP
                    break;
                case 4://down
                    if (DungeonLevel.grid[mapLocX, mapLocY + 1, 1] != 0 || DungeonLevel.grid[mapLocX, mapLocY + 1, 0] < 100) instructions[0] = 0;//if there is a Mob to the down NOP
                    break;
            }

            int animTime = actions[instructions[0]].act(this, target);
            instructions.RemoveAt(0);
            --actionsRemaining;
            return animTime;
        }

        public void setTimeInState(int milliSecsInState)
        {
            ((MobAnimator)anim).stateSeqTime = milliSecsInState - 35;
        }

        public int takeDamage(Mob source, double dmgSent, DamageType dmgType)
        {
            if ((entityType < 3 && source.entityType < 3) || (entityType > 2 && source.entityType > 2))
            {
                return 0;
            }
            double damageTaken = 0;// using doubles to calculate, to reduce rounding errors
            switch (dmgType)
            {
                case DamageType.absolute:
                    damageTaken = dmgSent;
                    break;
                case DamageType.normal:// need to meet to decide more about how numbers will be handled for all this
                    damageTaken = dmgSent - stats.strength;
                    break;
                case DamageType.earth:
                    damageTaken = dmgSent - stats.level;
                    break;
                case DamageType.fire:
                    damageTaken = dmgSent - stats.level;
                    break;
                case DamageType.ice:
                    damageTaken = dmgSent - stats.level;
                    break;
            }
            if (damageTaken < 0) damageTaken = 0;

            stats.health -= (int)damageTaken;
            return (int)damageTaken;//casting as an int to keep numbers more discrete for simplicity
        }
    }
}
